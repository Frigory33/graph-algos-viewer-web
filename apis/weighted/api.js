function loadGraph(flags) {
   const edges = getAllEdges(), calcIncMatrix = (flags & 2) == 2;
   const graph = {
         nbVertices: nbVertices, nbEdges: edges.length, isUndirected: graphIsUndirected,
         edges: [], succLists: [], predLists: [], adjMatrix: [],
      };
   for (let vNum = 0; vNum < nbVertices; ++vNum) {
      graph.succLists[vNum] = [];
      graph.predLists[vNum] = [];
      graph.adjMatrix[vNum] = new Float64Array(nbVertices);
   }
   if (calcIncMatrix) {
      graph.incMatrix = [];
      for (let vNum = 0; vNum < nbVertices; ++vNum) {
         graph.incMatrix[vNum] = new Float64Array(edges.length);
      }
   }
   graph.selectedVertices = Object.freeze(Array.from(svgGraph.vertices.querySelectorAll('.vertex.selected'))
      .map(vertex => parseInt(vertex.getAttribute('data-num')))
      .sort((vNum1, vNum2) => vNum1 < vNum2 ? -1 : vNum1 > vNum2 ? 1 : 0));
   let edgeNum = 0;
   graph.selectedEdges = [];
   for (const edge of edges) {
      const srcNum = parseInt(document.getElementById(edge.getAttribute('data-from')).getAttribute('data-num')),
         destNum = parseInt(document.getElementById(edge.getAttribute('data-to')).getAttribute('data-num')),
         weight = strToFloat(edge.querySelector('.weight').textContent);
      const edgeProps = Object.freeze({ src: srcNum, dest: destNum, weight: weight, num: edgeNum }),
         asSucc = Object.freeze({ vertex: destNum, weight: weight, num: edgeNum }),
         asPred = Object.freeze({ vertex: srcNum, weight: weight, num: edgeNum });
      graph.edges.push(edgeProps);
      graph.succLists[srcNum].push(asSucc);
      graph.predLists[destNum].push(asPred);
      graph.adjMatrix[srcNum][destNum] = weight;
      if (graph.isUndirected && srcNum != destNum) {
         graph.succLists[destNum].push(asPred);
         graph.predLists[srcNum].push(asSucc);
         graph.adjMatrix[destNum][srcNum] = weight;
      }
      if (calcIncMatrix) {
         graph.incMatrix[srcNum][edgeNum] = graph.isUndirected ? weight : -weight;
         graph.incMatrix[destNum][edgeNum] = weight;
      }
      if (edge.classList.contains('selected')) {
         graph.selectedEdges.push(edgeNum);
      }
      ++edgeNum;
   }
   for (let vNum = 0; vNum < nbVertices; ++vNum) {
      Object.freeze(graph.succLists[vNum]);
      Object.freeze(graph.predLists[vNum]);
   }
   Object.freeze(graph.succLists);
   Object.freeze(graph.predLists);
   Object.freeze(graph.adjMatrix);
   if (calcIncMatrix) {
      Object.freeze(graph.incMatrix);
   }
   Object.freeze(graph.selectedEdges);
   if ((flags & 1) == 1) {
      graph.verticesPos = new Array(nbVertices);
      for (const vertex of getAllVertices()) {
         const vNum = parseInt(vertex.getAttribute('data-num')), vCirc = vertex.querySelector('circle');
         graph.verticesPos[vNum] =
            Object.freeze({ x: parseFloat(vCirc.getAttribute('cx')), y: parseFloat(vCirc.getAttribute('cy')) });
      }
      Object.freeze(graph.verticesPos);
   }
   return Object.freeze(graph);
}


function formatArraysForVertices(rowNames, values) {
   let result = [[{ textCode: 'vertex' }]];
   for (let vertexNum = 0; vertexNum < values[0].length; ++vertexNum) {
      result[0].push(getVertexLabel(vertexNum));
   }
   for (let rowI = 0; rowI < rowNames.length; ++rowI) {
      result.push([rowNames[rowI]]);
      for (const val of values[rowI]) {
         result[rowI + 1].push(val === undefined ? "" : (val == null ? "—" : val));
      }
   }
   return result;
}

function formatBinaryTree(title, treeAsArray) {
   const result = [[title]];
   if (treeAsArray.length <= 0) {
      return result;
   }
   const nbRows = 1 << (31 - Math.clz32(treeAsArray.length));
   let colNum = 0;
   for (let pos = 0; pos < treeAsArray.length;) {
      const initialPos = pos, nbLeafs = nbRows >> colNum, colEndPos = Math.min(pos * 2 + 1, treeAsArray.length);
      ++colNum;
      for (; pos < colEndPos; ++pos) {
         const rowNum = (pos - initialPos) * nbLeafs;
         while (result.length <= rowNum) {
            result.push([]);
         }
         while (result[rowNum].length < colNum) {
            result[rowNum].push('');
         }
         result[rowNum].push(treeAsArray[pos]);
      }
   }
   return result;
}

function parentsToEdges(parents) {
   const edgesList = [];
   for (const vertexNum in parents) {
      if (parents[vertexNum] != null) {
         edgesList.push({ src: parents[vertexNum], dest: vertexNum });
      }
   }
   return edgesList;
}

function getEdgesFromBools(edgesToTake) {
   const edges = svgGraph.edges.querySelectorAll('.edge'), edgesList = [];
   for (let edgeNum = 0; edgeNum < edges.length && edgeNum < edgesToTake.length; ++edgeNum) {
      if (edgesToTake[edgeNum]) {
         edgesList.push({ num: edgeNum });
      }
   }
   return edgesList;
}

function getVerticesFromBools(verticesToTake) {
   const verticesList = [];
   for (let vertexNum = 0; vertexNum < verticesToTake.length; ++vertexNum) {
      if (verticesToTake[vertexNum]) {
         verticesList.push(vertexNum);
      }
   }
   return verticesList;
}


function countOperation(name, quantity) {
   quantity = quantity == undefined ? 1 : quantity;
   algoOperations[name] = algoOperations[name] ? algoOperations[name] + quantity : quantity;
}


function PriorityQueue(priorityFct, saveIndexes, opNames) {
   const data = [], dataIndexes = [];
   const assignValue = saveIndexes ? function(index, value) {
         data[index] = value;
         dataIndexes[value] = index;
      } : function(index, value) {
         data[index] = value;
      };
   const queue = {
      data: data,
      dataIndexes: dataIndexes,
      length: () => data.length,
      top: () => data[0],
      upgrade: function(pos, val) {
         ++pos;
         while (pos > 1 && priorityFct(val, data[(pos >>> 1) - 1])) {
            if (opNames != null) {
               countOperation(opNames[0]);
            }
            assignValue(pos - 1, data[(pos >>> 1) - 1]);
            pos >>>= 1;
         }
         assignValue(pos - 1, val);
      },
      downgrade: function(pos, val) {
         ++pos;
         while (pos << 1 <= data.length) {
            if (pos << 1 < data.length && priorityFct(data[pos << 1], data[(pos << 1) - 1])) {
               if (priorityFct(val, data[pos << 1])) {
                  break;
               }
               assignValue(pos - 1, data[pos << 1]);
               pos = (pos << 1) + 1;
            } else if (priorityFct(val, data[(pos << 1) - 1])) {
               break;
            } else {
               assignValue(pos - 1, data[(pos << 1) - 1]);
               pos <<= 1;
            }
            if (opNames != null) {
               countOperation(opNames[1]);
            }
         }
         assignValue(pos - 1, val);
      },
      push: function(val) {
         queue.upgrade(data.length, val);
      },
      pop: function() {
         const head = data[0], last = data.pop();
         if (data.length > 0) {
            queue.downgrade(0, last);
         }
         return head;
      },
   };
   return queue;
}


let algoGraph = null;

function runAlgorithm(fct, flags) {
   algoGraph = loadGraph(flags);
   algoOperations = {};
   algoStates = fct(algoGraph);
   algoGraph = null;
   initAlgorithmResult();
}
