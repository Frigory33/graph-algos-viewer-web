open Api


let myAlgo graph =
   let dataToShow = ref [] in
   let addDataToShow args =
      let vNumToVLabel vNum = JsonStr (getVertexLabel vNum)
      and edgeNumToEdgeDesc edgeNum =
         JsonStr (Printf.sprintf "%s→%s" (getVertexLabel graph.edges.(edgeNum).src) (getVertexLabel graph.edges.(edgeNum).dest))
      in
      let selVerts = JsonStr "Sommets sélectionnés" :: List.map vNumToVLabel graph.selectedVertices
      and selEdges = JsonStr "Arcs sélectionnés" :: List.map edgeNumToEdgeDesc graph.selectedEdges
      and params = JsonStr "Paramètres de l’état" :: args in
      dataToShow := [
            [("type", JsonStr "text"); ("value", JsonStr "Ce texte est retourné par l’algorithme.")];
            [("type", JsonStr "table"); ("value", JsonList [JsonList selVerts; JsonList selEdges; JsonList params])];
         ] :: !dataToShow;
   in

   countOperation "Opération";

   (* Écrivez votre algorithme ici *)

   addDataToShow [];

   List.rev !dataToShow


let _ =
   runAlgorithm myAlgo;
