#! /bin/env python3


class Graph:
   from collections import namedtuple
   EdgeProps = namedtuple('EdgeProps', ['src', 'dest', 'weight', 'num'])
   EdgeEnd = namedtuple('EdgeEnd', ['vertex', 'weight', 'num'])
   Position = namedtuple('Position', ['x', 'y'])

   def __init__(self, nbVertices, nbEdges, isUndirected, flags, namingConvType, namingConvFirst):
      self.nbVertices = nbVertices
      self.nbEdges = nbEdges
      self.isUndirected = isUndirected
      self.namingConvType = namingConvType
      self.namingConvFirst = namingConvFirst
      self.selectedVertices = []
      self.selectedEdges = []
      self.edges = [None] * nbEdges
      self.succLists = [[] for _ in range(nbVertices)]
      self.predLists = [[] for _ in range(nbVertices)]
      self.adjMatrix = [[0] * nbVertices for _ in range(nbVertices)]
      if flags & 2 == 2:
         self.incMatrix = [[0] * nbEdges for _ in range(nbVertices)]
      if flags & 1 == 1:
         self.verticesPos = [None] * nbVertices


def loadGraph():
   nbVertices, nbEdges, isUndirected, flags = map(int, input().split())
   namingConvType, namingConvFirst = map(int, input().split())
   graph = Graph(nbVertices, nbEdges, isUndirected != 0, flags, namingConvType, namingConvFirst)
   graph.selectedVertices = list(map(int, input().split()))
   graph.selectedEdges = list(map(int, input().split()))
   for edgeNum in range(nbEdges):
      data = input().split()
      srcNum, destNum = map(int, data[:2])
      weight = float(data[2])
      edgeProps = Graph.EdgeProps(srcNum, destNum, weight, edgeNum)
      asSucc = Graph.EdgeEnd(destNum, weight, edgeNum)
      asPred = Graph.EdgeEnd(srcNum, weight, edgeNum)
      graph.edges[edgeNum] = edgeProps
      graph.succLists[srcNum].append(asSucc)
      graph.predLists[destNum].append(asPred)
      graph.adjMatrix[srcNum][destNum] = weight
      if graph.isUndirected and srcNum != destNum:
         graph.succLists[destNum].append(asPred)
         graph.predLists[srcNum].append(asSucc)
         graph.adjMatrix[destNum][srcNum] = weight
      if flags & 2 == 2:
         graph.incMatrix[srcNum][edgeNum] = weight if graph.isUndirected else -weight
         graph.incMatrix[destNum][edgeNum] = weight
   if flags & 1 == 1:
      for vertexNum in range(nbVertices):
         graph.verticesPos[vertexNum] = Graph.Position(*map(float, input().split()))
   return graph


def getVertexLabel(vertexNum):
   global graph

   if vertexNum == None:
      return "—"
   elif vertexNum < 0:
      return ""

   nb = graph.namingConvFirst + vertexNum
   if graph.namingConvType == 0:
      return str(nb)

   def adjustLetterCode(code):
      if 3 <= graph.namingConvType <= 4 and code >= 17:
         return code + 1
      return code

   alphabetsData = [(10, "0"), (26, "A"), (26, "a"), (24, "Α"), (24, "α"), (32, "А"), (32, "а")]
   alphabetLength = alphabetsData[graph.namingConvType][0]
   firstLetterCode = ord(alphabetsData[graph.namingConvType][1])
   label = ''
   nb += 1
   while nb > 0:
      label = chr(firstLetterCode + adjustLetterCode((nb - 1) % alphabetLength)) + label
      nb = (nb - 1) // alphabetLength
   return label

def floatToStr(nb):
   from math import isfinite, inf
   if not isfinite(nb):
      return "∞" if nb == inf else "-∞" if nb == -inf else "?!NaN!?"
   return "{:g}".format(nb).replace(".", ",")


def formatArraysForVertices(rowNames, values):
   global graph
   row = [{ 'textCode': 'vertex' }]
   for vertexNum in range(graph.nbVertices):
      row.append(getVertexLabel(vertexNum))
   result = [row]
   for rowNum in range(len(rowNames)):
      row = [rowNames[rowNum]]
      for val in values[rowNum]:
         row.append("—" if val == None else val)
      result.append(row)
   return result

def formatBinaryTree(title, treeAsArray):
   result = [[title]]
   if len(treeAsArray) <= 0:
      return result
   nbRows = 1
   subTreeSize = len(treeAsArray)
   while subTreeSize > 1:
      nbRows <<= 1
      subTreeSize >>= 1
   colNum = 0
   pos = 0
   while pos < len(treeAsArray):
      initialPos = pos
      nbLeafs = nbRows >> colNum
      colEndPos = min(pos * 2 + 1, len(treeAsArray))
      colNum += 1
      while pos < colEndPos:
         rowNum = (pos - initialPos) * nbLeafs
         while len(result) <= rowNum:
            result.append([])
         while len(result[rowNum]) < colNum:
            result[rowNum].append("")
         result[rowNum].append(treeAsArray[pos])
         pos += 1
   return result

def parentsToEdges(parents):
   edgesList = []
   for vertexNum in range(len(parents)):
      if parents[vertexNum] != None and parents[vertexNum] >= 0:
         edgesList.append({ 'src': parents[vertexNum], 'dest': vertexNum })
   return edgesList

def getEdgesFromBools(edgesToTake):
   edgesList = []
   for edgeNum in range(len(edgesToTake)):
      if edgesToTake[edgeNum]:
         edgesList.append({ 'num': edgeNum })
   return edgesList

def getVerticesFromBools(verticesToTake):
   verticesList = []
   for vertexNum in range(len(verticesToTake)):
      if verticesToTake[vertexNum]:
         verticesList.append(vertexNum)
   return verticesList


def countOperation(name, quantity = 1):
   global operations
   if name in operations:
      operations[name] += quantity
   else:
      operations[name] = quantity


class PriorityQueue:
   def __init__(self, priorityFct, saveIndexes = False, opNames = None):
      self.priorityFct = priorityFct
      self.opNames = opNames
      self.data = []
      def assignValueNoIndexes(index, value):
         self.data[index] = value
      def assignValueWithIndexes(index, value):
         self.data[index] = value
         if value >= len(self.dataIndexes):
            self.dataIndexes.extend([-1] * (value - len(self.dataIndexes) + 1))
         self.dataIndexes[value] = index
      self.assignValue = assignValueNoIndexes
      if saveIndexes:
         self.dataIndexes = []
         self.assignValue = assignValueWithIndexes

   def length(self):
      return len(self.data)

   def top(self):
      return self.data[0]

   def upgrade(self, pos, val):
      pos += 1
      while pos > 1 and self.priorityFct(val, self.data[(pos >> 1) - 1]):
         if self.opNames != None:
            countOperation(self.opNames[0])
         self.assignValue(pos - 1, self.data[(pos >> 1) - 1])
         pos >>= 1
      self.assignValue(pos - 1, val)

   def downgrade(self, pos, val):
      pos += 1
      while pos << 1 <= self.length():
         if pos << 1 < self.length() and self.priorityFct(self.data[pos << 1], self.data[(pos << 1) - 1]):
            if self.priorityFct(val, self.data[pos << 1]):
               break
            self.assignValue(pos - 1, self.data[pos << 1])
            pos = (pos << 1) + 1
         elif self.priorityFct(val, self.data[(pos << 1) - 1]):
            break
         else:
            self.assignValue(pos - 1, self.data[(pos << 1) - 1])
            pos <<= 1
         if self.opNames != None:
            countOperation(self.opNames[1])
      self.assignValue(pos - 1, val)

   def push(self, val):
      self.data.append(val)
      self.upgrade(self.length() - 1, val)

   def pop(self):
      head = self.data[0]
      last = self.data.pop()
      if self.length() > 0:
         self.downgrade(0, last)
      return head


def runAlgorithm(fct):
   global operations, graph
   graph = loadGraph()
   operations = {}

   def processDataToShow(dataToShow):
      def edgesToDicts(elemsList):
         for elemNum in range(len(elemsList)):
            elem = elemsList[elemNum]
            if isinstance(elem, Graph.EdgeProps) or isinstance(elem, Graph.EdgeEnd):
               elemsList[elemNum] = elem._asdict()
      def formatNumbers(elemsList):
         from math import isfinite, inf
         if isinstance(elemsList, list):
            for elemNum in range(len(elemsList)):
               elem = elemsList[elemNum]
               if isinstance(elem, float) and not isfinite(elem):
                  elemsList[elemNum] = "∞" if elem == inf else "-∞" if elem == -inf else "?!NaN!?"
      for state in dataToShow:
         for data in state:
            if data["type"] == "colorization" and isinstance(data["target"], list):
               edgesToDicts(data["target"])
            elif data["type"] == "table" and isinstance(data["value"], list):
               for row in data["value"]:
                  formatNumbers(row)
            elif data["type"] in ["vertexValues", "edgeValues"]:
               formatNumbers(data["value"])

   from json import dumps
   dataToShow = fct(graph)
   processDataToShow(dataToShow)
   print(dumps({ "states": dataToShow, "operations": operations }, ensure_ascii = False))
