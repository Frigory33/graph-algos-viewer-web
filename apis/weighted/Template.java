import java.util.*;


public class Template extends Api {
   private Graph graph;
   private final List<List<Map<String, Object>>> dataToShow = new ArrayList<>();

   private void addDataToShow(Object... args) {
      List<Object> selVerts = new ArrayList<>(), selEdges = new ArrayList<>(), params = new ArrayList<>();
      selVerts.add("Sommets sélectionnés");
      for (int vNum: graph.selectedVertices) {
         selVerts.add(getVertexLabel(vNum));
      }
      selEdges.add("Arcs sélectionnés");
      for (int edgeNum: graph.selectedEdges) {
         selEdges.add(getVertexLabel(graph.edges[edgeNum].src) + "→" + getVertexLabel(graph.edges[edgeNum].dest));
      }
      params.add("Paramètres de l’état");
      for (var arg: args) {
         params.add(arg);
      }
      dataToShow.add(List.of(
            Map.ofEntries(Map.entry("type", "text"), Map.entry("value", "Ce texte est retourné par l’algorithme.")),
            Map.ofEntries(Map.entry("type", "table"), Map.entry("value", List.of(selVerts, selEdges, params)))
         ));
   }

   private List<List<Map<String, Object>>> myAlgo(Graph graphArg) {
      graph = graphArg;

      countOperation("Opération");

      // Écrivez votre algorithme ici

      addDataToShow();

      return dataToShow;
   }


   public static void main(String[] args) {
      Template templ = new Template();
      runAlgorithm(templ::myAlgo);
   }
}
