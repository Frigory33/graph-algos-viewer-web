import java.util.*;
import java.util.function.*;


public abstract class Api {
   public static class Graph {
      public static class EdgeProps {
         public final int src, dest;
         public final double weight;
         public final int num;
         public EdgeProps(int src, int dest, double weight, int num) {
            this.src = src;
            this.dest = dest;
            this.weight = weight;
            this.num = num;
         }
      }
      public static class EdgeEnd {
         public final int vertex;
         public final double weight;
         public final int num;
         public EdgeEnd(int vertex, double weight, int num) {
            this.vertex = vertex;
            this.weight = weight;
            this.num = num;
         }
      }
      public static class Position {
         public final double x, y;
         public Position(double x, double y) {
            this.x = x;
            this.y = y;
         }
      }

      public final int nbVertices, nbEdges;
      public final boolean isUndirected;
      public final int namingConvType, namingConvFirst;
      public int[] selectedVertices, selectedEdges;
      public final EdgeProps[] edges;
      public final EdgeEnd[][] succLists, predLists;
      public final double[][] adjMatrix, incMatrix;
      public final Position[] verticesPos;
      public Graph(int nbVertices, int nbEdges, boolean isUndirected, int flags, int namingConvType, int namingConvFirst) {
         this.nbVertices = nbVertices;
         this.nbEdges = nbEdges;
         this.isUndirected = isUndirected;
         this.namingConvType = namingConvType;
         this.namingConvFirst = namingConvFirst;
         edges = new EdgeProps[nbEdges];
         succLists = new EdgeEnd[nbVertices][];
         predLists = new EdgeEnd[nbVertices][];
         adjMatrix = new double[nbVertices][nbVertices];
         incMatrix = (flags & 2) == 2 ? new double[nbVertices][nbEdges] : null;
         verticesPos = (flags & 1) == 1 ? new Position[nbVertices] : null;
      }
   }


   private static Graph graph;
   private static Map<String, Integer> operations;

   private static final int[] alphabetsLengths = { 10, 26, 26, 24, 24, 32, 32 };
   private static final String[] alphabetsFirstLetters = { "0", "A", "a", "Α", "α", "А", "а" };


   public static Graph loadGraph() {
      Scanner input = new Scanner(System.in);
      String[] components;

      components = input.nextLine().split("\\s+");
      int nbVertices = Integer.parseInt(components[0]), nbEdges = Integer.parseInt(components[1]),
         isUndirected = Integer.parseInt(components[2]), flags = Integer.parseInt(components[3]);
      components = input.nextLine().split("\\s+");
      int namingConvType = Integer.parseInt(components[0]), namingConvFirst = Integer.parseInt(components[1]);

      Graph graph = new Graph(nbVertices, nbEdges, isUndirected != 0, flags, namingConvType, namingConvFirst);

      int[][] selected = new int[2][];
      for (int selTypeNum = 0; selTypeNum < selected.length; ++selTypeNum) {
         String line = input.nextLine();
         components = line.isEmpty() ? new String[0] : line.split("\\s+");
         selected[selTypeNum] = new int[components.length];
         for (int selNum = 0; selNum < components.length; ++selNum) {
            selected[selTypeNum][selNum] = Integer.parseInt(components[selNum]);
         }
      }
      graph.selectedVertices = selected[0];
      graph.selectedEdges = selected[1];

      @SuppressWarnings("unchecked")
      ArrayList<Graph.EdgeEnd>[] succLists = new ArrayList[nbVertices], predLists = new ArrayList[nbVertices];
      for (int vNum = 0; vNum < nbVertices; ++vNum) {
         succLists[vNum] = new ArrayList<>();
         predLists[vNum] = new ArrayList<>();
      }
      for (int edgeNum = 0; edgeNum < nbEdges; ++edgeNum) {
         components = input.nextLine().split("\\s+");
         int srcNum = Integer.parseInt(components[0]), destNum = Integer.parseInt(components[1]);
         double weight = Double.parseDouble(components[2]);
         Graph.EdgeProps edgeProps = new Graph.EdgeProps(srcNum, destNum, weight, edgeNum);
         Graph.EdgeEnd asSucc = new Graph.EdgeEnd(destNum, weight, edgeNum), asPred = new Graph.EdgeEnd(srcNum, weight, edgeNum);
         graph.edges[edgeNum] = edgeProps;
         succLists[srcNum].add(asSucc);
         predLists[destNum].add(asPred);
         graph.adjMatrix[srcNum][destNum] = weight;
         if (graph.isUndirected && srcNum != destNum) {
            succLists[destNum].add(asPred);
            predLists[srcNum].add(asSucc);
            graph.adjMatrix[destNum][srcNum] = weight;
         }
         if ((flags & 2) == 2) {
            graph.incMatrix[srcNum][edgeNum] = graph.isUndirected ? weight : -weight;
            graph.incMatrix[destNum][edgeNum] = weight;
         }
      }
      for (int vNum = 0; vNum < nbVertices; ++vNum) {
         graph.succLists[vNum] = succLists[vNum].toArray(new Graph.EdgeEnd[0]);
         graph.predLists[vNum] = predLists[vNum].toArray(new Graph.EdgeEnd[0]);
      }

      if ((flags & 1) == 1) {
         for (int vertexNum = 0; vertexNum < nbVertices; ++vertexNum) {
            components = input.nextLine().split("\\s+");
            graph.verticesPos[vertexNum] =
               new Graph.Position(Double.parseDouble(components[0]), Double.parseDouble(components[1]));
         }
      }

      return graph;
   }


   public static String getVertexLabel(int vertexNum) {
      if (vertexNum < 0) {
         return vertexNum == -1 ? "—" : "";
      }

      int nb = graph.namingConvFirst + vertexNum;
      if (graph.namingConvType == 0) {
         return "" + nb;
      }

      IntUnaryOperator adjustLetterCode = code -> {
         if ((graph.namingConvType == 3 || graph.namingConvType == 4) && code >= 17) {
            return code + 1;
         }
         return code;
      };

      int alphabetLength = alphabetsLengths[graph.namingConvType],
         firstLetterCode = alphabetsFirstLetters[graph.namingConvType].codePointAt(0);

      StringBuilder label = new StringBuilder();
      ++nb;
      while (nb > 0) {
         int letterCode = firstLetterCode + adjustLetterCode.applyAsInt((nb - 1) % alphabetLength);
         label.appendCodePoint(letterCode);
         nb = (nb - 1) / alphabetLength;
      }
      return label.reverse().toString();
   }

   public static String floatToStr(double nb) {
      if (!Double.isFinite(nb)) {
         return nb == Double.POSITIVE_INFINITY ? "∞" : (nb == Double.NEGATIVE_INFINITY ? "-∞" : "?!NaN!?");
      }
      String str = "" + nb;
      str = str.endsWith(".0") ? str.substring(0, str.length() - 2) : str;
      return str.replace('.', ',');
   }


   public static List<List<Object>> formatArraysForVertices(String[] rowNames, List<List<?>> values) {
      List<Object> row = new ArrayList<>();
      row.add(Map.of("textCode", "vertex"));
      for (int vertexNum = 0; vertexNum < graph.nbVertices; ++vertexNum) {
         row.add(getVertexLabel(vertexNum));
      }
      List<List<Object>> result = new ArrayList<>();
      result.add(row);
      for (int rowNum = 0; rowNum < rowNames.length; ++rowNum) {
         row = new ArrayList<>();
         row.add(rowNames[rowNum]);
         for (var val: values.get(rowNum)) {
            row.add(val);
         }
         result.add(row);
      }
      return result;
   }

   public static List<List<Object>> formatBinaryTree(String title, List<?> treeAsArray) {
      List<List<Object>> result = new ArrayList<>();
      result.add(new ArrayList<>());
      result.get(0).add(title);
      if (treeAsArray.isEmpty()) {
         return result;
      }
      int nbRows = 1, subTreeSize = treeAsArray.size();
      while (subTreeSize > 1) {
         nbRows <<= 1;
         subTreeSize >>= 1;
      }
      int colNum = 0;
      for (int pos = 0; pos < treeAsArray.size();) {
         int initialPos = pos, nbLeafs = nbRows >> colNum, colEndPos = Math.min(pos * 2 + 1, treeAsArray.size());
         ++colNum;
         for (; pos < colEndPos; ++pos) {
            int rowNum = (pos - initialPos) * nbLeafs;
            while (result.size() <= rowNum) {
               result.add(new ArrayList<>());
            }
            var row = result.get(rowNum);
            while (row.size() < colNum) {
               row.add("");
            }
            row.add(treeAsArray.get(pos));
         }
      }
      return result;
   }

   public static List<Object> parentsToEdges(int[] parents) {
      List<Object> edgesList = new ArrayList<>();
      for (int vertexNum = 0; vertexNum < parents.length; ++vertexNum) {
         if (parents[vertexNum] >= 0) {
            edgesList.add(Map.ofEntries(Map.entry("src", parents[vertexNum]), Map.entry("dest", vertexNum)));
         }
      }
      return edgesList;
   }

   public static List<Object> getEdgesFromBools(boolean[] edgesToTake) {
      List<Object> edgesList = new ArrayList<>();
      for (int edgeNum = 0; edgeNum < edgesToTake.length; ++edgeNum) {
         if (edgesToTake[edgeNum]) {
            edgesList.add(Map.ofEntries(Map.entry("num", edgeNum)));
         }
      }
      return edgesList;
   }

   public static List<Object> getVerticesFromBools(boolean[] verticesToTake) {
      List<Object> verticesList = new ArrayList<>();
      for (int vertexNum = 0; vertexNum < verticesToTake.length; ++vertexNum) {
         if (verticesToTake[vertexNum]) {
            verticesList.add(vertexNum);
         }
      }
      return verticesList;
   }


   public static void countOperation(String name) {
      countOperation(name, 1);
   }
   public static void countOperation(String name, int quantity) {
      operations.merge(name, quantity, (q1, q2) -> q1 + q2);
   }


   public static class PriorityQueue {
      private final BiPredicate<Integer, Integer> priorityFct;
      private final String[] opNames;
      public final ArrayList<Integer> data = new ArrayList<>(), dataIndexes = new ArrayList<>();

      public PriorityQueue(BiPredicate<Integer, Integer> priorityFct) {
         this(priorityFct, null);
      }
      public PriorityQueue(BiPredicate<Integer, Integer> priorityFct, String[] opNames) {
         this.priorityFct = priorityFct;
         this.opNames = opNames;
      }

      public int length() {
         return data.size();
      }
      public int top() {
         return data.get(0);
      }

      public void upgrade(int pos, int val) {
         ++pos;
         while (pos > 1 && priorityFct.test(val, data.get((pos >> 1) - 1))) {
            if (opNames != null) {
               countOperation(opNames[0]);
            }
            assignValue(pos - 1, data.get((pos >> 1) - 1));
            pos >>= 1;
         }
         assignValue(pos - 1, val);
      }

      public void downgrade(int pos, int val) {
         ++pos;
         while (pos << 1 <= data.size()) {
            if (pos << 1 < data.size() && priorityFct.test(data.get(pos << 1), data.get((pos << 1) - 1))) {
               if (priorityFct.test(val, data.get(pos << 1))) {
                  break;
               }
               assignValue(pos - 1, data.get(pos << 1));
               pos = (pos << 1) + 1;
            } else if (priorityFct.test(val, data.get((pos << 1) - 1))) {
               break;
            } else {
               assignValue(pos - 1, data.get((pos << 1) - 1));
               pos <<= 1;
            }
            if (opNames != null) {
               countOperation(opNames[1]);
            }
         }
         assignValue(pos - 1, val);
      }

      public void push(int val) {
         data.add(val);
         upgrade(data.size() - 1, val);
      }

      public int pop() {
         int head = top(), last = data.remove(data.size() - 1);
         if (data.size() > 0) {
            downgrade(0, last);
         }
         return head;
      }

      protected void assignValue(int index, int value) {
         data.set(index, value);
         while (value >= dataIndexes.size()) {
            dataIndexes.add(-1);
         }
         dataIndexes.set(value, index);
      }
   }


   private static void writeComma(StringBuilder jsonOut, boolean[] isFirst) {
      if (isFirst[0]) {
         isFirst[0] = false;
      } else {
         jsonOut.append(", ");
      }
   }

   private static String quoteString(String str) {
      StringBuilder result = new StringBuilder();
      result.append('"');
      for (int crtNum = 0; crtNum < str.length(); ++crtNum) {
         char crt = str.charAt(crtNum);
         if (crt == '"' || crt == '\\') {
            result.append('\\');
         }
         result.append(crt);
      }
      result.append('"');
      return result.toString();
   }

   @SuppressWarnings("unchecked")
   private static void anyToJson(StringBuilder jsonOut, Object data) {
      if (data == null) {
         jsonOut.append("null");
      } else if (data instanceof Number || data instanceof Boolean) {
         if (data instanceof Double nb && !Double.isFinite(nb)) {
            anyToJson(jsonOut, nb == Double.POSITIVE_INFINITY ? "∞" : (nb == Double.NEGATIVE_INFINITY ? "-∞" : "?!NaN!?"));
         }
         jsonOut.append(data);
      } else if (data instanceof Map mapOfData) {
         jsonOut.append("{ ");
         boolean[] isFirst = { true };
         for (var dataEntryObj: mapOfData.entrySet()) {
            var dataEntry = (Map.Entry<String, Object>)dataEntryObj;
            writeComma(jsonOut, isFirst);
            jsonOut.append(quoteString(dataEntry.getKey())).append(": ");
            anyToJson(jsonOut, dataEntry.getValue());
         }
         jsonOut.append(" }");
      } else if (data instanceof List arrayOfData) {
         jsonOut.append("[");
         boolean[] isFirst = { true };
         for (var subData: arrayOfData) {
            writeComma(jsonOut, isFirst);
            anyToJson(jsonOut, subData);
         }
         jsonOut.append("]");
      } else if (data instanceof Graph.EdgeProps ep) {
         jsonOut.append("{ \"num\": ").append(ep.num).append(" }");
      } else if (data instanceof Graph.EdgeEnd ee) {
         jsonOut.append("{ \"num\": ").append(ee.num).append(" }");
      } else {
         jsonOut.append(quoteString(data.toString()));
      }
   }

   public static void runAlgorithm(Function<Graph, List<List<Map<String, Object>>>> fct) {
      graph = loadGraph();
      operations = new HashMap<>();

      var dataToShow = fct.apply(graph);

      StringBuilder jsonOut = new StringBuilder();
      jsonOut.append("{ \"states\": [");
      boolean[] isFirst = { true };
      for (var state: dataToShow) {
         writeComma(jsonOut, isFirst);
         jsonOut.append("[");
         boolean[] isFirst2 = { true };
         for (var data: state) {
            writeComma(jsonOut, isFirst2);
            jsonOut.append("{ ");
            boolean[] isFirst3 = { true };
            data.forEach((key, val) -> {
               writeComma(jsonOut, isFirst3);
               jsonOut.append(quoteString(key)).append(": ");
               anyToJson(jsonOut, val);
            });
            jsonOut.append(" }");
         }
         jsonOut.append("]");
      }
      jsonOut.append("], \"operations\": { ");
      isFirst = new boolean[] { true };
      for (var op: operations.entrySet()) {
         writeComma(jsonOut, isFirst);
         jsonOut.append(quoteString(op.getKey())).append(": ").append(op.getValue());
      }
      jsonOut.append(" } }");
      System.out.println(jsonOut);
   }
}
