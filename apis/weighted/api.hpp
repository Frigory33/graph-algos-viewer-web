#ifndef GRAPH_API_HPP
#define GRAPH_API_HPP


#include <string>
#include <vector>
#include <map>
#include <functional>
#include <any>


#define whole(container) container.begin(), container.end()


struct Graph {
   struct EdgeProps {
      int src, dest;
      double weight;
      int num;
   };
   struct EdgeEnd {
      int vertex;
      double weight;
      int num;
   };
   struct Position {
      double x, y;
   };

   int nbVertices = 0, nbEdges = 0;
   bool isUndirected = false;
   int namingConvType = 0, namingConvFirst = 1;
   std::vector<int> selectedVertices, selectedEdges;
   std::vector<EdgeProps> edges;
   std::vector<std::vector<EdgeEnd>> succLists, predLists;
   std::vector<std::vector<double>> adjMatrix, incMatrix;
   std::vector<Position> verticesPos;

   Graph() = default;
   Graph(int nbVertices, int nbEdges, bool isUndirected, int namingConvType, int namingConvFirst)
      : nbVertices(nbVertices), nbEdges(nbEdges), isUndirected(isUndirected)
      , namingConvType(namingConvType), namingConvFirst(namingConvFirst)
      , edges(nbEdges), succLists(nbVertices), predLists(nbVertices)
      , adjMatrix(nbVertices, std::vector<double>(nbVertices)) {
   }
};


Graph loadGraph();


typedef std::vector<std::any> AnyVector;
typedef std::map<std::string, std::any> Dict;
typedef std::vector<AnyVector> Table;


std::string getVertexLabel(int vertexNum);
std::string floatToStr(double nb);

Table formatArraysForVertices(std::vector<std::string> const & rowNames, Table const & values);
Table formatBinaryTree(std::string const & title, AnyVector const & treeAsArray);
AnyVector parentsToEdges(std::vector<int> const & parents);
AnyVector getEdgesFromBools(std::vector<bool> const & edgesToTake);
AnyVector getVerticesFromBools(std::vector<bool> const & verticesToTake);


void countOperation(std::string const & name, int quantity = 1);


class PriorityQueue {
      std::function<bool(int, int)> priorityFct;
      std::string * opNames;
   public:
      std::vector<int> data, dataIndexes;

      PriorityQueue(std::function<bool(int, int)> priorityFct, std::string * opNames = nullptr)
         : priorityFct(priorityFct), opNames(opNames) {
      }
      int length() {
         return data.size();
      }
      int top() {
         return data[0];
      }
      void upgrade(int pos, int val);
      void downgrade(int pos, int val);
      void push(int val);
      int pop();
   protected:
      void assignValue(int index, int value);
};


typedef std::vector<std::vector<Dict>> DataToShow;


void runAlgorithm(std::function<DataToShow(Graph const &)> fct);


#endif
