function myAlgo(graph) {
   let dataToShow = [];
   function addDataToShow(...args) {
      function edgeNumToEdgeDesc(edgeNum) {
         return getVertexLabel(graph.edges[edgeNum].src) + "→" + getVertexLabel(graph.edges[edgeNum].dest);
      }
      const tableContents = [
         ["Sommets sélectionnés", ...graph.selectedVertices.map(getVertexLabel)],
         ["Arcs sélectionnés", ...graph.selectedEdges.map(edgeNumToEdgeDesc)],
         ["Paramètres de l’état", ...args],
      ];
      dataToShow.push([
            { type: 'text', value: "Ce texte est retourné par l’algorithme." },
            { type: 'table', value: tableContents },
         ]);
   }

   countOperation("Opération");

   // Écrivez votre algorithme ici

   addDataToShow();

   return dataToShow;
}


runAlgorithm(myAlgo);
