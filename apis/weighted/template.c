#include "api.h"


static Graph const * graph;
static DynArr(Any) dataToShow = {};


static void addDataToShow(DynArr(Any) args)
{
   DynArr(Any) selVerts = {}, selEdges = {}, stateArgs = {};
   dynarr_push(&selVerts, Any("Sommets sélectionnés"));
   for_slice (int, selV, graph->selectedVertices) {
      dynarr_push(&selVerts, Any(getVertexLabel(selV)));
   }
   dynarr_push(&selEdges, Any("Arcs sélectionnés"));
   for_slice (int, selE, graph->selectedEdges) {
      EdgeProps edge = graph->edges[selE];
      dynarr_push(&selEdges, Any(formatStr("%s→%s", getVertexLabel(edge.src), getVertexLabel(edge.dest))));
   }
   dynarr_push(&stateArgs, Any("Paramètres de l’état"));
   dynarr_cat(&stateArgs, args);
   dynarr_push(&dataToShow, Any(slice_build(Any,
         Any(dict_build(Any, { "type", Any("text") }, { "value", Any("Ce texte est retourné par l’algorithme.") })),
         Any(dict_build(Any, { "type", Any("table") },
            { "value", Any(slice_build(Any, Any(selVerts), Any(selEdges), Any(stateArgs))) })),
      )));
}

Slice(Any) myAlgo(Graph const * graphArg)
{
   graph = graphArg;

   countOperation("Opération");

   // Écrivez votre algorithme ici

   addDataToShow((DynArr(Any)){});

   return slice_on(Any, dataToShow);
}


int main()
{
   runAlgorithm(myAlgo);
}
