#! /bin/env python3


from api import *


def myAlgo(graph):
   dataToShow = []
   def addDataToShow(*args):
      def edgeNumToEdgeDesc(edgeNum):
         return "{}→{}".format(*map(getVertexLabel, [graph.edges[edgeNum].src, graph.edges[edgeNum].dest]))
      tableContents = [
         ["Sommets sélectionnés", *map(getVertexLabel, graph.selectedVertices)],
         ["Arcs sélectionnés", *map(edgeNumToEdgeDesc, graph.selectedEdges)],
         ["Paramètres de l’état", *args],
      ]
      dataToShow.append([
            { "type": "text", "value": "Ce texte est retourné par l’algorithme." },
            { "type": "table", "value": tableContents },
         ]);

   countOperation("Opération")

   # Écrivez votre algorithme ici

   addDataToShow()

   return dataToShow


runAlgorithm(myAlgo)
