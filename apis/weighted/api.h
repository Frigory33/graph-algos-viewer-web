#ifndef GRAPH_API_H
#define GRAPH_API_H


#include <handymacros.h>


typedef struct EdgeProps {
   int src, dest;
   double weight;
   int num;
} EdgeProps;

typedef struct EdgeEnd {
   int vertex;
   double weight;
   int num;
} EdgeEnd;

typedef struct Position {
   double x, y;
} Position;


typedef enum TypeId {
   NONE, OTHER_ANY, BOOLEAN, INT, FLOAT, STRING, EDGE_PROPS, EDGE_END, SLICE, DICT, PTR,
} TypeId;

typedef struct Any Any, AnyT;

DeclareDynArr(int)
DeclareDynArr(EdgeEnd)
DeclareSlice(Any)
DeclareDynArr(Any)
typedef struct DictEntry(Any) DictEntry(Any);
DeclareSlice(DictEntry(Any))
DeclareDynArr(DictEntry(Any))

struct Any {
   TypeId type;
   union {
      Any * a;
      bool b;
      int i;
      double f;
      char const * s;
      EdgeProps ep;
      EdgeEnd ee;
      Slice(Any) sli;
      Slice(DictEntry_Any) dic;
      void * ptr;
   };
   #ifdef __cplusplus
      Any(Any * a): type(OTHER_ANY), a(a) {}
      Any(bool b): type(BOOLEAN), b(b) {}
      Any(int i): type(INT), i(i) {}
      Any(double f): type(FLOAT), f(f) {}
      Any(char const * s): type(STRING), s(s) {}
      Any(EdgeProps ep): type(EDGE_PROPS), ep(ep) {}
      Any(EdgeEnd ee): type(EDGE_END), ee(ee) {}
      Any(Slice(Any) sli): type(SLICE), sli(sli) {}
      Any(Slice(DictEntry_Any) dic): type(DICT), dic(dic) {}
      Any(void * ptr): type(PTR), ptr(ptr) {}
   #endif
};

DeclareDictEntry(Any)

#ifndef __cplusplus
   #define Any(val) \
      _Generic(val, \
         Any *: otherAny, \
         bool: boolAny, int: intAny, double: floatAny, \
         char *: stringAny, char const *: stringAny, \
         EdgeProps: epAny, EdgeEnd: eeAny, \
         Slice(Any): sliceAny, DynArr(Any): dynarrAny, \
         Slice(DictEntry_Any): dictAny, \
         default: ptrAny \
      )(val)
#endif


typedef struct Graph {
   int nbVertices, nbEdges;
   bool isUndirected;
   int namingConvType, namingConvFirst;
   char const * * vertexLabels;
   DynArr(int) selectedVertices, selectedEdges;
   EdgeProps * edges;
   DynArr(EdgeEnd) * succLists, * predLists;
   double * * adjMatrix, * * incMatrix;
   Position * verticesPos;
} Graph;


typedef struct PriorityQueue PriorityQueue;
struct PriorityQueue {
   bool (* priorityFct)(Any val1, Any val2);
   char const * * opNames;
   DynArr(Any) data;
   DynArr(int) dataIndexes;
   void (* assignValue)(PriorityQueue * pq, int index, Any value);
};


#ifdef __cplusplus
   #define otherAny Any
   #define boolAny Any
   #define intAny Any
   #define floatAny Any
   #define stringAny Any
   #define epAny Any
   #define eeAny Any
   #define sliceAny Any
   #define dynarrAny Any
   #define dictAny Any
   #define ptrAny Any
#else
   static inline Any otherAny(Any * a) { return (Any){ OTHER_ANY, .a = a }; }
   static inline Any boolAny(bool b) { return (Any){ BOOLEAN, .b = b }; }
   static inline Any intAny(int i) { return (Any){ INT, .i = i }; }
   static inline Any floatAny(double f) { return (Any){ FLOAT, .f = f }; }
   static inline Any stringAny(char const * s) { return (Any){ STRING, .s = s }; }
   static inline Any epAny(EdgeProps ep) { return (Any){ EDGE_PROPS, .ep = ep }; }
   static inline Any eeAny(EdgeEnd ee) { return (Any){ EDGE_END, .ee = ee }; }
   static inline Any sliceAny(Slice(Any) sli) { return (Any){ SLICE, .sli = sli }; }
   static inline Any dynarrAny(DynArr(Any) arr) { return (Any){ SLICE, .sli = slice_on(Any, arr) }; }
   static inline Any dictAny(Slice(DictEntry_Any) dic) { return (Any){ DICT, .dic = dic }; }
   static inline Any ptrAny(void * ptr) { return (Any){ PTR, .ptr = ptr }; }
#endif

void destroyAny(Any any);


void addString(char * str);
char const * formatStr(char const * format, ...);
char const * concatStrings(char const * strs[]);


Graph * loadGraph();
void destroyGraph(Graph * graph);


char const * getVertexLabel(int vertexNum);
char const * floatToStr(double nb);

Any formatArraysForVertices(char const * rowNames[], Slice(Any) values);
Any formatBinaryTree(char const * title, Slice(Any) treeAsArray);
Any parentsToEdges(int const parents[]);
Any getEdgesFromBools(bool const edgesToTake[]);
Any getVerticesFromBools(bool const verticesToTake[]);


void countOperations(char const * name, int quantity, ...);
#define countOperation(...) countOperations(__VA_ARGS__, 1)


PriorityQueue PQ_create(bool (* priorityFct)(Any val1, Any val2), bool saveIndexes, char const * * opNames);
void PQ_destroy(PriorityQueue * pq);
void PQ_upgrade(PriorityQueue * pq, int pos, Any val);
void PQ_downgrade(PriorityQueue * pq, int pos, Any val);
void PQ_push(PriorityQueue * pq, Any val);
Any PQ_pop(PriorityQueue * pq);


void runAlgorithm(Slice(Any) (* fct)(Graph const * graph));


#endif
