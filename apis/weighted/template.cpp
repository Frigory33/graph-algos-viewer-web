#include "api.hpp"

#include <sstream>


DataToShow myAlgo(Graph const & graph)
{
   DataToShow dataToShow;
   auto addDataToShow = [&](AnyVector const & args = {}) {
      AnyVector selVerts = { "Sommets sélectionnés" }, selEdges = { "Arcs sélectionnés" },
         params = { "Paramètres de l’état" };
      std::transform(whole(graph.selectedVertices), std::back_inserter(selVerts), &getVertexLabel);
      for (int edgeNum: graph.selectedEdges) {
         std::ostringstream buf;
         buf << getVertexLabel(graph.edges.at(edgeNum).src) << "→" << getVertexLabel(graph.edges.at(edgeNum).dest);
         selEdges.push_back(buf.str());
      }
      std::copy(whole(args), std::back_inserter(params));
      dataToShow.push_back({
            { { "type", "text" }, { "value", "Ce texte est retourné par l’algorithme." } },
            { { "type", "table" }, { "value", Table{ selVerts, selEdges, params } } },
         });
   };

   countOperation("Opération");

   // Écrivez votre algorithme ici

   addDataToShow();

   return dataToShow;
}


int main()
{
   runAlgorithm(myAlgo);
}
