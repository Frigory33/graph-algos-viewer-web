#include "api.h"

#include <locale.h>
#include <stdio.h>
#include <uchar.h>
#include <math.h>


static int nbAllocStrs = 0, nbUsedStrs = 0;
static char * * strs = nullptr;

static Graph * graph = nullptr;
static DynArr(DictEntry_Any) operations = {};


void addString(char * str)
{
   if (nbUsedStrs >= nbAllocStrs) {
      nbAllocStrs = nbAllocStrs <= 0 ? 4 : nbAllocStrs * 2;
      strs = (char * *)realloc(strs, nbAllocStrs * sizeof(*strs));
   }
   strs[nbUsedStrs] = str;
   ++nbUsedStrs;
}

static void clearStrings()
{
   for_range (int, strNum, 0, < nbUsedStrs, ++) {
      free(strs[strNum]);
   }
   free(strs);
   strs = nullptr;
}

char const * formatStr(char const * format, ...)
{
   va_list args;
   va_start(args, format);
   int len = vsnprintf(nullptr, 0, format, args) + 1;
   va_end(args);
   char * result = (char *)malloc(len * sizeof(*result));
   va_start(args, format);
   vsnprintf(result, len, format, args);
   va_end(args);
   addString(result);
   return result;
}

char const * concatStrings(char const * strs[])
{
   int len = 1;
   for_array_until (char const *, str, strs, nullptr) {
      len += strlen(str);
   }
   char * result = (char *)malloc(len * sizeof(*result));
   int pos = 0;
   for_array_until (char const *, str, strs, nullptr) {
      pos += sprintf(result + pos, "%s", str);
   }
   addString(result);
   return result;
}


void destroyAny(Any any)
{
   switch (any.type) {
      case SLICE:
         for_slice (Any, subAny, any.sli) {
            destroyAny(subAny);
         }
         free(any.sli.data);
         break;
      case DICT:
         for_slice (DictEntry_Any, ent, any.dic) {
            destroyAny(ent.val);
         }
         free(any.dic.data);
         break;
      default: ;
   }
}


Graph * loadGraph()
{
   setlocale(LC_CTYPE, "fr_FR.utf8");

   Graph * graph = (Graph *)malloc(sizeof(*graph));
   *graph = (Graph){};
   int isUndirected = 0, flags = 0;
   scanf("%d %d %d %d%*c", &graph->nbVertices, &graph->nbEdges, &isUndirected, &flags);
   graph->isUndirected = isUndirected != 0;
   scanf("%d %d%*c", &graph->namingConvType, &graph->namingConvFirst);
   graph->vertexLabels = (char const * *)calloc(graph->nbVertices, sizeof(*graph->vertexLabels));

   DynArr(int) * selectionArrays[] = { &graph->selectedVertices, &graph->selectedEdges };
   for_array (DynArr(int) *, selArr, selectionArrays) {
      char buf[2];
      if (scanf("%1[\n]", buf) == 0) {
         int nbRead;
         char crt;
         do {
            int num;
            nbRead = scanf("%d%c", &num, &crt);
            dynarr_push(selArr, num);
         } while (nbRead >= 2 && crt != '\n');
      }
   }

   if (graph->nbEdges > 0) {
      graph->edges = (EdgeProps *)malloc(graph->nbEdges * sizeof(*graph->edges));
   }
   if (graph->nbVertices > 0) {
      graph->succLists = (DynArr(EdgeEnd) *)calloc(graph->nbVertices, sizeof(*graph->succLists));
      graph->predLists = (DynArr(EdgeEnd) *)calloc(graph->nbVertices, sizeof(*graph->predLists));
      graph->adjMatrix = (double * *)malloc(graph->nbVertices * sizeof(*graph->adjMatrix));
      graph->adjMatrix[0] = (double *)calloc(graph->nbVertices * graph->nbVertices, sizeof(**graph->adjMatrix));
      for (int vertexNum = 1; vertexNum < graph->nbVertices; ++vertexNum) {
         graph->adjMatrix[vertexNum] = graph->adjMatrix[vertexNum - 1] + graph->nbVertices;
      }
   }
   bool calcIncMatrix = (flags & 2) == 2;
   if (calcIncMatrix && graph->nbVertices * graph->nbEdges > 0) {
      graph->incMatrix = (double * *)malloc(graph->nbVertices * sizeof(*graph->incMatrix));
      graph->incMatrix[0] = (double *)calloc(graph->nbVertices * graph->nbEdges, sizeof(**graph->incMatrix));
      for (int vertexNum = 1; vertexNum < graph->nbVertices; ++vertexNum) {
         graph->incMatrix[vertexNum] = graph->incMatrix[vertexNum - 1] + graph->nbEdges;
      }
   }
   for_range (int, edgeNum, 0, < graph->nbEdges, ++) {
      int srcNum, destNum;
      double weight;
      scanf("%d %d %lf%*c", &srcNum, &destNum, &weight);
      EdgeProps edgeProps = { srcNum, destNum, weight, edgeNum };
      EdgeEnd asSucc = { destNum, weight, edgeNum }, asPred = { srcNum, weight, edgeNum };
      graph->edges[edgeNum] = edgeProps;
      dynarr_push(&graph->succLists[srcNum], asSucc);
      dynarr_push(&graph->predLists[destNum], asPred);
      graph->adjMatrix[srcNum][destNum] = weight;
      if (graph->isUndirected && srcNum != destNum) {
         dynarr_push(&graph->succLists[destNum], asPred);
         dynarr_push(&graph->predLists[srcNum], asSucc);
         graph->adjMatrix[destNum][srcNum] = weight;
      }
      if (calcIncMatrix) {
         graph->incMatrix[srcNum][edgeNum] = graph->isUndirected ? weight : -weight;
         graph->incMatrix[destNum][edgeNum] = weight;
      }
   }

   if ((flags & 1) == 1) {
      graph->verticesPos = (Position *)malloc(graph->nbVertices * sizeof(*graph->verticesPos));
      for_range (int, vertexNum, 0, < graph->nbVertices, ++) {
         scanf("%lf %lf", &graph->verticesPos[vertexNum].x, &graph->verticesPos[vertexNum].y);
      }
   }

   return graph;
}

void destroyGraph(Graph * graph)
{
   free(graph->vertexLabels);
   free(graph->selectedVertices.data);
   free(graph->selectedEdges.data);
   free(graph->edges);
   for_range (int, vNum, 0, < graph->nbVertices, ++) {
      free(graph->succLists[vNum].data);
      free(graph->predLists[vNum].data);
   }
   free(graph->succLists);
   free(graph->predLists);
   if (graph->adjMatrix != NULL) {
      free(graph->adjMatrix[0]);
      free(graph->adjMatrix);
   }
   if (graph->incMatrix != NULL) {
      free(graph->incMatrix[0]);
      free(graph->incMatrix);
   }
   free(graph->verticesPos);
   free(graph);
}


static inline int adjustLetterCode(int code)
{
   if ((graph->namingConvType == 3 || graph->namingConvType == 4) && code >= 17) {
      return code + 1;
   }
   return code;
}

char const * getVertexLabel(int vertexNum)
{
   if (vertexNum < 0 || vertexNum >= graph->nbVertices) {
      return vertexNum == -1 ? "—" : "";
   } else if (graph->vertexLabels[vertexNum] != NULL) {
      return graph->vertexLabels[vertexNum];
   }

   int nb = graph->namingConvFirst + vertexNum;
   if (graph->namingConvType == 0) {
      return graph->vertexLabels[vertexNum] = formatStr("%d", nb);
   }

   static struct { int length; char const * firstLetter; } const alphabetsData[] =
         { { 10, "0" }, { 26, "A" }, { 26, "a" }, { 24, "Α" }, { 24, "α" }, { 32, "А" }, { 32, "а" } };
   if (graph->namingConvType < 0 || array_len(alphabetsData) <= graph->namingConvType) {
      return "";
   }

   int alphabetLength = alphabetsData[graph->namingConvType].length;
   char const * firstLetter = alphabetsData[graph->namingConvType].firstLetter;
   char32_t firstLetterCode;
   mbstate_t state1 = {};
   mbrtoc32(&firstLetterCode, firstLetter, strlen(firstLetter), &state1);

   int len = 1, allocLen = 16;
   char * label = (char *)malloc(allocLen * sizeof(*label));
   label[0] = '\0';
   ++nb;
   while (nb > 0) {
      char32_t letterCode = firstLetterCode + adjustLetterCode((nb - 1) % alphabetLength);
      char char8Buf[MB_CUR_MAX], buf2[len];
      strcpy(buf2, label);
      mbstate_t state2 = {};
      int letterLen = c32rtomb(char8Buf, letterCode, &state2);
      len += letterLen;
      if (len > allocLen) {
         allocLen *= 2;
         label = (char *)realloc(label, allocLen * sizeof(*label));
      }
      sprintf(label, "%.*s%s", letterLen, char8Buf, buf2);
      nb = (nb - 1) / alphabetLength;
   }
   addString(label);
   return graph->vertexLabels[vertexNum] = label;
}

char const * floatToStr(double nb)
{
   if (!isfinite(nb)) {
      return nb == HUGE_VAL ? "∞" : (nb == -HUGE_VAL ? "-∞" : "?!NaN!?");
   }
   char const * result = formatStr("%lg", nb);
   for_array_ref_until (char, ptr, (char *)result, '\0') {
      if (*ptr == '.') {
         *ptr = ',';
      }
   }
   return result;
}


Any formatArraysForVertices(char const * rowNames[], Slice(Any) values)
{
   DynArr(Any) row = {};
   dynarr_push(&row, Any(dict_build(Any, { "textCode", Any("vertex") })));
   for (int vertexNum = 0; vertexNum < graph->nbVertices; ++vertexNum) {
      dynarr_push(&row, Any(getVertexLabel(vertexNum)));
   }
   DynArr(Any) table = {};
   dynarr_push(&table, Any(row));
   for_range (int, rowNum, 0, < values.len, ++) {
      row = (DynArr(Any)){};
      dynarr_push(&row, Any(rowNames[rowNum]));
      for_slice (Any, val, values.data[rowNum].sli) {
         dynarr_push(&row, val);
      }
      dynarr_push(&table, Any(row));
   }
   return Any(table);
}

Any formatBinaryTree(char const * title, Slice(Any) treeAsArray)
{
   DeclareDynArr(DynArr(Any))
   DynArr(DynArr_Any) table = {};
   dynarr_push(&table, (DynArr(Any)){});
   dynarr_push(&table.data[0], Any(title));
   if (treeAsArray.len > 0) {
      int nbRows = 1, subTreeSize = treeAsArray.len;
      while (subTreeSize > 1) {
         nbRows <<= 1;
         subTreeSize >>= 1;
      }
      int colNum = 0;
      for (int pos = 0; pos < treeAsArray.len;) {
         int initialPos = pos, nbLeafs = nbRows >> colNum, colEndPos = pos * 2 + 1;
         colEndPos = colEndPos < treeAsArray.len ? colEndPos : treeAsArray.len;
         ++colNum;
         for (; pos < colEndPos; ++pos) {
            int rowNum = (pos - initialPos) * nbLeafs;
            while (table.len <= rowNum) {
               dynarr_push(&table, (DynArr(Any)){});
            }
            while (table.data[rowNum].len < colNum) {
               dynarr_push(&table.data[rowNum], Any(""));
            }
            dynarr_push(&table.data[rowNum], treeAsArray.data[pos]);
         }
      }
   }
   Slice(Any) result = slice_new(Any, table.len);
   for_range (int, rowNum, 0, < table.len, ++) {
      result.data[rowNum] = Any(table.data[rowNum]);
   }
   free(table.data);
   return Any(result);
}

Any parentsToEdges(int const parents[])
{
   DynArr(Any) edgesList = {};
   for_range (int, vertexNum, 0, < graph->nbVertices, ++) {
      if (parents[vertexNum] >= 0) {
         dynarr_push(&edgesList, Any(dict_build(Any, { "src", Any(parents[vertexNum]) }, { "dest", Any(vertexNum) })));
      }
   }
   return Any(edgesList);
}

Any getEdgesFromBools(bool const edgesToTake[])
{
   DynArr(Any) edgesList = {};
   for_range (int, edgeNum, 0, < graph->nbEdges, ++) {
      if (edgesToTake[edgeNum]) {
         dynarr_push(&edgesList, Any(dict_build(Any, { "num", Any(edgeNum) })));
      }
   }
   return Any(edgesList);
}

Any getVerticesFromBools(bool const verticesToTake[])
{
   DynArr(Any) verticesList = {};
   for_range (int, vertexNum, 0, < graph->nbVertices, ++) {
      if (verticesToTake[vertexNum]) {
         dynarr_push(&verticesList, Any(vertexNum));
      }
   }
   return Any(verticesList);
}


void countOperations(char const * name, int quantity, ...)
{
   Any * count = dict_get(operations, name);
   if (count == nullptr || count->type != INT) {
      dict_set(&operations, name, Any(quantity));
   } else {
      count->i += quantity;
   }
}


static void assignValue(PriorityQueue * pq, int index, Any value)
{
   pq->data.data[index] = value;
}
static void assignValueWithIndexes(PriorityQueue * pq, int index, Any value)
{
   pq->data.data[index] = value;
   if (value.type == INT) {
      if (value.i >= pq->dataIndexes.len) {
         pq->dataIndexes.len = value.i + 1;
         dynarr_grow(&pq->dataIndexes);
      }
      pq->dataIndexes.data[value.i] = index;
   }
}

PriorityQueue PQ_create(bool (* priorityFct)(Any val1, Any val2), bool saveIndexes, char const * * opNames)
{
   PriorityQueue pq = { priorityFct, opNames, .assignValue = saveIndexes ? &assignValueWithIndexes : &assignValue };
   return pq;
}

void PQ_destroy(PriorityQueue * pq)
{
   free(pq->data.data);
   free(pq->dataIndexes.data);
}

void PQ_upgrade(PriorityQueue * pq, int pos, Any val)
{
   ++pos;
   while (pos > 1 && pq->priorityFct(val, pq->data.data[(pos >> 1) - 1])) {
      if (pq->opNames != NULL) {
         countOperation(pq->opNames[0]);
      }
      pq->assignValue(pq, pos - 1, pq->data.data[(pos >> 1) - 1]);
      pos >>= 1;
   }
   pq->assignValue(pq, pos - 1, val);
}

void PQ_downgrade(PriorityQueue * pq, int pos, Any val)
{
   ++pos;
   while (pos << 1 <= pq->data.len) {
      if (pos << 1 < pq->data.len && pq->priorityFct(pq->data.data[pos << 1], pq->data.data[(pos << 1) - 1])) {
         if (pq->priorityFct(val, pq->data.data[pos << 1])) {
            break;
         }
         pq->assignValue(pq, pos - 1, pq->data.data[pos << 1]);
         pos = (pos << 1) + 1;
      } else if (pq->priorityFct(val, pq->data.data[(pos << 1) - 1])) {
         break;
      } else {
         pq->assignValue(pq, pos - 1, pq->data.data[(pos << 1) - 1]);
         pos <<= 1;
      }
      if (pq->opNames != NULL) {
         countOperation(pq->opNames[1]);
      }
   }
   pq->assignValue(pq, pos - 1, val);
}

void PQ_push(PriorityQueue * pq, Any val)
{
   dynarr_push(&pq->data, val);
   PQ_upgrade(pq, pq->data.len - 1, val);
}

Any PQ_pop(PriorityQueue * pq)
{
   Any head = pq->data.data[0], last = pq->data.data[pq->data.len - 1];
   dynarr_pop(&pq->data);
   if (pq->data.len > 0) {
      PQ_downgrade(pq, 0, last);
   }
   return head;
}


static inline void writeComma(bool * isFirst)
{
   if (*isFirst) {
      *isFirst = false;
   } else {
      printf(", ");
   }
}

static void printQuotedString(char const * str)
{
   char const * cPtr = str;
   printf("\"");
   while (*cPtr != '\0') {
      if (*cPtr == '"' || *cPtr == '\\') {
         printf("\\");
      }
      printf("%c", *cPtr);
      ++cPtr;
   }
   printf("\"");
}

static void printAnyAsJson(Any data)
{
   switch (data.type) {
      case OTHER_ANY:
         printAnyAsJson(*data.a);
         break;
      case BOOLEAN:
         printf(data.b ? "true" : "false");
         break;
      case INT:
         printf("%d", data.i);
         break;
      case FLOAT:
         if (!isfinite(data.f)) {
            printAnyAsJson(Any(data.f == HUGE_VAL ? "∞" : (data.f == -HUGE_VAL ? "-∞" : "?!NaN!?")));
         } else {
            printf("%lg", data.f);
         }
         break;
      case STRING:
         printQuotedString(data.s);
         break;
      case EDGE_PROPS:
         printf("{ \"num\": %d }", data.ep.num);
         break;
      case EDGE_END:
         printf("{ \"num\": %d }", data.ee.num);
         break;
      case SLICE: {
         printf("[");
         bool isFirst = true;
         for (int elemNum = 0; elemNum < data.sli.len; ++elemNum) {
            writeComma(&isFirst);
            printAnyAsJson(data.sli.data[elemNum]);
         }
         printf("]");
      }  break;
      case DICT: {
         printf("{ ");
         bool isFirst = true;
         for (int elemNum = 0; elemNum < data.dic.len; ++elemNum) {
            writeComma(&isFirst);
            printQuotedString(data.dic.data[elemNum].key);
            printf(": ");
            printAnyAsJson(data.dic.data[elemNum].val);
         }
         printf(" }");
      }  break;
      case PTR:
         printf("%p", data.ptr);
         break;
      default:
         printf("null");
   }
}


void runAlgorithm(Slice(Any) (* fct)(Graph const * graph))
{
   graph = loadGraph();

   Slice(Any) dataToShow = fct(graph);
   Any allData = Any(dict_build(Any, { "states", Any(dataToShow) }, { "operations", Any(slice_on(DictEntry(Any), operations)) }));
   printAnyAsJson(allData);
   printf("\n");
   destroyAny(allData);

   destroyGraph(graph);
   graph = NULL;
   operations = (DynArr(DictEntry_Any)){};
   clearStrings();
}
