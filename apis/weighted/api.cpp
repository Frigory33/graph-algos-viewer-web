#include "api.hpp"

#include <cmath>
#include <cuchar>
#include <iostream>
#include <iomanip>
#include <sstream>


static Graph graph;
static std::map<std::string, int> operations;


Graph loadGraph()
{
   struct LinesReader: std::istringstream {
      LinesReader & line(bool withExceptions = true) {
         std::string nextLine;
         std::getline(std::cin, nextLine);
         clear();
         str(nextLine);
         exceptions(withExceptions ? std::ios::failbit : std::ios::goodbit);
         return *this;
      }
   };

   std::setlocale(LC_CTYPE, "fr_FR.utf8");
   std::ios::sync_with_stdio(false);
   LinesReader input;

   int nbVertices = 0, nbEdges = 0, isUndirected = 0, flags = 0, namingConvType = 0, namingConvFirst = 0;
   input.line() >> nbVertices >> nbEdges >> isUndirected >> flags;
   input.line() >> namingConvType >> namingConvFirst;

   Graph graph(nbVertices, nbEdges, isUndirected != 0, namingConvType, namingConvFirst);

   for (auto * array: { &graph.selectedVertices, &graph.selectedEdges }) {
      int num;
      input.line(false);
      while (input >> num) {
         array->push_back(num);
      }
   }

   bool calcIncMatrix = (flags & 2) == 2;
   if (calcIncMatrix) {
      graph.incMatrix.assign(nbVertices, std::vector<double>(nbEdges));
   }

   for (int edgeNum = 0; edgeNum < nbEdges; ++edgeNum) {
      int srcNum, destNum;
      double weight;
      input.line() >> srcNum >> destNum >> weight;
      Graph::EdgeProps edgeProps = { srcNum, destNum, weight, edgeNum };
      Graph::EdgeEnd asSucc = { destNum, weight, edgeNum }, asPred = { srcNum, weight, edgeNum };
      graph.edges[edgeNum] = edgeProps;
      graph.succLists[srcNum].push_back(asSucc);
      graph.predLists[destNum].push_back(asPred);
      graph.adjMatrix[srcNum][destNum] = weight;
      if (graph.isUndirected && srcNum != destNum) {
         graph.succLists[destNum].push_back(asPred);
         graph.predLists[srcNum].push_back(asSucc);
         graph.adjMatrix[destNum][srcNum] = weight;
      }
      if (calcIncMatrix) {
         graph.incMatrix[srcNum][edgeNum] = graph.isUndirected ? weight : -weight;
         graph.incMatrix[destNum][edgeNum] = weight;
      }
   }

   if ((flags & 1) == 1) {
      graph.verticesPos.resize(nbVertices);
      for (auto & vPos: graph.verticesPos) {
         input.line() >> vPos.x >> vPos.y;
      }
   }

   return graph;
}


std::string getVertexLabel(int vertexNum)
{
   if (vertexNum < 0) {
      return vertexNum == -1 ? "—" : "";
   }

   int nb = graph.namingConvFirst + vertexNum;
   if (graph.namingConvType == 0) {
      std::ostringstream buf;
      buf << nb;
      return buf.str();
   }

   auto adjustLetterCode = [](int code) -> int {
      if ((graph.namingConvType == 3 || graph.namingConvType == 4) && code >= 17) {
         return code + 1;
      }
      return code;
   };

   static std::vector<std::pair<int, std::string>> const alphabetsData =
         { { 10, "0" }, { 26, "A" }, { 26, "a" }, { 24, "Α" }, { 24, "α" }, { 32, "А" }, { 32, "а" } };
   int alphabetLength = alphabetsData.at(graph.namingConvType).first;
   std::string firstLetter = alphabetsData.at(graph.namingConvType).second;
   char32_t firstLetterCode;
   std::mbstate_t state1 = {};
   std::mbrtoc32(&firstLetterCode, firstLetter.c_str(), firstLetter.length(), &state1);

   std::string label;
   ++nb;
   while (nb > 0) {
      char32_t letterCode = firstLetterCode + adjustLetterCode((nb - 1) % alphabetLength);
      char char8Buf[MB_CUR_MAX];
      std::mbstate_t state2 = {};
      int letterLen = std::c32rtomb(char8Buf, letterCode, &state2);
      label = std::string(char8Buf, char8Buf + letterLen) + label;
      nb = (nb - 1) / alphabetLength;
   }
   return label;
}

std::string floatToStr(double nb)
{
   if (!std::isfinite(nb)) {
      return nb == HUGE_VAL ? "∞" : (nb == -HUGE_VAL ? "-∞" : "?!NaN!?");
   }
   std::ostringstream buf;
   buf << nb;
   std::string result = buf.str();
   std::replace(whole(result), '.', ',');
   return result;
}


Table formatArraysForVertices(std::vector<std::string> const & rowNames, Table const & values)
{
   AnyVector row = { Dict{ { "textCode", "vertex" } } };
   for (int vertexNum = 0; vertexNum < graph.nbVertices; ++vertexNum) {
      row.push_back(getVertexLabel(vertexNum));
   }
   Table result = { row };
   for (int rowNum = 0; rowNum < (int)rowNames.size(); ++rowNum) {
      row = { rowNames[rowNum] };
      for (std::any const & val: values[rowNum]) {
         row.push_back(val);
      }
      result.push_back(row);
   }
   return result;
}

Table formatBinaryTree(std::string const & title, AnyVector const & treeAsArray)
{
   Table result = { { title } };
   if (treeAsArray.empty()) {
      return result;
   }
   int nbRows = 1, subTreeSize = treeAsArray.size();
   while (subTreeSize > 1) {
      nbRows <<= 1;
      subTreeSize >>= 1;
   }
   int colNum = 0;
   for (int pos = 0; pos < (int)treeAsArray.size();) {
      int initialPos = pos, nbLeafs = nbRows >> colNum, colEndPos = std::min(pos * 2 + 1, (int)treeAsArray.size());
      ++colNum;
      for (; pos < colEndPos; ++pos) {
         int rowNum = (pos - initialPos) * nbLeafs;
         if ((int)result.size() <= rowNum) {
            result.resize(rowNum + 1);
         }
         if ((int)result[rowNum].size() < colNum) {
            result[rowNum].resize(colNum, "");
         }
         result[rowNum].push_back(treeAsArray[pos]);
      }
   }
   return result;
}

AnyVector parentsToEdges(std::vector<int> const & parents)
{
   AnyVector edgesList;
   for (int vertexNum = 0; vertexNum < (int)parents.size(); ++vertexNum) {
      if (parents[vertexNum] >= 0) {
         edgesList.push_back(Dict{ { "src", parents[vertexNum] }, { "dest", vertexNum } });
      }
   }
   return edgesList;
}

AnyVector getEdgesFromBools(std::vector<bool> const & edgesToTake)
{
   AnyVector edgesList;
   for (int edgeNum = 0; edgeNum < (int)edgesToTake.size(); ++edgeNum) {
      if (edgesToTake[edgeNum]) {
         edgesList.push_back(Dict{ { "num", edgeNum } });
      }
   }
   return edgesList;
}

AnyVector getVerticesFromBools(std::vector<bool> const & verticesToTake)
{
   AnyVector verticesList;
   for (int vertexNum = 0; vertexNum < (int)verticesToTake.size(); ++vertexNum) {
      if (verticesToTake[vertexNum]) {
         verticesList.push_back(vertexNum);
      }
   }
   return verticesList;
}


void countOperation(std::string const & name, int quantity)
{
   operations[name] += quantity;
}


void PriorityQueue::upgrade(int pos, int val) {
   ++pos;
   while (pos > 1 && priorityFct(val, data[(pos >> 1) - 1])) {
      if (opNames != nullptr) {
         countOperation(opNames[0]);
      }
      assignValue(pos - 1, data[(pos >> 1) - 1]);
      pos >>= 1;
   }
   assignValue(pos - 1, val);
}

void PriorityQueue::downgrade(int pos, int val) {
   ++pos;
   while (pos << 1 <= length()) {
      if (pos << 1 < length() && priorityFct(data[pos << 1], data[(pos << 1) - 1])) {
         if (priorityFct(val, data[pos << 1])) {
            break;
         }
         assignValue(pos - 1, data[pos << 1]);
         pos = (pos << 1) + 1;
      } else if (priorityFct(val, data[(pos << 1) - 1])) {
         break;
      } else {
         assignValue(pos - 1, data[(pos << 1) - 1]);
         pos <<= 1;
      }
      if (opNames != nullptr) {
         countOperation(opNames[1]);
      }
   }
   assignValue(pos - 1, val);
}

void PriorityQueue::push(int val) {
   data.push_back(val);
   upgrade(length() - 1, val);
}

int PriorityQueue::pop() {
   int head = data[0], last = data.back();
   data.pop_back();
   if (length() > 0) {
      downgrade(0, last);
   }
   return head;
}

void PriorityQueue::assignValue(int index, int value) {
   data[index] = value;
   if (value >= (int)dataIndexes.size()) {
      dataIndexes.resize(value + 1, -1);
   }
   dataIndexes[value] = index;
}


static void writeComma(std::ostream & jsonOut, bool & isFirst)
{
   if (isFirst) {
      isFirst = false;
   } else {
      jsonOut << ", ";
   }
}

static void anyToJson(std::ostream & jsonOut, std::any const & data)
{
   if (data.type() == typeid(std::string)) {
      jsonOut << std::quoted(std::any_cast<std::string const &>(data));
   } else if (data.type() == typeid(char const *)) {
      jsonOut << std::quoted(std::any_cast<char const *>(data));
   } else if (data.type() == typeid(int)) {
      jsonOut << std::any_cast<int>(data);
   } else if (data.type() == typeid(double)) {
      double nb = std::any_cast<double>(data);
      if (!std::isfinite(nb)) {
         anyToJson(jsonOut, nb == HUGE_VAL ? "∞" : (nb == -HUGE_VAL ? "-∞" : "?!NaN!?"));
      } else {
         jsonOut << nb;
      }
   } else if (data.type() == typeid(bool)) {
      jsonOut << std::boolalpha << std::any_cast<bool>(data);
   } else if (data.type() == typeid(Dict)) {
      auto const & mapOfData = std::any_cast<Dict const &>(data);
      jsonOut << "{ ";
      bool isFirst = true;
      for (auto const & pairOfData: mapOfData) {
         writeComma(jsonOut, isFirst);
         jsonOut << std::quoted(pairOfData.first) << ": ";
         anyToJson(jsonOut, pairOfData.second);
      }
      jsonOut << " }";
   } else if (data.type() == typeid(AnyVector)) {
      auto const & arrayOfData = std::any_cast<AnyVector const &>(data);
      jsonOut << "[";
      bool isFirst = true;
      for (auto const & data: arrayOfData) {
         writeComma(jsonOut, isFirst);
         anyToJson(jsonOut, data);
      }
      jsonOut << "]";
   } else if (data.type() == typeid(Table)) {
      Table const & table = std::any_cast<Table const &>(data);
      anyToJson(jsonOut, AnyVector(whole(table)));
   } else if (data.type() == typeid(Graph::EdgeProps)) {
      jsonOut << "{ \"num\": " << std::any_cast<Graph::EdgeProps>(data).num << " }";
   } else if (data.type() == typeid(Graph::EdgeEnd)) {
      jsonOut << "{ \"num\": " << std::any_cast<Graph::EdgeEnd>(data).num << " }";
   } else {
      jsonOut << "null";
   }
}


void runAlgorithm(std::function<DataToShow(Graph const &)> fct)
{
   graph = loadGraph();
   operations.clear();

   DataToShow dataToShow = fct(graph);

   std::ostringstream jsonOut;
   jsonOut << "{ \"states\": [";
   bool isFirst = true;
   for (auto const & state: dataToShow) {
      writeComma(jsonOut, isFirst);
      jsonOut << "[";
      bool isFirst2 = true;
      for (auto const & data: state) {
         writeComma(jsonOut, isFirst2);
         anyToJson(jsonOut, data);
      }
      jsonOut << "]";
   }
   jsonOut << "], \"operations\": { ";
   isFirst = true;
   for (auto const & op: operations) {
      writeComma(jsonOut, isFirst);
      jsonOut << std::quoted(op.first) << ": " << op.second;
   }
   jsonOut << " } }";
   std::cout << jsonOut.str() << std::endl;
}
