type edge_props = {
   src: int;
   dest: int;
   weight: float;
   num: int;
}
type edge_end = {
   vertex: int;
   weight: float;
   num: int;
}
type position = {
   x: float;
   y: float;
}
type graph = {
   nbVertices: int;
   nbEdges: int;
   isUndirected: bool;
   namingConvType: int;
   namingConvFirst: int;
   selectedVertices: int list;
   selectedEdges: int list;
   edges: edge_props array;
   succLists: edge_end list array;
   predLists: edge_end list array;
   adjMatrix: float array array;
   incMatrix: float array array;
   verticesPos: position array;
}

type operation = {
   name: string;
   mutable quantity: int;
}

type json =
   | JsonNull
   | JsonBool of bool
   | JsonInt of int
   | JsonFloat of float
   | JsonStr of string
   | JsonArray of json array
   | JsonList of json list
   | JsonObj of (string * json) list
   | JsonOpt of json option
   | JsonEdgeProps of edge_props
   | JsonEdgeEnd of edge_end


let graph = ref {
   nbVertices = 0;
   nbEdges = 0;
   isUndirected = false;
   namingConvType = 0;
   namingConvFirst = 1;
   selectedVertices = [];
   selectedEdges = [];
   edges = [| |];
   succLists = [| |];
   predLists = [| |];
   adjMatrix = [| |];
   incMatrix = [| |];
   verticesPos = [| |]
}

let operations: operation list ref = ref []


let loadGraph() =
   let nbVertices, nbEdges, isUndirectedValue, flags = Scanf.sscanf (read_line()) " %d %d %d %d" (fun a b c d -> a, b, c, d) in
   let isUndirected = isUndirectedValue <> 0 in
   let namingConvType, namingConvFirst = Scanf.sscanf (read_line()) " %d %d" (fun a b -> a, b) in

   let readIntList() =
      let line = read_line() in
      if String.length line = 0
         then []
         else List.map int_of_string (String.split_on_char ' ' line)
   in
   let selectedVertices = readIntList()
   and selectedEdges = readIntList() in

   let readEdge num =
      let src, dest, weight = Scanf.sscanf (read_line()) " %d %d %f" (fun a b c -> a, b, c) in
      { src = src; dest = dest; weight = weight; num = num }
   in
   let edges = Array.init nbEdges readEdge in

   let succLists = Array.make nbVertices [] and predLists = Array.make nbVertices [] in
   let adjMatrix = Array.make_matrix nbVertices nbVertices 0. in
   let calcIncMatrix = flags land 2 = 2 in
   let incMatrix = 
      if calcIncMatrix
         then Array.make_matrix nbVertices nbEdges 0.
         else [| |]
   in

   let applyEdge edge =
      let asSucc = { vertex = edge.dest; weight = edge.weight; num = edge.num }
      and asPred = { vertex = edge.src; weight = edge.weight; num = edge.num } in
      succLists.(edge.src) <- asSucc :: succLists.(edge.src);
      predLists.(edge.dest) <- asPred :: predLists.(edge.dest);
      adjMatrix.(edge.src).(edge.dest) <- edge.weight;
      if flags land 2 = 2 then (
         incMatrix.(edge.src).(edge.num) <- if isUndirected then edge.weight else -.edge.weight;
         incMatrix.(edge.dest).(edge.num) <- edge.weight;
      );
      if isUndirected && edge.src <> edge.dest then (
         succLists.(edge.dest) <- asPred :: succLists.(edge.dest);
         predLists.(edge.src) <- asSucc :: predLists.(edge.src);
         adjMatrix.(edge.dest).(edge.src) <- edge.weight;
      );
   in
   Array.iter applyEdge edges;

   let readPos _ =
      let x, y = Scanf.sscanf (read_line()) " %f %f" (fun a b -> a, b) in
      { x = x; y = y }
   in
   let verticesPos =
      if flags land 1 = 1
         then Array.init nbVertices readPos
         else [| |]
   in

   {
      nbVertices = nbVertices;
      nbEdges = nbEdges;
      isUndirected = isUndirected;
      namingConvType = namingConvType;
      namingConvFirst = namingConvFirst;
      selectedVertices = selectedVertices;
      selectedEdges = selectedEdges;
      edges = edges;
      succLists = Array.map List.rev succLists;
      predLists = Array.map List.rev predLists;
      adjMatrix = adjMatrix;
      incMatrix = incMatrix;
      verticesPos = verticesPos;
   }


let getVertexLabel vertexNum =
   let nb = !graph.namingConvFirst + vertexNum in

   if vertexNum < 0 then (
      if vertexNum = -1 then "—" else ""

   ) else if !graph.namingConvType = 0 then (
      string_of_int nb

   ) else (
      let adjustLetterCode code =
         if (!graph.namingConvType = 3 || !graph.namingConvType = 4) && code >= 17
            then code + 1
            else code
      in

      (* For OCaml 5
      let alphabetsData = [| (10, "0"); (26, "A"); (26, "a"); (24, "Α"); (24, "α"); (32, "А"); (32, "а") |] in
      let alphabetLength, firstLetter = alphabetsData.(!graph.namingConvType) in
      let firstLetterCode = Uchar.to_int (Uchar.utf_decode_uchar (String.get_utf_8_uchar firstLetter 0)) in
      *)
      let alphabetsData = [| (10, 48); (26, 65); (26, 97); (24, 913); (24, 945); (32, 1040); (32, 1072) |] in
      let alphabetLength, firstLetterCode = alphabetsData.(!graph.namingConvType) in

      let label = ref [] in
      let nb = ref (nb + 1) in
      while !nb > 0 do
         let letterCode = firstLetterCode + adjustLetterCode ((!nb - 1) mod alphabetLength) in
         label := letterCode :: !label;
         nb := (!nb - 1) / alphabetLength;
      done;
      let labelBuf = Buffer.create 4 in
      let concatCodePoints codePoint = Buffer.add_utf_8_uchar labelBuf (Uchar.of_int codePoint) in
      List.iter concatCodePoints !label;
      Buffer.contents labelBuf
   )

let floatToStr nb =
   if Float.is_finite nb
      then String.map (fun crt -> if crt == '.' then ',' else crt) (Printf.sprintf "%g" nb)
      else if nb = Float.infinity then "∞"
      else if nb = Float.neg_infinity then "-∞"
      else "?!NaN!?"


let formatArraysForVertices rowNames values =
   let vNumToVLabel vNum = JsonStr (getVertexLabel vNum) in
   let firstRow = Array.append [| JsonObj [("textCode", JsonStr "vertex")] |] (Array.init !graph.nbVertices vNumToVLabel) in
   let generateRow index =
      JsonArray (Array.append [| JsonStr rowNames.(index) |] values.(index))
   in
   Array.append [| JsonArray firstRow |] (Array.init (Array.length rowNames) generateRow)

let formatBinaryTree title treeAsArray =
   let arrayLen = Array.length treeAsArray in
   if arrayLen <= 0 then
      [| JsonArray [| JsonStr title |] |]
   else (
      let nbRows = ref 1
      and nbCols = ref 2
      and subTreeSize = ref arrayLen in
      while !subTreeSize > 1 do
         nbRows := !nbRows * 2;
         incr nbCols;
         subTreeSize := !subTreeSize / 2;
      done;
      let result = Array.make_matrix !nbRows !nbCols JsonNull
      and resultLengths = Array.make !nbRows 0 in
      result.(0).(0) <- JsonStr title;
      resultLengths.(0) <- 1;
      let colNum = ref 0
      and nbUsedRows = ref 0
      and pos = ref 0 in
      while !pos < arrayLen do
         let initialPos = !pos
         and nbLeafs = !nbRows lsr !colNum
         and colEndPos = min (!pos * 2 + 1) arrayLen in
         incr colNum;
         while !pos < colEndPos do
            let rowNum = (!pos - initialPos) * nbLeafs in
            for prevColNum = resultLengths.(rowNum) to !colNum - 1 do
               result.(rowNum).(prevColNum) <- JsonStr "";
            done;
            result.(rowNum).(!colNum) <- treeAsArray.(!pos);
            resultLengths.(rowNum) <- !colNum + 1;
            nbUsedRows := max (rowNum + 1) !nbUsedRows;
            incr pos;
         done;
      done;
      let shrinkRow rowNum row = JsonArray (Array.sub row 0 resultLengths.(rowNum)) in
      Array.mapi shrinkRow (Array.sub result 0 !nbUsedRows)
   )

let parentsToEdges parents =
   let edgesList = ref [] in
   let parentToEdge vNum parentVNum =
      if parentVNum >= 0 then
         edgesList := (JsonObj [("src", JsonInt parentVNum); ("dest", JsonInt vNum)]) :: !edgesList;
   in
   Array.iteri parentToEdge parents;
   List.rev !edgesList

let getEdgesFromBools edgesToTake =
   let edgesList = ref [] in
   let edgeFromBool num takeIt =
      if takeIt then
         edgesList := (JsonObj [("num", JsonInt num)]) :: !edgesList;
   in
   Array.iteri edgeFromBool edgesToTake;
   List.rev !edgesList

let getVerticesFromBools verticesToTake =
   let verticesList = ref [] in
   let vertexFromBool num takeIt =
      if takeIt then
         verticesList := (JsonInt num) :: !verticesList;
   in
   Array.iteri vertexFromBool verticesToTake;
   List.rev !verticesList


let countOperations name quantity =
   let rec doAddition = function
      | [] -> operations := { name = name; quantity = quantity } :: !operations;
      | op :: remaining ->
         if name = op.name then (
            op.quantity <- op.quantity + quantity;
         ) else (
            doAddition remaining;
         )
   in
   doAddition !operations
let countOperation name = countOperations name 1


let assignValueNoIndexes data _ index value = data.(index) <- value
let assignValueWithIndexes data dataIndexes index value =
   data.(index) <- value;
   let dataIndexesLen = Array.length !dataIndexes in
   if value >= dataIndexesLen then (
      let takeIndex valAsIndex = if valAsIndex < dataIndexesLen then !dataIndexes.(valAsIndex) else -1 in
      let newSize = max 4 (max (value + 1) (dataIndexesLen * 2)) in
      dataIndexes := Array.init newSize takeIndex;
   );
   !dataIndexes.(value) <- index;

class ['a] priority_queue priorityFct ?(assignValue = assignValueNoIndexes) ?(opNames = [||]) noneValue = object(self)
   val mutable data : 'a array = [| |]
   val mutable dataIndexes : int array ref = ref [| |]
   val mutable length = 0
   method private assignValue = assignValue data dataIndexes
   method private countOp opNum =
      if Array.length opNames >= 2 then
         countOperation opNames.(opNum);
   method data = Array.sub data 0 length
   method dataIndexes = !dataIndexes
   method length = length
   method top = data.(0)
   method upgrade pos value =
      let pos = ref (pos + 1) in
      while !pos > 1 && priorityFct value data.((!pos / 2) - 1) do
         self#countOp 0;
         self#assignValue (!pos - 1) data.((!pos / 2) - 1);
         pos := !pos / 2;
      done;
      self#assignValue (!pos - 1) value;
   method downgrade pos value =
      let posFound pos = self#assignValue (pos - 1) value in
      let rec innerDowngrade pos =
         if pos * 2 > length then (
            posFound pos;
         ) else if pos * 2 < length && priorityFct data.(pos * 2) data.(pos * 2 - 1) then (
            if priorityFct data.(pos * 2) value then (
               self#countOp 1;
               self#assignValue (pos - 1) data.(pos * 2);
               innerDowngrade (pos * 2 + 1);
            ) else (
               posFound pos;
            )
         ) else if priorityFct data.(pos * 2 - 1) value then (
            self#countOp 1;
            self#assignValue (pos - 1) data.(pos * 2 - 1);
            innerDowngrade (pos * 2);
         ) else (
            posFound pos;
         )
      in
      innerDowngrade (pos + 1);
   method push value =
      length <- length + 1;
      let dataLen = Array.length data in
      if length > dataLen then (
         let takeValue index = if index < dataLen then data.(index) else noneValue in
         let newSize = max 4 (dataLen * 2) in
         let newData = Array.init newSize takeValue in
         data <- newData;
      );
      self#upgrade (length - 1) value;
   method pop =
      let head = data.(0)
      and last = data.(length - 1) in
      length <- length - 1;
      if length > 0 then
         self#downgrade 0 last;
      head
end


let runAlgorithm fct =
   graph := loadGraph();
   operations := [];

   let dataToShow = fct !graph in

   let writeComma isFirst =
      if !isFirst then (
         isFirst := false;
      ) else (
         print_string ", ";
      )
   and quoteString str =
      let result = Buffer.create (String.length str) in
      Buffer.add_char result '"';
      let addChar crt =
         if crt = '"' || crt = '\\' then
            Buffer.add_char result '\\';
         Buffer.add_char result crt;
      in
      String.iter addChar str;
      Buffer.add_char result '"';
      Buffer.contents result
   in
   let rec printElem isFirst elem =
      writeComma isFirst;
      printJson elem;
   and printJson = function
      | JsonNull -> print_string "null";
      | JsonBool flag -> print_string (Bool.to_string flag);
      | JsonInt nb -> print_int nb;
      | JsonFloat nb ->
         if Float.is_finite nb then (
            Printf.printf "%g" nb;
         ) else (
            printJson (JsonStr (
                  if nb = Float.infinity then "∞"
                  else if nb = Float.neg_infinity then "-∞"
                  else "?!NaN!?"
               ));
         )
      | JsonStr str -> print_string (quoteString str);
      | JsonArray arr ->
         print_string "[";
         let isFirst = ref true in
         Array.iter (printElem isFirst) arr;
         print_string "]";
      | JsonList l ->
         print_string "[";
         let isFirst = ref true in
         List.iter (printElem isFirst) l;
         print_string "]";
      | JsonObj obj ->
         print_string "{ ";
         let isFirst = ref true in
         let printPair (key, value) =
            writeComma isFirst;
            Printf.printf "%s: " (quoteString key);
            printJson value;
         in
         List.iter printPair obj;
         print_string " }";
      | JsonOpt None -> print_string "null";
      | JsonOpt (Some json) -> printJson json;
      | JsonEdgeProps edgeProps -> Printf.printf "{ \"num\": %d }" edgeProps.num;
      | JsonEdgeEnd edgeEnd -> Printf.printf "{ \"num\": %d }" edgeEnd.num;
   in

   print_string "{ \"states\": [";
   let isFirst = ref true in
   let printState state =
      writeComma isFirst;
      print_string "[";
      let isFirst2 = ref true in
      let printStateElem elem =
         writeComma isFirst2;
         printJson (JsonObj elem);
      in
      List.iter printStateElem state;
      print_string "]";
   in
   List.iter printState dataToShow;
   print_string "], \"operations\": { ";
   isFirst := true;
   let printOperation op =
      writeComma isFirst;
      Printf.printf "%s: %d" (quoteString op.name) op.quantity;
   in
   List.iter printOperation (List.rev !operations);
   print_endline " } }";
