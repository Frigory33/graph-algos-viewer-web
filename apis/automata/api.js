function loadAutomaton() {
   const edges = getAllEdges(), alphabet = new Set();
   const automaton = {
         nbStates: nbVertices, nbTransitions: edges.length, states: [], initial: [], final: [], alphabet: "",
         word: document.getElementById('algoInput_word').value,
         expression: document.getElementById('algoInput_expression').value,
      };
   for (let sNum = 0; sNum < automaton.nbStates; ++sNum) {
      automaton.states[sNum] = [];
   }
   let edgeNum = 0;
   for (const edge of edges) {
      const srcNum = parseInt(document.getElementById(edge.getAttribute('data-from')).getAttribute('data-num')),
         destNum = parseInt(document.getElementById(edge.getAttribute('data-to')).getAttribute('data-num'));
      let letters = edge.querySelector('.weight').textContent.split(",").map(letter => letter.trim())
         .map(letter => letter == "ε" ? "" : letter);
      for (const letter of letters) {
         for (const subLetter of letter) {
            alphabet.add(subLetter);
         }
         automaton.states[srcNum].push({ state: destNum, letter: letter, num: edgeNum });
      }
      ++edgeNum;
   }
   for (const vertex of svgGraph.vertices.querySelectorAll('.vertex.initial')) {
      automaton.initial.push(parseInt(vertex.getAttribute('data-num')));
   }
   for (const vertex of svgGraph.vertices.querySelectorAll('.vertex.final')) {
      automaton.final.push(parseInt(vertex.getAttribute('data-num')));
   }
   automaton.alphabet = Array.from(alphabet.values());
   return automaton;
}


const getStateLabel = getVertexLabel;


function countOperation(name, quantity) {
   quantity = quantity == undefined ? 1 : quantity;
   operations[name] = operations[name] ? operations[name] + quantity : quantity;
}


let algoAutomaton = null;

function runAlgorithm(fct) {
   mergeParallelTransi();
   algoAutomaton = loadAutomaton();
   algoOperations = {};
   algoStates = fct(algoAutomaton);
   algoAutomaton = null;
   initAlgorithmResult();
}
