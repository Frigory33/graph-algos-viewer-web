let svgGraph, namingConv, graphIsUndirected, nbVertices;
let numberPoint, possibleNumberPoints;

let nextVertexId;
let documentHistory = [], posInHistory = -1, historyManipDescTimeout = null, saveInHistory = true;


function getVertex(vNum) {
   return svgGraph.vertices.querySelector('.vertex[data-num="' + vNum + '"]');
}
function getAllVertices() {
   return svgGraph.vertices.children;
}
function getEdgeByIds(vFromId, vToId, undirectedMode, exclusionSet) {
   let selector = '.edge[data-from="' + vFromId + '"][data-to="' + vToId + '"]';
   if (undirectedMode) {
      selector += ', .edge[data-from="' + vToId + '"][data-to="' + vFromId + '"]';
   }
   if (exclusionSet) {
      for (const edge of svgGraph.edges.querySelectorAll(selector)) {
         if (!exclusionSet.has(edge)) {
            return edge;
         }
      }
      return null;
   }
   return svgGraph.edges.querySelector(selector);
}
function getEdge(vFromNum, vToNum, undirectedMode) {
   let vFrom, vTo;
   if ((vFrom = getVertex(vFromNum)) && (vTo = getVertex(vToNum))) {
      return getEdgeByIds(vFrom.id, vTo.id, undirectedMode);
   }
   return null;
}
function getAllEdges() {
   return svgGraph.edges.children;
}
function getGraphSelection(precision) {
   return svgGraph.querySelectorAll((precision == undefined ? '' : precision) + '.selected');
}

function getSvgTitle() {
   return document.getElementById('svgFileTitle').value.trim() || getText('graph', true);
}

function createSvgElement(tagName, attributes) {
   const elem = document.createElementNS('http://www.w3.org/2000/svg', tagName);
   if (attributes) {
      for (const [attrName, attrValue] of Object.entries(attributes)) {
         if (attrValue != null) {
            elem.setAttribute(attrName, attrValue);
         }
      }
   }
   return elem;
}


function initSvg() {
   Object.assign(svgGraph, {
         edges: svgGraph.getElementById('edges'),
         vertices: svgGraph.getElementById('vertices'),
         shadow: svgGraph.getElementById('shadow'),
      });
   const allVertices = getAllVertices();
   nbVertices = allVertices.length;
   let maxVertexId = 0;
   for (const vertex of allVertices) {
      maxVertexId = Math.max(parseInt(vertex.id.substr(6)), maxVertexId);
   }
   nextVertexId = maxVertexId + 1;
   updateNamingConv();
   graphIsUndirected = svgGraph.classList.contains('undirectedGraph');
   document.getElementById('vertexNamingType').value = namingConv.type;
   document.getElementById('vertexNamingFirst').value = getVertexLabelFromZero(namingConv.first);
   document.getElementById('showWeights').checked = !svgGraph.classList.contains('hideWeights');
   document.getElementById('graphIsDirected').checked = !graphIsUndirected;
   document.getElementById('graphContainer').classList[graphIsUndirected ? 'add' : 'remove']('undirectedGraph');
   updateElemsSize();
   if (window.updateSelectionControls) {
      updateSelectionControls();
   }
   documentChanged(getText('action_documentLoading'));
}

function clearSvg() {
   for (const group of svgGraph.querySelectorAll(':scope > g')) {
      group.innerHTML = '';
   }
   nbVertices = 0;
   nextVertexId = 1;
}

function loadSvg(data, fileName, noReload) {
   removeAllAlgorithmicInfo();
   const oldSvgCode = noReload ? null : svgGraph.outerHTML;
   try {
      const tmpBlock = document.createElement('div');
      tmpBlock.innerHTML = data;
      const newSvg = tmpBlock.querySelector('svg'), newSvgClass = newSvg.getAttribute('class');
      if (newSvgClass) {
         svgGraph.setAttribute('class', newSvgClass);
      } else {
         svgGraph.removeAttribute('class');
      }
      svgGraph.innerHTML = newSvg.innerHTML.trimStart();
      let titleContent = null;
      const titleTag = svgGraph.querySelector('title');
      if (titleTag != null) {
         titleContent = titleTag.textContent;
      }
      for (const node of svgGraph.querySelectorAll('title, title + style')) {
         node.remove();
      }
      initSvg();
      if (fileName) {
         const svgTitle = fileName.match(/^([^\.]+)/)[1];
         if (svgTitle) {
            document.getElementById('svgFileTitle').value = svgTitle;
         }
      } else if (titleContent) {
         document.getElementById('svgFileTitle').value = titleContent;
      }
   } catch {
      if (!noReload) {
         loadSvg(oldSvgCode, undefined, true);
         alert(getText('fileCouldntBeLoadedAsSvg', true));
      } else {
         alert(getText('previousGraphCouldntBeRestored', true));
      }
   }
}

function getSvgCode() {
   let cssText = '';
   try {
      const cssRules = document.styleSheets[1].rules;
      cssText = ' ';
      for (let cssRuleNum = 0; cssRuleNum < cssRules.length; ++cssRuleNum) {
         cssText += cssRules[cssRuleNum].cssText + ' ';
      }
   } catch {}
   let classAttr = svgGraph.getAttribute('class');
   classAttr = classAttr ? ' class="' + classAttr + '"' : '';
   return '<svg xmlns="http://www.w3.org/2000/svg"' + classAttr + '>\n' +
         '<title>' + getSvgTitle() + '</title><style>' + cssText + '</style>' + svgGraph.innerHTML +
      '</svg>';
}


function documentChanged(actionDesc) {
   if (saveInHistory) {
      if (posInHistory >= 50) {
         documentHistory.shift();
      } else {
         ++posInHistory;
      }
      documentHistory.splice(posInHistory, Infinity,
         { desc: actionDesc, data: LZString.compressToUint8Array(svgGraph.outerHTML) });
   }
}

function reloadFromHistory() {
   saveInHistory = false;
   loadSvg(LZString.decompressFromUint8Array(documentHistory[posInHistory].data), undefined, true);
   deselectAll();
   saveInHistory = true;
}

function undoDocumentAction() {
   removeAllAlgorithmicInfo();
   if (posInHistory > 0) {
      historyManipDescTimeout = showTemporaryMsg(document.getElementById('historyManipulationDesc'),
         getText('actionCanceled', false, [documentHistory[posInHistory].desc]), 5000, historyManipDescTimeout);
      --posInHistory;
      reloadFromHistory();
   } else {
      historyManipDescTimeout = showTemporaryMsg(document.getElementById('historyManipulationDesc'),
         getText('noActionToCancel'), 5000, historyManipDescTimeout);
   }
}

function redoDocumentAction() {
   removeAllAlgorithmicInfo();
   if (posInHistory + 1 < documentHistory.length) {
      ++posInHistory;
      historyManipDescTimeout = showTemporaryMsg(document.getElementById('historyManipulationDesc'),
         getText('actionRestored', false, [documentHistory[posInHistory].desc]), 5000, historyManipDescTimeout);
      reloadFromHistory();
   } else {
      historyManipDescTimeout = showTemporaryMsg(document.getElementById('historyManipulationDesc'),
         getText('noActionToRestore'), 5000, historyManipDescTimeout);
   }
}

function loadDotSvg(data, fileName) {
   removeAllAlgorithmicInfo();
   const dotSvgContainer = document.createElement('div');
   dotSvgContainer.innerHTML = data;
   clearSvg();
   const vertices = [];
   for (const vElem of dotSvgContainer.querySelectorAll('.node')) {
      const vTitle = vElem.querySelector('title'), vShape = vElem.querySelector('ellipse');
      let vNum;
      if (vTitle != null && !isNaN(vNum = parseInt(vTitle.textContent))) {
         vertices[vNum] = [vShape.getAttribute('cx'), vShape.getAttribute('cy')];
      }
   }
   for (let vNum = 0; vNum < vertices.length; ++vNum) {
      createVertex(vertices[vNum][0], vertices[vNum][1]);
   }
   for (const eElem of dotSvgContainer.querySelectorAll('.edge title')) {
      const vNums = eElem.textContent.match(/^(\d+)-[->](\d+)$/);
      if (vNums) {
         const vFrom = getVertex(vNums[1]), vTo = getVertex(vNums[2]);
         if (vFrom != null && vTo != null) {
            createEdge(vFrom, vTo);
         }
      }
   }
   svgGraph.classList[dotSvgContainer.querySelector('.edge title').textContent.match('->') ? 'remove' : 'add']
      ('undirectedGraph');
   initSvg();
   if (fileName) {
      const svgTitle = fileName.match(/^([^\.]+)/)[1];
      if (svgTitle) {
         document.getElementById('svgFileTitle').value = svgTitle;
      }
   }
   documentChanged(getText('action_documentLoading'));
}


window.addEventListener('load', function() {
   svgGraph = document.getElementById('graph');

   numberPoint = getText('numberPoint');
   possibleNumberPoints = new RegExp('[' + getText('possibleNumberPoints') + ']');

   addMarker('selected');
   initSvg();
   window.ondragover = function(evt) {
      evt.preventDefault();
   };
   window.ondrop = function(evt) {
      evt.preventDefault();
      function loadFile(data, fileName) {
         if (data.match(/graphviz/i)) {
            loadDotSvg(data, fileName);
         } else if ((fileName && fileName.endsWith(".svg")) || data.startsWith('<svg ')) {
            loadSvg(data, fileName);
         } else {
            loadAlgorithmText(data, fileName);
         }
      }
      if (evt.dataTransfer.files.length > 0) {
         const file = evt.dataTransfer.files[0];
         file.text().then(data => loadFile(data, file.name));
      } else if (evt.dataTransfer.items.length > 0) {
         evt.dataTransfer.items[0].getAsString(data => loadFile(data));
      }
   };

   document.getElementById('undoButton').onclick = undoDocumentAction;
   document.getElementById('redoButton').onclick = redoDocumentAction;

   for (const button of document.querySelectorAll('#selectionButtons button')) {
      const functionName = button.getAttribute('data-function');
      if (functionName != null) {
         button.onclick = () => window[functionName]();
      }
   }

   document.getElementById('svgLoadFromFile').onclick = function() {
      document.getElementById('svgFileInput').showPicker();
   };
   document.getElementById('svgFileInput').onchange = function() {
      const file = this.files[0];
      file.text().then(data => loadSvg(data, file.name));
   };
   document.getElementById('svgSaveToFile').onclick = function() {
      const link = this.parentElement.querySelector('a[download]');
      link.setAttribute('download', getSvgTitle() + ".svg");
      link.href = 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(getSvgCode());
      link.click();
   };

   const localStorageList = document.getElementById('svgLocalStorageList');
   {
      let nbSaved = 0, graphTitle;
      while ((graphTitle = localStorage.getItem('graph' + nbSaved + '_title')) != null) {
         const option = document.createElement('option');
         option.setAttribute('value', nbSaved.toString());
         option.textContent = graphTitle;
         localStorageList.appendChild(option);
         ++nbSaved;
      }
   }
   document.getElementById('svgSaveToLocalStorage').onclick = function() {
      const nbSaved = localStorageList.options.length, graphTitle = getSvgTitle();
      localStorage.setItem('graph' + nbSaved, LZString.compressToUTF16(svgGraph.outerHTML));
      localStorage.setItem('graph' + nbSaved + '_title', graphTitle);
      const option = document.createElement('option');
      option.setAttribute('value', nbSaved.toString());
      option.textContent = graphTitle;
      localStorageList.appendChild(option);
   };
   document.getElementById('svgLoadFromLocalStorage').onclick = function() {
      const graphIndex = localStorageList.selectedIndex;
      if (graphIndex >= 0) {
         const graphData = LZString.decompressFromUTF16(localStorage.getItem('graph' + graphIndex)),
            graphTitle = localStorage.getItem('graph' + graphIndex + '_title');
         loadSvg(graphData);
         document.getElementById('svgFileTitle').value = graphTitle;
      }
   };
   document.getElementById('svgRemoveFromLocalStorage').onclick = function() {
      let graphIndex = localStorageList.selectedIndex;
      if (graphIndex >= 0 &&
            confirm(getText('questionRemoveGraph', true, [localStorage.getItem('graph' + graphIndex + '_title')]))) {
         localStorageList.remove(graphIndex);
         while (graphIndex < localStorageList.options.length) {
            localStorageList.options[graphIndex].setAttribute('value', graphIndex.toString());
            localStorage.setItem('graph' + graphIndex, localStorage.getItem('graph' + (graphIndex + 1)));
            localStorage.setItem('graph' + graphIndex + '_title', localStorage.getItem('graph' + (graphIndex + 1) + '_title'));
            ++graphIndex;
         }
         localStorage.removeItem('graph' + graphIndex);
         localStorage.removeItem('graph' + graphIndex + '_title');
      }
   };

   document.getElementById('getSvgCode').onclick = function() {
      document.querySelector('#graphDocument textarea').value = getSvgCode();
   };
   document.getElementById('loadSvgCode').onclick = function() {
      loadSvg(document.querySelector('#graphDocument textarea').value);
   };

   let autoLayoutTimeout = null;
   function repeatAutoLayout() {
      autoLayoutGraph();
      autoLayoutTimeout = setTimeout(repeatAutoLayout, 40);
   }
   if (document.getElementById('autoLayoutGraph').checked) {
      repeatAutoLayout();
   }
   document.getElementById('autoLayoutGraph').onchange = function() {
      if (this.checked) {
         repeatAutoLayout();
      } else {
         clearTimeout(autoLayoutTimeout);
      }
   };
});


function getAlphabetData(alphNum) {
   const data = [[10, "0"], [26, "A"], [26, "a"], [24, "Α"], [24, "α"], [32, "А"], [32, "а"]][alphNum];
   return data == undefined ? null : [data[0], data[1].codePointAt(0)];
}

function updateNamingConv() {
   namingConv = svgGraph.vertices.getAttribute('data-naming-conv');
   namingConv = namingConv ? namingConv.split(/\s+/).map(val => parseInt(val)) : [0, 1];
   namingConv = {
      type: getAlphabetData(namingConv[0]) != null ? namingConv[0] : 0,
      first: !isFinite(namingConv[1]) || (namingConv[0] != 0 && namingConv[1] < 0) ? 0 : namingConv[1],
   };
}

function getVertexLabelFromZero(vertexNum) {
   if (namingConv.type == 0) {
      return "" + vertexNum;
   }

   function adjustLetterCode(code) {
      if ([3, 4].includes(namingConv.type) && code >= 17) return code + 1;
      return code;
   }
   const [alphabetLength, firstLetterCode] = getAlphabetData(namingConv.type);
   let str = "";
   let nb = vertexNum + 1;
   while (nb > 0) {
      str = String.fromCodePoint(firstLetterCode + adjustLetterCode((nb - 1) % alphabetLength)) + str;
      nb = Math.trunc((nb - 1) / alphabetLength);
   }
   return str;
}
function getVertexLabel(vertexNum) {
   if (vertexNum === undefined) {
      return "";
   } else if (vertexNum === null) {
      return "—";
   }
   vertexNum = parseInt(vertexNum);
   return getVertexLabelFromZero(namingConv.first + parseInt(vertexNum));
}

function vertexLabelToNumFromZero(vertexLabel) {
   if (namingConv.type == 0) {
      return parseInt(vertexLabel);
   } else if (vertexLabel == undefined) {
      return NaN;
   }

   function adjustLetterCode(code) {
      if ([3, 4].includes(namingConv.type) && code > 17) return code - 1;
      return code;
   }
   const [alphabetLength, firstLetterCode] = getAlphabetData(namingConv.type);
   let num = 0;
   for (let letterNum = 0; letterNum < vertexLabel.length; ++letterNum) {
      const letterValue = adjustLetterCode(vertexLabel.codePointAt(letterNum) - firstLetterCode);
      if (letterValue < 0 || alphabetLength <= letterValue) {
         return NaN;
      }
      num = num * alphabetLength + letterValue + 1;
   }
   return num - 1;
}
function vertexLabelToNum(vertexLabel) {
   return vertexLabelToNumFromZero(vertexLabel) - namingConv.first;
}


function floatToStr(nb) {
   if (!isFinite(nb)) {
      return nb == Infinity ? "∞" : (nb == -Infinity ? "-∞" : "?!NaN!?");
   }
   return nb.toString().replace(".", numberPoint);
}
function strToFloat(str) {
   return str == null ? NaN : parseFloat(str.replace(possibleNumberPoints, "."));
}


function selectAll(elemList, deselect) {
   removeAlgorithmResult();
   elemList = elemList != undefined ? elemList : svgGraph.querySelectorAll('#vertices .vertex, #edges .edge');
   const classMethod = deselect ? 'remove' : 'add';
   for (const elem of elemList) {
      elem.classList[classMethod]('selected');
   }
   updateSelectionControls();
}
var selectAllVertices = () => selectAll(getAllVertices());
var selectAllEdges = () => selectAll(getAllEdges());

function selectLinkedVertices(deselect, singleExtremity) {
   removeAlgorithmResult();
   const classMethod = deselect ? 'remove' : 'add', extremities = singleExtremity ? [singleExtremity] : ['from', 'to'];
   for (const selectedEdge of getGraphSelection('.edge')) {
      for (const extremity of extremities) {
         svgGraph.getElementById(selectedEdge.getAttribute('data-' + extremity)).classList[classMethod]('selected');
      }
   }
   updateSelectionControls();
}
var selectSrcVertices = () => selectLinkedVertices(false, 'from');
var selectDestVertices = () => selectLinkedVertices(false, 'to');
function selectSrcAndDestVertices(deselect) {
   removeAlgorithmResult();
   const classMethod = deselect ? 'remove' : 'add';
   for (const selectedEdge of getGraphSelection('.edge')) {
      const vertexId = selectedEdge.getAttribute('data-to');
      if (svgGraph.edges.querySelector('.edge.selected[data-from="' + vertexId + '"]') != null) {
         svgGraph.getElementById(vertexId).classList[classMethod]('selected');
      }
   }
   updateSelectionControls();
}

function selectIncidentEdges(deselect, singleExtremity) {
   removeAlgorithmResult();
   const classMethod = deselect ? 'remove' : 'add', extremities = singleExtremity ? [singleExtremity] : ['from', 'to'];
   for (const selectedVertex of getGraphSelection('.vertex')) {
      for (const extremity of extremities) {
         for (const edge of svgGraph.edges.querySelectorAll('.edge[data-' + extremity + '="' + selectedVertex.id + '"]')) {
            edge.classList[classMethod]('selected');
         }
      }
   }
   updateSelectionControls();
}
var selectIncomingEdges = () => selectIncidentEdges(false, 'to');
var selectOutgoingEdges = () => selectIncidentEdges(false, 'from');
function selectInBetweenEdges(deselect) {
   removeAlgorithmResult();
   const classMethod = deselect ? 'remove' : 'add';
   for (const edge of getAllEdges()) {
      if (svgGraph.getElementById(edge.getAttribute('data-from')).classList.contains('selected') &&
            svgGraph.getElementById(edge.getAttribute('data-to')).classList.contains('selected')) {
         edge.classList[classMethod]('selected');
      }
   }
   updateSelectionControls();
}

var deselectAll = () => selectAll(undefined, true);
var deselectAllVertices = () => selectAll(getGraphSelection('.vertex'), true);
var deselectAllEdges = () => selectAll(getGraphSelection('.edge'), true);

var deselectLinkedVertices = () => selectLinkedVertices(true);
var deselectSrcVertices = () => selectLinkedVertices(true, 'from');
var deselectDestVertices = () => selectLinkedVertices(true, 'to');
var deselectSrcAndDestVertices = () => selectSrcAndDestVertices(true);

var deselectIncidentEdges = () => selectIncidentEdges(true);
var deselectIncomingEdges = () => selectIncidentEdges(true, 'to');
var deselectOutgoingEdges = () => selectIncidentEdges(true, 'from');
var deselectInBetweenEdges = () => selectInBetweenEdges(true);


function addMarker(name) {
   if (svgGraph.getElementById('edge_niceArrow_' + name) == null) {
      const defs = svgGraph.querySelector('defs'), marker = defs.querySelector('#edge_niceArrow');
      const newMarker = marker.cloneNode();
      newMarker.id += '_' + name;
      newMarker.innerHTML = marker.innerHTML;
      defs.appendChild(newMarker);
   }
}

function addColorization(colorValue) {
   if (!colorValue.match(/^([a-z]+|#\d+)$/)) {
      return null;
   }
   const colorName = colorValue[0] == '#' ? colorValue.substr(1) : colorValue;
   if (svgGraph.querySelector('defs #edge_niceArrow_' + colorName) == null) {
      addMarker(colorName);
      svgGraph.getElementById('colorizations').innerHTML += `
         .colorization_${colorName} circle,
         .colorization_${colorName} .edgePath {
            stroke: ${colorValue};
         }
         .colorization_${colorName} .edgePath {
            marker-end: url('#edge_niceArrow_${colorName}');
         }
         .colorization_${colorName} circle,
         .colorization_${colorName} text,
         text.colorization_${colorName},
         #edge_niceArrow_${colorName} {
            fill: ${colorValue};
         }`;
   }
   return colorName;
}
