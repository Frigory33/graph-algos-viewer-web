let defaultWeight = 1;


function roundVertexCoords(x, y) {
   const gridW = parseFloat(document.getElementById('gridWidth').value),
      gridH = parseFloat(document.getElementById('gridHeight').value);
   x = isNaN(gridW) && gridW != 0 ? x : Math.round(x / gridW) * gridW;
   y = isNaN(gridH) && gridH != 0 ? y : Math.round(y / gridH) * gridH;
   return [x, y];
}

function createVertex(x, y, shadow) {
   [x, y] = roundVertexCoords(x, y);
   const container = shadow ? svgGraph.shadow : svgGraph.vertices,
      vertex = createSvgElement('g', { 'id': 'vertex' + nextVertexId, 'class': 'vertex', 'data-num': nbVertices });
   vertex.innerHTML = '<circle class="white" cx="' + x + '" cy="' + y + '" r="1.5em"/>' +
      '<circle cx="' + x + '" cy="' + y + '" r="1.5em"/>' +
      '<text x="' + x + '" y="' + y + '">' + getVertexLabel(nbVertices) + '</text>';
   container.appendChild(vertex);
   if (!shadow) {
      ++nbVertices;
      ++nextVertexId;
      removeAllAlgorithmicInfo();
   }
   return vertex;
}

function moveVertex(vertex, x, y, noRound) {
   if (!noRound) {
      [x, y] = roundVertexCoords(x, y);
   }
   for (const circle of vertex.querySelectorAll('circle')) {
      circle.setAttribute('cx', x);
      circle.setAttribute('cy', y);
   }
   const text = vertex.querySelector('text');
   text.setAttribute('x', x);
   text.setAttribute('y', y);
   const edgesFrom = [], edgesTo = [];
   for (const edge of
         svgGraph.edges.querySelectorAll('.edge[data-from="' + vertex.id + '"], .edge[data-to="' + vertex.id + '"]')) {
      moveEdge(edge, 1);
   }
   if (vertex.classList.contains('initial')) {
      svgGraph.querySelector('#initialFinal .initialArrow[data-vertex="' + vertex.id + '"]').remove();
      createInitialFinalArrow(svgGraph.getElementById('initialFinal'), 'initial', vertex);
   }
   if (vertex.classList.contains('final')) {
      svgGraph.querySelector('#initialFinal .finalArrow[data-vertex="' + vertex.id + '"]').remove();
      createInitialFinalArrow(svgGraph.getElementById('initialFinal'), 'final', vertex);
   }
}

function generateVertices(nb, method, nbLevels, x1, y1, x2, y2) {
   const left = Math.min(x1, x2), top = Math.min(y1, y2), width = Math.abs(x2 - x1), height = Math.abs(y2 - y1);
   const nbByLevel = Math.ceil(nb / nbLevels);
   for (let vertexNum = 0; vertexNum < nb; ++vertexNum) {
      let vertex;
      if (method == 'grid') {
         vertex = createVertex(left + (vertexNum % nbByLevel + .5) * width / nbByLevel,
            top + (Math.floor(vertexNum / nbByLevel) + .5) * height / nbLevels);
      } else if (method == 'ellipse') {
         vertex = createVertex(left + width / 2 + (nbLevels - Math.floor(vertexNum / nbByLevel)) *
               Math.sin(Math.PI * 2 / nbByLevel * (vertexNum % nbByLevel)) * width / 2 / nbLevels,
            top + height / 2 - (nbLevels - Math.floor(vertexNum / nbByLevel)) *
               Math.cos(Math.PI * 2 / nbByLevel * (vertexNum % nbByLevel)) * height / 2 / nbLevels);
      } else {
         vertex = createVertex(left + Math.random() * width, top + Math.random() * height);
      }
      vertex.classList.add('selected');
   }
}


function createEdge(vertFrom, vertTo, shadow, weight) {
   const container = shadow ? svgGraph.shadow : svgGraph.edges,
      edge = createSvgElement('g', { 'class': 'edge', 'data-from': vertFrom.id, 'data-to': vertTo.id }),
      theWeight = weight == undefined ? defaultWeight : typeof(weight) == 'number' ? floatToStr(weight) : weight;
   edge.innerHTML = '<text class="weight">' + theWeight + '</text>';
   container.appendChild(edge);
   moveEdge(edge);
   if (!shadow) {
      removeAllAlgorithmicInfo();
   }
   return edge;
}

function moveEdge(edge, curvePos) {
   function roundCoord(coord) {
      return Math.round(coord * 100.) / 100.;
   }
   const vertFrom = document.getElementById(edge.getAttribute('data-from')),
      vertTo = document.getElementById(edge.getAttribute('data-to')),
      weight = edge.querySelector('.weight').textContent;
   const graphRect = svgGraph.getBoundingClientRect(),
      rectFrom = vertFrom.getBoundingClientRect(), rectTo = vertTo.getBoundingClientRect();
   let xFrom = rectFrom.x + rectFrom.width / 2 - graphRect.x, yFrom = rectFrom.y + rectFrom.height / 2 - graphRect.y,
      xTo = rectTo.x + rectTo.width / 2 - graphRect.x, yTo = rectTo.y + rectTo.height / 2 - graphRect.y;
   const arrowWidth = graphIsUndirected ? 0 :
         parseFloat(document.getElementById('edge_niceArrow').getAttribute('markerWidth')) * 2 * sizeFactor;
   let svgCode = '';
   if (vertFrom == vertTo) {
      let angle = Math.PI * 1.75;
      if (typeof curvePos == 'number') {
         const edgePath = edge.querySelector('.edgePath');
         angle = edgePath == null ? 0 : parseFloat(edgePath.getAttribute('data-angle')) || 0;
      } else if (curvePos) {
         angle = -Math.atan2(xFrom - curvePos.x, yFrom - curvePos.y) - Math.PI / 4;
      }
      angle = (angle % (Math.PI * 2) + (Math.PI * 2)) % (Math.PI * 2);
      const angleSin = Math.sin(angle), angleCos = Math.cos(angle);
      const x1 = rectFrom.x + rectFrom.width * (1 + angleSin) / 2 - graphRect.x,
         y1 = rectFrom.y + rectFrom.height * (1 - angleCos) / 2 - graphRect.y,
         x2 = rectFrom.x + rectFrom.width * (1 + angleCos) / 2 + arrowWidth * angleCos - graphRect.x,
         y2 = rectFrom.y + rectFrom.height * (1 + angleSin) / 2 + arrowWidth * angleSin - graphRect.y;
      const pathStr = 'd="M ' + roundCoord(x1) + ',' + roundCoord(y1) +
            ' A ' + roundCoord(rectFrom.width * .75) + ',' + roundCoord(rectFrom.height * .75) + ' 1 1 1 ' +
               roundCoord(x2) + ',' + roundCoord(y2) + '"';
      svgCode += '<path class="thickInvisible" ' + pathStr + '/>' +
         '<path class="edgePath" ' + pathStr + ' data-angle="' + roundCoord(angle) + '"/>';
      const textAngle = angle + Math.PI / 4, angleForText = angle % (Math.PI / 2) - Math.PI / 4,
         textShift = angle < Math.PI / 2 || (Math.PI <= angle && angle < Math.PI * 3 / 2) ? 2 : 2.25;
         textX = xFrom + rectFrom.width * Math.sin(textAngle) * textShift,
         textY = yFrom - rectFrom.height * Math.cos(textAngle) * textShift;
      const weightTextClass = angle < Math.PI / 2 ? ' leftAligned' :
            Math.PI <= angle && angle < Math.PI * 3 / 2 ? ' rightAligned' : '';
      svgCode += '<text class="weight' + weightTextClass + '" x="' + roundCoord(textX) + '" y="' + roundCoord(textY) + '" ' +
         'transform="rotate(' + roundCoord(angleForText * 180 / Math.PI) + ', ' +
            roundCoord(textX) + ', ' + roundCoord(textY) + ')">' + weight + '</text>';
   } else {
      const distanceX = xTo - xFrom, distanceY = yTo - yFrom, angle = Math.atan2(distanceY, distanceX);
      const angleForText = angle + Math.PI / 2 * (Math.trunc(-angle / (Math.PI / 4)) + Math.trunc(angle / (Math.PI / 2))),
         angleForShift = angle + Math.PI / 2, absoluteAngle = Math.abs(angle),
         centerX = (xFrom + xTo) / 2, centerY = (yFrom + yTo) / 2;
      let weightTextClass = '', textShift = 20;
      if (Math.PI / 4 <= absoluteAngle && absoluteAngle < Math.PI * 3 / 4) {
         weightTextClass = ' ' + (angle < 0 ? 'left' : 'right') + 'Aligned';
         textShift = 8;
      }
      textShift *= sizeFactor;
      let textX = centerX + Math.cos(angleForShift) * textShift, textY = centerY + Math.sin(angleForShift) * textShift;
      let curvingAmount = null;
      if (typeof curvePos == 'number') {
         curvingAmount = parseFloat(edge.querySelector('.edgePath').getAttribute('data-curving-amount')) * curvePos;
      }
      if (curvePos && !Number.isNaN(curvingAmount)) {
         if (curvingAmount == null) {
            const curveDistX = curvePos.x - centerX, curveDistY = curvePos.y - centerY,
               curveAngle = Math.atan2(curveDistY, curveDistX) - angle;
            curvingAmount = Math.sin(curveAngle) * Math.sqrt(curveDistX * curveDistX + curveDistY * curveDistY);
         }
         curveX = centerX + Math.cos(angleForShift) * curvingAmount * 2,
         curveY = centerY + Math.sin(angleForShift) * curvingAmount * 2;
         textX += Math.cos(angleForShift) * curvingAmount;
         textY += Math.sin(angleForShift) * curvingAmount;
         const angleFrom = Math.atan2(curveY - yFrom, curveX - xFrom), angleTo = Math.atan2(yTo - curveY, xTo - curveX);
         xFrom += Math.cos(angleFrom) * rectFrom.width / 2;
         yFrom += Math.sin(angleFrom) * rectFrom.height / 2;
         xTo -= Math.cos(angleTo) * (rectTo.width / 2 + arrowWidth);
         yTo -= Math.sin(angleTo) * (rectTo.height / 2 + arrowWidth);
         const newCenterX = (xFrom + xTo) / 2, newCenterY = (yFrom + yTo) / 2;
         curveX -= newCenterX - centerX;
         curveY -= newCenterY - centerY;
         const pathStr = 'd="M ' + roundCoord(xFrom) + ',' + roundCoord(yFrom) +
               ' Q ' + roundCoord(curveX) + ',' + roundCoord(curveY) + ' ' + roundCoord(xTo) + ',' + roundCoord(yTo) + '"';
         svgCode += '<path class="thickInvisible" ' + pathStr + '/>' +
            '<path class="edgePath" ' + pathStr + ' data-curving-amount="' + curvingAmount + '"/>';
      } else {
         xFrom += Math.cos(angle) * rectFrom.width / 2;
         yFrom += Math.sin(angle) * rectFrom.height / 2;
         xTo -= Math.cos(angle) * (rectTo.width / 2 + arrowWidth);
         yTo -= Math.sin(angle) * (rectTo.height / 2 + arrowWidth);
         const coordsStr = 'x1="' + roundCoord(xFrom) + '" y1="' + roundCoord(yFrom) + '" ' +
            'x2="' + roundCoord(xTo) + '" y2="' + roundCoord(yTo) + '"';
         svgCode += '<line class="thickInvisible" ' + coordsStr + '/>' +
            '<line class="edgePath" ' + coordsStr + '/>';
      }
      svgCode += '<text class="weight' + weightTextClass + '" x="' + roundCoord(textX) + '" y="' + roundCoord(textY) + '" ' +
         'transform="rotate(' + roundCoord(angleForText * 180 / Math.PI) + ', ' +
            roundCoord(textX) + ', ' + roundCoord(textY) + ')">' + weight + '</text>';
   }
   edge.innerHTML = svgCode;
}


function removeElems(elems) {
   const initialNbVertices = nbVertices;
   for (const elem of elems) {
      elem.remove();
      if (elem.classList.contains('vertex')) {
         const incidentEdges =
            svgGraph.edges.querySelectorAll('.edge[data-from="' + elem.id + '"], .edge[data-to="' + elem.id + '"]');
         for (const edge of incidentEdges) {
            edge.remove();
         }
         if (elem.classList.contains('initial')) {
            svgGraph.querySelector('#initialFinal .initialArrow[data-vertex="' + elem.id + '"]').remove();
         }
         if (elem.classList.contains('final')) {
            svgGraph.querySelector('#initialFinal .finalArrow[data-vertex="' + elem.id + '"]').remove();
         }
         --nbVertices;
      }
   }
   let vertexNum = 0;
   for (let initialVNum = 0; initialVNum < initialNbVertices; ++initialVNum) {
      const vertex = getVertex(initialVNum);
      if (vertex != null) {
         vertex.querySelector('text').textContent = getVertexLabel(vertexNum);
         vertex.setAttribute('data-num', vertexNum);
         ++vertexNum;
      }
   }
   removeAllAlgorithmicInfo();
}


function autoLayoutGraph() {
   for (const vertex of getAllVertices()) {
      const circle = vertex.querySelector('circle');
      vertex.pos = { x: parseFloat(circle.getAttribute('cx')), y: parseFloat(circle.getAttribute('cy')) };
      vertex.move = { x: 0, y: 0 };
   }
   for (const vertex1 of getAllVertices()) {
      for (const vertex2 of getAllVertices()) {
         if (vertex1 != vertex2) {
            const connected = svgGraph.edges.querySelectorAll(
                  '[data-from="' + vertex1.id + '"][data-to="' + vertex2.id + '"], ' +
                  '[data-from="' + vertex2.id + '"][data-to="' + vertex1.id + '"]').length > 0,
               distX = vertex2.pos.x - vertex1.pos.x, distY = vertex2.pos.y - vertex1.pos.y,
               distSq = distX * distX + distY * distY;
            if (distSq == 0) {
               const angle = Math.random() * Math.PI * 2;
               vertex1.move.x += Math.cos(angle);
               vertex1.move.y += Math.sin(angle);
            } else if (connected) {
               const distXSgn = distX < 0 ? -1 : distX == 0 ? 0 : 1, distYSgn = distY < 0 ? -1 : distY == 0 ? 0 : 1;
               vertex1.move.x += .005 * Math.log(distSq / 2500) * distX - distXSgn;
               vertex1.move.y += .005 * Math.log(distSq / 2500) * distY - distYSgn;
            } else if (distSq < 50000) {
               vertex1.move.x -= 50 / distSq * distX;
               vertex1.move.y -= 50 / distSq * distY;
            }
         }
      }
   }
   const vertexRadius = 25 * sizeFactor;
   for (const vertex of getAllVertices()) {
      const newX = Math.min(Math.max(vertexRadius, vertex.pos.x + vertex.move.x), svgGraph.clientWidth - vertexRadius),
         newY = Math.min(Math.max(vertexRadius, vertex.pos.y + vertex.move.y), svgGraph.clientHeight - vertexRadius);
      moveVertex(vertex, newX, newY, true);
      delete vertex.pos;
      delete vertex.move;
   }
}
