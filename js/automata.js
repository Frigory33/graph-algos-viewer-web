function getGraphAsText() {
   const automaton = loadAutomaton();
   let output = automaton.nbStates + " " + automaton.nbTransitions + "\n" +
      namingConv.type + " " + namingConv.first + "\n";
   for (const transiData of automaton.transitions) {
      output += transiData.src + " " + transiData.dest + " " + transiData.letters.join(",") + "\n";
   }
   output += automaton.word + "\n" + automaton.expression + "\n";
   return output;
}


function updateSelectionControls() {
   const vertices = svgGraph.vertices.querySelectorAll('.vertex.selected').length > 0;
   for (const button of document.querySelectorAll('#automataInitialToggle, #automataFinalToggle')) {
      if (vertices) {
         button.removeAttribute('disabled');
      } else {
         button.setAttribute('disabled', '');
      }
   }
}


function getInitialFinalContainer() {
   let container = svgGraph.getElementById('initialFinal');
   if (container == null) {
      container = createSvgElement('g', { id: 'initialFinal' });
      svgGraph.insertBefore(container, svgGraph.getElementById('edges').nextSibling);
   }
   return container;
}

function createInitialFinalArrow(container, className, vertex) {
   const circle = vertex.querySelector('circle'), vertexRect = vertex.getBoundingClientRect(),
      cx = parseFloat(circle.getAttribute('cx')), cy = parseFloat(circle.getAttribute('cy')),
      arrowWidth = parseFloat(document.getElementById('edge_niceArrow').getAttribute('markerWidth')) * 2 * sizeFactor,
      isInitial = className == 'initial';
   const group = createSvgElement('g', { 'class': className + 'Arrow', 'data-vertex': vertex.id });
   group.append(createSvgElement('line', {
         'class': 'edgePath',
         x1: cx + vertexRect.width * (isInitial ? -1.5 : .5), y1: cy,
         x2: cx + vertexRect.width * (isInitial ? -.5 : 1.5) - arrowWidth, y2: cy
      }));
   container.append(group);
}

function toggleInitialFinal(className) {
   const container = getInitialFinalContainer();
   for (const vertex of svgGraph.vertices.querySelectorAll('.vertex.selected')) {
      if (vertex.classList.contains(className)) {
         container.querySelector('.' + className + 'Arrow[data-vertex="' + vertex.id + '"]').remove();
      } else {
         createInitialFinalArrow(container, className, vertex);
      }
      vertex.classList.toggle(className);
   }
   documentChanged(getText('action_' + className + 'Toggle'));
}

document.getElementById('automataInitialToggle').onclick = function() {
   toggleInitialFinal('initial');
};
document.getElementById('automataFinalToggle').onclick = function() {
   toggleInitialFinal('final');
};


function addWellState() {
   removeAllAlgorithmicInfo();
   const automaton = loadAutomaton();
   let well = null, transiAdded = false;
   for (let sNum = 0; sNum < automaton.nbStates; ++sNum) {
      const lettersToAdd = new Set();
      automaton.alphabet.forEach(letter => lettersToAdd.add(letter));
      for (const transi of automaton.states[sNum]) {
         if (transi.letter.length == 1) {
            lettersToAdd.delete(transi.letter);
         }
      }
      if (lettersToAdd.size != 0) {
         if (well == null) {
            well = createVertex(svgGraph.clientWidth / 2, svgGraph.clientHeight / 2);
         }
         const vertex = svgGraph.vertices.querySelector('[data-num="' + sNum + '"]');
         createEdge(vertex, well, false, Array.from(lettersToAdd.values()).join(", "));
      }
   }
   if (well != null) {
      createEdge(well, well, false, automaton.alphabet.join(", "));
      deselectAll();
      well.classList.add('selected');
      return true;
   }
   return false;
}
document.getElementById('addWellState').onclick = function() {
   if (!addWellState()) {
      alert("L’automate est déjà complet.");
   }
};

function mergeParallelTransi() {
   removeAllAlgorithmicInfo();
   let modified = false;
   for (const vertex of getAllVertices()) {
      const destVertices = new Set();
      for (const edge of svgGraph.edges.querySelectorAll('.edge[data-from="' + vertex.id + '"]')) {
         const dest = edge.getAttribute('data-to');
         if (!destVertices.has(dest)) {
            destVertices.add(dest);
            const letters = new Set();
            edge.querySelector('.weight').textContent.split(", ").forEach(letter => letters.add(letter));
            for (const parallelEdge of
                  svgGraph.edges.querySelectorAll('.edge[data-from="' + vertex.id + '"][data-to="' + dest + '"]')) {
               if (parallelEdge != edge) {
                  modified = true;
                  parallelEdge.querySelector('.weight').textContent.split(", ").forEach(letter => letters.add(letter));
                  parallelEdge.remove();
               }
            }
            edge.querySelector('.weight').textContent = Array.from(letters.values()).sort().join(", ");
         }
      }
   }
   if (modified) {
      documentChanged(getText('action_parallelTransiMerge'));
   }
}
document.getElementById('mergeParallelTransi').onclick = mergeParallelTransi;
