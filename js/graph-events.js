let boxDrawingStart = null, newEdgeSource = null, hasQuittedVertex = false,
   moveStartPos = null, verticesPosBeforeMove = null,
   longTouchTimeout = null, isTouching = false;


function getMouseOffset(evt) {
   const svgBounds = svgGraph.getBoundingClientRect();
   return { x: evt.clientX - svgBounds.x, y: evt.clientY - svgBounds.y, clientX: evt.clientX, clientY: evt.clientY };
}
function getPointedGroup(x, y) {
   const pointed = document.elementFromPoint(x, y);
   return pointed == null ? null : pointed.closest('g');
}

function onMouseDown(evt) {
   const mousePos = getMouseOffset(evt);
   if ([Tools.GENERATE, Tools.SELECT, Tools.ZOOM].includes(currentTool)) {
      boxDrawingStart = { x: mousePos.x, y: mousePos.y };
   } else if (currentTool == Tools.CREATE) {
      const selectedVertices = getGraphSelection('.vertex');
      deselectAll();
      hasQuittedVertex = false;
      let pointedElem = getPointedGroup(evt.clientX, evt.clientY);
      if (pointedElem == null) {
         const [x, y] = roundVertexCoords(mousePos.x, mousePos.y), graphRect = svgGraph.getBoundingClientRect();
         pointedElem = getPointedGroup(graphRect.x + x, graphRect.y + y);
         if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
            pointedElem = createVertex(mousePos.x, mousePos.y);
            documentChanged(getText('action_vertexCreation'));
         }
      }
      pointedElem.classList.add('selected');
      if (evt.shiftKey && selectedVertices.length == 1 && pointedElem.classList.contains('vertex') &&
            pointedElem != selectedVertices[0]) {
         createEdge(selectedVertices[0], pointedElem);
         documentChanged(getText('action_linkCreation'));
      }
   } else if (currentTool == Tools.MOVE) {
      moveStartPos = { x: mousePos.x, y: mousePos.y };
      let pointedElem = getPointedGroup(evt.clientX, evt.clientY);
      if (!evt.shiftKey && (pointedElem == null || !pointedElem.classList.contains('selected'))) {
         deselectAll();
      }
      if (pointedElem != null) {
         pointedElem.classList.add('selected');
      }
      verticesPosBeforeMove = new Array(nbVertices);
      for (const selectedVertex of getAllVertices()) {
         const circle = selectedVertex.querySelector('circle');
         verticesPosBeforeMove[parseInt(selectedVertex.getAttribute('data-num'))] =
            { x: parseFloat(circle.getAttribute('cx')), y: parseFloat(circle.getAttribute('cy')) };
      }
   }
   updateSelectionControls();
}

function onBoxMouseUp(evt, mousePos) {
   const boxX1 = Math.min(boxDrawingStart.x, mousePos.x), boxY1 = Math.min(boxDrawingStart.y, mousePos.y),
      boxX2 = Math.max(boxDrawingStart.x, mousePos.x), boxY2 = Math.max(boxDrawingStart.y, mousePos.y);
   const graphRect = svgGraph.getBoundingClientRect();
   if (currentTool == Tools.GENERATE) {
      const form = document.querySelector('#generateVertices'),
         nbToGenerate = Math.min(parseInt(form.elements.number.value) || 0, 1000);
      if (nbToGenerate > 0) {
         generateVertices(nbToGenerate, form.elements.vertexLayout.value,
            Math.max(1, parseInt(form.elements.nbLevels.value) || 1),
            boxDrawingStart.x, boxDrawingStart.y, mousePos.x, mousePos.y);
         documentChanged(getText('action_verticesGeneration'));
      }
   } else if (currentTool == Tools.ZOOM) {
      const isBackward = document.getElementById('zoomBackward').checked;
      let ratioX = (boxX2 - boxX1) / graphRect.width, ratioY = (boxY2 - boxY1) / graphRect.height,
         baseX = 0, baseY = 0, boxX = boxX1, boxY = boxY1;
      document.getElementById('gridWidth').value = '';
      document.getElementById('gridHeight').value = '';
      if (isBackward) {
         ratioX = 1 / ratioX;
         ratioY = 1 / ratioY;
         baseX = boxX1;
         baseY = boxY1;
         boxX = 0;
         boxY = 0;
      }
      for (const vertex of getAllVertices()) {
         const circle = vertex.querySelector('circle'),
            vX = parseFloat(circle.getAttribute('cx')), vY = parseFloat(circle.getAttribute('cy'));
         moveVertex(vertex, baseX + (vX - boxX) / ratioX, baseY + (vY - boxY) / ratioY, true);
      }
      documentChanged(getText(isBackward ? 'action_zoomBackward' : 'action_zoomForward'));
   } else {
      for (const elem of svgGraph.querySelectorAll('#vertices .vertex, #edges .edge')) {
         const elemRect = elem.getBoundingClientRect(),
            left = elemRect.x - graphRect.x, top = elemRect.y - graphRect.y;
         if (boxX1 <= left && left + elemRect.width <= boxX2 && boxY1 <= top && top + elemRect.height <= boxY2) {
            elem.classList.add('selected');
         }
      }
   }
}
function onMouseUp(evt) {
   const mousePos = getMouseOffset(evt);
   svgGraph.shadow.innerHTML = '';
   if ([Tools.GENERATE, Tools.SELECT, Tools.ZOOM].includes(currentTool)) {
      if (currentTool == Tools.ZOOM || !evt.shiftKey) {
         deselectAll();
      }
      if (boxDrawingStart == null) {
      } else if (Math.abs(boxDrawingStart.x - mousePos.x) > 5 && Math.abs(boxDrawingStart.y - mousePos.y) > 5) {
         onBoxMouseUp(evt, mousePos);
      } else if (currentTool == Tools.SELECT) {
         const pointedElem = getPointedGroup(evt.clientX, evt.clientY);
         if (pointedElem != null) {
            pointedElem.classList.toggle('selected');
         }
      } else if (currentTool == Tools.ZOOM) {
         const isBackward = document.getElementById('zoomBackward').checked;
         const ratio = isBackward ? 1 / 1.1 : 1.1;
         document.getElementById('gridWidth').value = '';
         document.getElementById('gridHeight').value = '';
         for (const vertex of getAllVertices()) {
            const circle = vertex.querySelector('circle'),
               vX = parseFloat(circle.getAttribute('cx')), vY = parseFloat(circle.getAttribute('cy'));
            moveVertex(vertex, mousePos.x + (vX - mousePos.x) * ratio, mousePos.y + (vY - mousePos.y) * ratio, true);
         }
         documentChanged(getText(isBackward ? 'action_zoomBackward' : 'action_zoomForward'));
      }
      boxDrawingStart = null;
   } else if (currentTool == Tools.CREATE) {
      const selectedElems = getGraphSelection();
      if (selectedElems.length == 1) {
         const selectedElem = selectedElems[0];
         let pointedElem = getPointedGroup(evt.clientX, evt.clientY);
         if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
            const [x, y] = roundVertexCoords(mousePos.x, mousePos.y), graphRect = svgGraph.getBoundingClientRect();
            pointedElem = getPointedGroup(graphRect.x + x, graphRect.y + y);
         }
         if (selectedElem.classList.contains('vertex')) {
            if (pointedElem == selectedElem) {
               if (hasQuittedVertex) {
                  createEdge(selectedElem, selectedElem);
                  documentChanged(getText('action_linkCreation'));
               }
            } else {
               selectedElem.classList.remove('selected');
               if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
                  pointedElem = createVertex(mousePos.x, mousePos.y);
                  documentChanged(getText('action_vertexCreation'));
               }
               createEdge(selectedElem, pointedElem);
               documentChanged(getText('action_linkCreation'));
               pointedElem.classList.add('selected');
            }
         } else if (selectedElem.classList.contains('edge')) {
            if (newEdgeSource != null) {
               if (pointedElem == newEdgeSource) {
                  if (hasQuittedVertex) {
                     selectedElem.setAttribute('data-from', newEdgeSource.id);
                     selectedElem.setAttribute('data-to', newEdgeSource.id);
                     moveEdge(selectedElem, 1);
                     documentChanged(getText('action_linkExtremitiesModification'));
                  }
               } else {
                  if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
                     pointedElem = createVertex(mousePos.x, mousePos.y);
                     documentChanged(getText('action_vertexCreation'));
                  }
                  selectedElem.setAttribute('data-from', newEdgeSource.id);
                  selectedElem.setAttribute('data-to', pointedElem.id);
                  moveEdge(selectedElem, 1);
                  documentChanged(getText('action_linkExtremitiesModification'));
               }
            }
            newEdgeSource = null;
         }
      }
      hasQuittedVertex = false;
   } else if (currentTool == Tools.MOVE) {
      const selectedElems = getGraphSelection();
      if (moveStartPos != null && (mousePos.x != moveStartPos.x || mousePos.y != moveStartPos.y)) {
         if (selectedElems.length == 1) {
            if (selectedElems[0].classList.contains('vertex')) {
               documentChanged(getText('action_vertexMoving'));
            } else if (selectedElems[0].classList.contains('edge')) {
               moveEdge(selectedElems[0], { x: mousePos.x, y: mousePos.y });
               documentChanged(getText('action_linkCurving'));
            }
         } else {
            documentChanged(getText('action_elementsMoving'));
         }
      }
      moveStartPos = null;
      verticesPosBeforeMove = null;
   }
   updateSelectionControls();
   showAlgoState();
}

function onMouseMove(evt) {
   const mousePos = getMouseOffset(evt);
   svgGraph.shadow.innerHTML = '';
   if ([Tools.GENERATE, Tools.SELECT, Tools.ZOOM].includes(currentTool)) {
      if (boxDrawingStart != null && boxDrawingStart.x != mousePos.x && boxDrawingStart.y != mousePos.y) {
         if (currentTool == Tools.GENERATE &&
               document.querySelector('#generateVertices').elements.vertexLayout.value == 'ellipse') {
            svgGraph.shadow.innerHTML +=
               '<ellipse class="boxDrawing" cx="' + (boxDrawingStart.x + mousePos.x) / 2 + '" ' +
                  'cy="' + (boxDrawingStart.y + mousePos.y) / 2 + '" ' +
                  'rx="' + Math.abs(boxDrawingStart.x - mousePos.x) / 2 + '" ' +
                  'ry="' + Math.abs(boxDrawingStart.y - mousePos.y) / 2 + '"/>';
         } else {
            svgGraph.shadow.innerHTML +=
               '<rect class="boxDrawing" x="' + Math.min(boxDrawingStart.x, mousePos.x) + '" ' +
                  'y="' + Math.min(boxDrawingStart.y, mousePos.y) + '" ' +
                  'width="' + Math.abs(boxDrawingStart.x - mousePos.x) + '" ' +
                  'height="' + Math.abs(boxDrawingStart.y - mousePos.y) + '"/>';
         }
      }
   } else if (currentTool == Tools.CREATE) {
      const selectedElems = getGraphSelection();
      if (selectedElems.length == 1) {
         const selectedElem = selectedElems[0];
         let pointedElem = getPointedGroup(evt.clientX, evt.clientY);
         if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
            const [x, y] = roundVertexCoords(mousePos.x, mousePos.y), graphRect = svgGraph.getBoundingClientRect();
            pointedElem = getPointedGroup(graphRect.x + x, graphRect.y + y);
            hasQuittedVertex = true;
         }
         if (selectedElem.classList.contains('vertex')) {
            if (pointedElem != selectedElem || hasQuittedVertex) {
               if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
                  pointedElem = createVertex(mousePos.x, mousePos.y, true);
               }
               svgGraph.shadow.prepend(createEdge(selectedElem, pointedElem, true));
            }
         } else if (selectedElem.classList.contains('edge')) {
            if (newEdgeSource == null) {
               if (pointedElem != null && pointedElem.classList.contains('vertex')) {
                  newEdgeSource = pointedElem;
               }
            } else if (pointedElem != newEdgeSource || hasQuittedVertex) {
               if (pointedElem == null || !pointedElem.classList.contains('vertex')) {
                  pointedElem = createVertex(mousePos.x, mousePos.y, true);
               }
               const shadowEdge = createEdge(newEdgeSource, pointedElem, true);
               shadowEdge.innerHTML = selectedElem.innerHTML;
               moveEdge(shadowEdge, 1);
               svgGraph.shadow.prepend(shadowEdge);
            }
         }
      }
   } else if (currentTool == Tools.MOVE) {
      let selectedElems = getGraphSelection();
      if (selectedElems.length == 1) {
         const selectedElem = selectedElems[0];
         if (selectedElem.classList.contains('vertex') && verticesPosBeforeMove != null) {
            const vPos = verticesPosBeforeMove[parseInt(selectedElem.getAttribute('data-num'))];
            moveVertex(selectedElem, vPos.x + mousePos.x - moveStartPos.x, vPos.y + mousePos.y - moveStartPos.y);
         } else if (selectedElem.classList.contains('edge')) {
            const vFrom = svgGraph.getElementById(selectedElem.getAttribute('data-from')),
               vTo = svgGraph.getElementById(selectedElem.getAttribute('data-to'));
            const shadowEdge = createEdge(vFrom, vTo, true, selectedElem.querySelector('.weight').textContent);
            moveEdge(shadowEdge, { x: mousePos.x, y: mousePos.y });
         }
      } else if (verticesPosBeforeMove != null) {
         if (selectedElems.length == 0) {
            selectedElems = getAllVertices();
         }
         for (const elem of selectedElems) {
            if (elem.classList.contains('vertex')) {
               const vPos = verticesPosBeforeMove[parseInt(elem.getAttribute('data-num'))];
               moveVertex(elem, vPos.x + mousePos.x - moveStartPos.x, vPos.y + mousePos.y - moveStartPos.y);
            }
         }
      }
   }
}

function onDblClick(evt) {
   const selectedElems = getGraphSelection(), selectedElem = selectedElems[0];
   if (selectedElems.length != 1) {
   } else if (selectedElem.classList.contains('edge')) {
      if (evt.shiftKey) {
         moveEdge(selectedElem);
         documentChanged(getText('action_linkUncurving'));
      } else {
         const promptText = { 'weighted': 'giveNewLinkWeight', 'automata': 'giveNewLinkLetters' }[currentSubject];
         const weightText = selectedElem.querySelector('.weight'),
            newWeightStr = prompt(getText(promptText, true), weightText.textContent);
         if (newWeightStr == null) {
         } else if (currentSubject == 'weighted') {
            const newWeight = strToFloat(newWeightStr);
            if (!Number.isNaN(newWeight) && newWeight != 0) {
               weightText.textContent = floatToStr(newWeight);
               documentChanged(getText('action_linkWeightModification'));
            } else if (newWeightStr != undefined && newWeightStr != null && newWeightStr != '') {
               alert(getText('invalidLinkValue', true));
            }
         } else {
            let letters = newWeightStr.split(",").map(letter => letter.trim()), uniqueLetters = "",
               lastLetter = "", emptyLetter = false;
            letters.sort();
            for (const letter of letters) {
               if (letter == "" || letter == "ε") {
                  emptyLetter = true;
               } else if (letter != lastLetter) {
                  uniqueLetters += (uniqueLetters == "" ? "" : ", ") + letter;
                  lastLetter = letter;
               }
            }
            if (emptyLetter) {
               uniqueLetters += (uniqueLetters == "" ? "" : ", ") + "ε";
            }
            weightText.textContent = uniqueLetters;
            documentChanged(getText('action_linkLettersModification'));
         }
      }
   } else if (selectedElem.classList.contains('vertex')) {
      const newNumStr = prompt(getText('giveNewVertexLabel', true, [getVertexLabel(0), getVertexLabel(nbVertices - 1)]),
            getVertexLabel(selectedElem.getAttribute('data-num'))),
         newNum = newNumStr == '' ? NaN : vertexLabelToNum(newNumStr);
      if (!Number.isNaN(newNum) && 0 <= newNum && newNum < nbVertices) {
         function changeVertexNum(oldNum, newNum) {
            const vertex = getVertex(oldNum);
            vertex.setAttribute('data-num', newNum);
            vertex.querySelector('text').textContent = getVertexLabel(newNum);
         }
         const origNum = parseInt(selectedElem.getAttribute('data-num'));
         for (let vertexNum = origNum; vertexNum < newNum; ++vertexNum) {
            changeVertexNum(vertexNum + 1, vertexNum);
         }
         for (let vertexNum = origNum; vertexNum > newNum; --vertexNum) {
            changeVertexNum(vertexNum - 1, vertexNum);
         }
         selectedElem.setAttribute('data-num', newNum);
         selectedElem.querySelector('text').textContent = getVertexLabel(newNum);
         documentChanged(getText('action_vertexLabelModification'));
      } else if (newNumStr != undefined && newNumStr != null && newNumStr != '') {
         alert(getText('invalidInput', true));
      }
   }
}

function onKeyDown(evt) {
   let actionDone = true;
   const algoStatesButtons = document.getElementById('algoStatesButtons');
   const keyMod = (evt.ctrlKey || evt.metaKey ? 'Ctrl+' : '') + (evt.altKey ? 'Alt+' : '') + (evt.shiftKey ? 'Shift+' : ''),
      keyComb = keyMod + evt.key, physKeyComb = keyMod + evt.code;
   switch (keyComb) {
      case 'Escape':
         svgGraph.shadow.innerHTML = '';
         if (currentTool == Tools.MOVE && verticesPosBeforeMove != null) {
            let selectedVertices = getGraphSelection('.vertex');
            if (selectedVertices.length <= 0 && svgGraph.querySelector('.selected') == null) {
               selectedVertices = getAllVertices();
            }
            for (const vertex of selectedVertices) {
               const vPos = verticesPosBeforeMove[parseInt(vertex.getAttribute('data-num'))];
               moveVertex(vertex, vPos.x, vPos.y, true);
            }
            moveStartPos = null;
            verticesPosBeforeMove = null;
         }
         deselectAll();
         boxDrawingStart = null;
         break;
      case 'Home':
         if (!algoStatesButtons.classList.contains('hide')) {
            algoStatesButtons.children[0].click();
         } else if (svgGraph.edges.querySelector('.edge.selected') != null) {
            for (const selectedEdge of getGraphSelection('.edge')) {
               svgGraph.edges.prepend(selectedEdge);
            }
            documentChanged(getText('action_placingLinksFirst'));
         } else {
            actionDone = false;
         }
         break;
      case 'End':
         if (!algoStatesButtons.classList.contains('hide')) {
            algoStatesButtons.children[4].click();
         } else if (svgGraph.edges.querySelector('.edge.selected') != null) {
            for (const selectedEdge of getGraphSelection('.edge')) {
               svgGraph.edges.appendChild(selectedEdge);
            }
            documentChanged(getText('action_placingLinksLast'));
         } else {
            actionDone = false;
         }
         break;
      case 'ArrowLeft':
         if (!algoStatesButtons.classList.contains('hide')) {
            algoStatesButtons.children[1].click();
         } else {
            actionDone = false;
         }
         break;
      case 'ArrowRight':
         if (!algoStatesButtons.classList.contains('hide')) {
            algoStatesButtons.children[3].click();
         } else {
            actionDone = false;
         }
         break;
      case 'Delete':
      case 'Backspace':
         let selection = getGraphSelection();
         if (selection.length > 0) {
            removeElems(selection);
            documentChanged(getText('action_selectedElementsDeletion'));
            updateSelectionControls();
         }
         break;
      case 'Ctrl+a':
         selectAll();
         break;
      case 'Ctrl+o':
         document.getElementById('svgLoadFromFile').dispatchEvent(new Event('click'));
         break;
      case 'Ctrl+s':
         document.getElementById('svgSaveToFile').dispatchEvent(new Event('click'));
         break;
      case 'Ctrl+z':
         undoDocumentAction();
         break;
      case 'Ctrl+Shift+Z':
      case 'Ctrl+y':
         redoDocumentAction();
         break;
      default:
         actionDone = false;
   }
   const modeButtons = document.querySelectorAll('#modesSection > .toggleGroup > button'),
      tabButtons = document.querySelectorAll('#tabs > .toggleGroup > button');
   let modeNum = -1;
   if (physKeyComb.startsWith('Digit')) {
      modeNum = parseInt(physKeyComb.substring(5));
   } else if (keyComb.match(/^\d+$/) != null) {
      modeNum = parseInt(keyComb);
   } else if (keyComb.match(/^Shift\+F\d+$/) != null) {
      const tabNum = parseInt(keyComb.substring(7)) - 1;
      if (0 <= tabNum && tabNum < tabButtons.length) {
         tabButtons[tabNum].dispatchEvent(new Event('click'));
         actionDone = true;
      }
   }
   const firstModeNum = currentSubject == 'weighted' ? 0 : 1;
   if (firstModeNum <= modeNum && modeNum < modeButtons.length) {
      modeButtons[modeNum].dispatchEvent(new Event('click'));
      actionDone = true;
   }
   if (actionDone) {
      evt.preventDefault();
   }
}

function onLongTouch(evt) {
   const touch = evt.changedTouches[0], pointedElem = getPointedGroup(touch.clientX, touch.clientY);
   if (pointedElem != null) {
      isTouching = false;
      if (!pointedElem.classList.contains('selected')) {
         deselectAll();
         pointedElem.classList.add('selected');
      }
      setTimeout(function() {
            if (currentTool == Tools.SELECT) {
               if (confirm(getText('questionDeleteSelectedElements', true))) {
                  removeElems(getGraphSelection());
                  documentChanged(getText('action_selectedElementsDeletion'));
               }
            } else {
               svgGraph.dispatchEvent(new MouseEvent('dblclick',
                  { clientX: touch.clientX, clientY: touch.clientY, button: 0, buttons: 1 }));
            }
         }, 0);
   }
}
function onTouch(evt) {
   evt.preventDefault();
   if (longTouchTimeout != null) {
      clearTimeout(longTouchTimeout);
      longTouchTimeout = null;
   }
   if (evt.type == 'touchcancel') {
      svgGraph.shadow = '';
      isTouching = false;
   } else if (evt.touches.length <= (evt.type == 'touchend' ? 0 : 1) && (evt.type == 'touchstart' || isTouching)) {
      const type = 'mouse' + { start: 'down', move: 'move', end: 'up' }[evt.type.substr(5)], touch = evt.changedTouches[0],
         shiftKey = evt.type == 'touchend' && currentTool == Tools.SELECT &&
            getPointedGroup(touch.clientX, touch.clientY) != null;
      svgGraph.dispatchEvent(new MouseEvent(type,
            { clientX: touch.clientX, clientY: touch.clientY, button: 0, buttons: 1, shiftKey: shiftKey }));
      if (evt.type == 'touchstart') {
         isTouching = true;
         longTouchTimeout = setTimeout(() => onLongTouch(evt), 500);
      } else if (evt.type == 'touchend') {
         isTouching = false;
      }
   }
}


window.addEventListener('load', function() {
   svgGraph.onmousedown = function(evt) {
      if (evt.button == 0) {
         removeAlgorithmResult();
         onMouseDown(evt);
      }
   };
   svgGraph.onmouseup = function(evt) {
      if (evt.button == 0) {
         removeAlgorithmResult();
         onMouseUp(evt);
      }
   };
   svgGraph.onmousemove = function(evt) {
      if (evt.buttons & 1) {
         onMouseMove(evt);
      }
   };
   svgGraph.onmouseleave = function() {
      svgGraph.shadow.innerHTML = '';
   };
   svgGraph.ondblclick = function(evt) {
      removeAllAlgorithmicInfo();
      onDblClick(evt);
   };
   svgGraph.ontouchstart = svgGraph.ontouchmove = svgGraph.ontouchend = svgGraph.ontouchcancel = onTouch;
   window.onkeydown = function(evt) {
      if (['SELECT', 'INPUT', 'TEXTAREA'].includes(evt.target.tagName)) {
         if (evt.key == 'Escape') {
            evt.target.blur();
         }
         return;
      }
      onKeyDown(evt);
   };
});
