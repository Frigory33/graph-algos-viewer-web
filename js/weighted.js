function getGraphAsText(flags) {
   flags = flags == undefined ? 0 : flags;
   const graph = loadGraph(flags);
   let output = graph.nbVertices + " " + graph.nbEdges + " " + (graph.isUndirected ? 1 : 0) + " " + flags + "\n" +
      namingConv.type + " " + namingConv.first + "\n" +
      graph.selectedVertices.join(" ") + "\n" + graph.selectedEdges.join(" ") + "\n";
   for (const edgeData of graph.edges) {
      output += edgeData.src + " " + edgeData.dest + " " + edgeData.weight + "\n";
   }
   if ((flags & 1) == 1) {
      for (const vPos of graph.verticesPos) {
         output += vPos.x + " " + vPos.y + "\n";
      }
   }
   return output;
}


function updateSelectionControls() {}


document.getElementById('generateEdges').onsubmit = function(evt) {
   evt.preventDefault();
   const allowAntiParallel = this.elements.antiParallel.checked && !graphIsUndirected, adjMatrix = loadGraph().adjMatrix;
   possibleEdges = [];
   if (this.elements.takeWhich.value == 'all') {
      for (let vNum1 = 0; vNum1 < nbVertices; ++vNum1) {
         for (let vNum2 = allowAntiParallel ? 0 : vNum1 + 1; vNum2 < nbVertices; ++vNum2) {
            if (vNum1 != vNum2 && adjMatrix[vNum1][vNum2] == 0 && (allowAntiParallel || adjMatrix[vNum2][vNum1] == 0)) {
               possibleEdges.push([vNum1, vNum2]);
            }
         }
      }
   } else {
      const verticesPos = new Array(nbVertices).fill(null).map(function(val, index) {
               const circle = getVertex(index).querySelector('circle');
               return { x: parseInt(circle.getAttribute('cx')), y: parseInt(circle.getAttribute('cy')) };
            });
      const sortedVertices = new Array(nbVertices).fill(null).map((val, index) => index)
            .sort(function(vNum1, vNum2) {
               const pos1 = verticesPos[vNum1], pos2 = verticesPos[vNum2];
               return (pos1.x * pos1.x + pos1.y * pos1.y) - (pos2.x * pos2.x + pos2.y * pos2.y);
            });
      const verticesInGrid = [];
      for (const vNum of sortedVertices) {
         if (verticesInGrid.length == 0) {
            verticesInGrid.push([vNum]);
         } else {
            const pos1 = verticesPos[vNum];
            let searchBound1 = 0, searchBound2 = verticesInGrid.length - 1;
            while (searchBound1 + 1 < searchBound2) {
               const searchMiddle = Math.trunc((searchBound1 + searchBound2) / 2),
                  row2 = verticesInGrid[searchMiddle], vNum2 = row2[row2.length - 1], pos2 = verticesPos[vNum2];
               if (pos1.y < pos2.y) {
                  searchBound2 = searchMiddle;
               } else {
                  searchBound1 = searchMiddle;
               }
            }
            const row2 = verticesInGrid[searchBound1], vNum2 = row2[row2.length - 1], pos2 = verticesPos[vNum2],
               row3 = verticesInGrid[searchBound2], vNum3 = row3[row3.length - 1], pos3 = verticesPos[vNum3],
               diff2 = { x: pos1.x - pos2.x, y: Math.abs(pos1.y - pos2.y) },
               diff3 = { x: pos1.x - pos3.x, y: Math.abs(pos1.y - pos3.y) };
            let [selectedRow, selectedDiff, newRowNum] = [searchBound1, diff2, searchBound2];
            if (pos1.y <= pos2.y) {
               newRowNum = searchBound1;
            } else if (pos1.y >= pos3.y) {
               [selectedRow, selectedDiff, newRowNum] = [searchBound2, diff3, searchBound2 + 1];
            } else if (diff2.y > diff3.y) {
               [selectedRow, selectedDiff] = [searchBound2, diff3];
            }
            if (selectedDiff.x > selectedDiff.y) {
               verticesInGrid[selectedRow].push(vNum);
            } else {
               const newRow = [], row4 = verticesInGrid[selectedRow];
               while (newRow.length + 1 < row4.length) {
                  const vNum5 = row4[newRow.length], vNum6 = row4[newRow.length + 1];
                  if (vNum5 == null || vNum6 == null) {
                     break;
                  }
                  const diff5 = pos1.x - verticesPos[vNum5].x, diff6 = verticesPos[vNum6].x - pos1.x;
                  if (diff6 >= diff5) {
                     break;
                  }
                  newRow.push(null);
               }
               newRow.push(vNum);
               verticesInGrid.splice(newRowNum, 0, newRow);
            }
         }
      }
      const directions = [[0, 1], [1, 0]], randomOrientation = !graphIsUndirected && !allowAntiParallel;
      if (this.elements.takeWhich.value == '8dirs') {
         directions.push([-1, 1], [1, 1]);
      }
      for (let rowNum = 0; rowNum < verticesInGrid.length; ++rowNum) {
         for (let colNum = 0; colNum < verticesInGrid[rowNum].length; ++colNum) {
            for (const [dRow, dCol] of directions) {
               const rowNum2 = rowNum + dRow, colNum2 = colNum + dCol;
               if (0 <= rowNum2 && rowNum2 < verticesInGrid.length &&
                     0 <= colNum2 && colNum2 < verticesInGrid[rowNum2].length) {
                  const vNum1 = verticesInGrid[rowNum][colNum], vNum2 = verticesInGrid[rowNum2][colNum2];
                  if (vNum1 != null && vNum2 != null) {
                     if (randomOrientation) {
                        if (adjMatrix[vNum1][vNum2] == 0 && adjMatrix[vNum2][vNum1] == 0) {
                           possibleEdges.push(Math.random() < .5 ? [vNum1, vNum2] : [vNum2, vNum1]);
                        }
                     } else {
                        if (adjMatrix[vNum1][vNum2] == 0) {
                           possibleEdges.push([vNum1, vNum2]);
                        }
                        if (!graphIsUndirected && adjMatrix[vNum2][vNum1] == 0) {
                           possibleEdges.push([vNum2, vNum1]);
                        }
                     }
                  }
               }
            }
         }
      }
   }
   for (let edgeNum = possibleEdges.length - 1; edgeNum > 0; --edgeNum) {
      let edgeNum2 = Math.floor(Math.random() * (edgeNum + 1));
      [possibleEdges[edgeNum], possibleEdges[edgeNum2]] = [possibleEdges[edgeNum2], possibleEdges[edgeNum]];
   }
   const nbEdgesByV = parseFloat(this.elements.quantity.value);
   let minWeight = parseFloat(this.elements.minWeight.value), maxWeight = parseFloat(this.elements.maxWeight.value),
      nbDecimals = parseFloat(this.elements.precision.value);
   nbDecimals = isNaN(nbDecimals) ? 0 : Math.max(0, Math.min(nbDecimals, 15));
   if (isNaN(nbEdgesByV) || isNaN(minWeight) || isNaN(maxWeight)) {
      alert(getText('atLeastOneInvalidValue', true));
      return;
   }
   if (minWeight > maxWeight) {
      [minWeight, maxWeight] = [maxWeight, minWeight];
   }
   const edgeNumEnd = Math.min(Math.ceil(nbEdgesByV * nbVertices / 2), possibleEdges.length);
   let precision = 10 ** parseFloat(nbDecimals), intervalSize = maxWeight - minWeight;
   if (intervalSize * precision < 1 && minWeight <= 0 && 0 <= maxWeight) {
      intervalSize = 0;
      minWeight = maxWeight = 1;
   }
   for (let edgeNum = 0; edgeNum < edgeNumEnd; ++edgeNum) {
      const [vNum1, vNum2] = possibleEdges[edgeNum];
      let weight;
      while ((weight = Math.round((Math.random() * intervalSize + minWeight) * precision) / precision) == 0);
      const edge = createEdge(getVertex(vNum1), getVertex(vNum2), false, weight);
      edge.classList.add('selected');
      const opposedEdge = getEdge(vNum2, vNum1);
      if (opposedEdge != null) {
         for (const edge2 of [edge, opposedEdge]) {
            edge2.querySelector('.edgePath').setAttribute('data-curving-amount', 50);
            moveEdge(edge2, 1);
         }
      }
   }
   if (edgeNumEnd > 0) {
      documentChanged(getText('action_linksGeneration'));
   }
};


const vNamingFirst = document.getElementById('vertexNamingFirst');

document.getElementById('vertexNamingType').onchange = function() {
   const firstChange = svgGraph.vertices.getAttribute('data-naming-conv') == null;
   svgGraph.vertices.setAttribute('data-naming-conv', this.value + ' ' + (firstChange ? 0 : namingConv.first));
   updateNamingConv();
   vNamingFirst.value = getVertexLabel(0);
   for (const vertex of getAllVertices()) {
      vertex.querySelector('text').textContent = getVertexLabel(parseInt(vertex.getAttribute('data-num')));
   }
   documentChanged(getText('action_vertexNamingTypeModification'));
};

vNamingFirst.onchange = function() {
   svgGraph.vertices.setAttribute('data-naming-conv', namingConv.type + ' ' + (vertexLabelToNumFromZero(this.value) || 0));
   updateNamingConv();
   for (const vertex of getAllVertices()) {
      vertex.querySelector('text').textContent = getVertexLabel(parseInt(vertex.getAttribute('data-num')));
   }
   documentChanged(getText('action_vertexNamingFirstModification'));
};

document.getElementById('vertexNamingForward').onclick = function() {
   vNamingFirst.value = getVertexLabel(1);
   vNamingFirst.onchange();
};

document.getElementById('vertexNamingBackward').onclick = function() {
   const backward = getVertexLabel(-1);
   if (backward) {
      vNamingFirst.value = backward;
      vNamingFirst.onchange();
   }
};

document.getElementById('showWeights').onchange = function() {
   svgGraph.classList.toggle('hideWeights');
   documentChanged(getText('action_weights' + (svgGraph.classList.contains('hideWeights') ? 'Hiding' : 'Displaying')));
};

document.getElementById('graphIsDirected').onclick = function() {
   removeAllAlgorithmicInfo();
   svgGraph.classList.toggle('undirectedGraph');
   document.getElementById('graphContainer').classList.toggle('undirectedGraph');
   graphIsUndirected = !graphIsUndirected;
   for (const edge of getAllEdges()) {
      moveEdge(edge, 1);
   }
   documentChanged(getText('action_switchingTo' + (graphIsUndirected ? 'UndirectedGraph' : 'DirectedGraph')));
};

document.getElementById('assignRealDistToEdges').onclick = function() {
   removeAllAlgorithmicInfo();
   for (const edge of getAllEdges()) {
      const vFrom = svgGraph.getElementById(edge.getAttribute('data-to')),
         vTo = svgGraph.getElementById(edge.getAttribute('data-from'));
      const circFrom = vFrom.querySelector('circle'), circTo = vTo.querySelector('circle');
      const xDiff = parseFloat(circFrom.getAttribute('cx')) - parseFloat(circTo.getAttribute('cx')),
         yDiff = parseFloat(circFrom.getAttribute('cy')) - parseFloat(circTo.getAttribute('cy'));
      const dist = Math.round(Math.sqrt(xDiff * xDiff + yDiff * yDiff) * 1000) / 1000;
      edge.querySelector('.weight').textContent = floatToStr(dist);
   }
   documentChanged(getText('action_usingRealDistancesAsWeights'));
};

document.getElementById('multiplyEdgeWeights').onclick = function() {
   let factor = prompt(getText('giveMultiplicationFactor', true), "1");
   if (factor) {
      factor = strToFloat(factor);
      if (!factor) {
         alert(getText('invalidInput', true));
      } else {
         removeAllAlgorithmicInfo();
         for (const edge of getAllEdges()) {
            const weightText = edge.querySelector('.weight');
            weightText.textContent = floatToStr(strToFloat(weightText.textContent) * factor);
         }
         documentChanged(getText('action_multiplicationOfWeightsBy', false, [floatToStr(factor)]));
      }
   }
};

document.getElementById('roundEdgeWeights').onclick = function() {
   let nbFigs = prompt(getText('giveNumberOfDecimalsToKeep', true), "0");
   if (nbFigs) {
      nbFigs = parseInt(nbFigs);
      if (isNaN(nbFigs)) {
         alert(getText('invalidInput', true));
      } else {
         const factor = 10 ** nbFigs;
         removeAllAlgorithmicInfo();
         for (const edge of getAllEdges()) {
            const weightText = edge.querySelector('.weight'), oldWeight = strToFloat(weightText.textContent);
            let newWeight = Math.round(oldWeight * factor) / factor;
            newWeight = newWeight == 0 ? (oldWeight < 0 ? -1 : 1) : newWeight;
            weightText.textContent = floatToStr(newWeight);
         }
         documentChanged(getText('action_roundingWeights', false, [nbFigs]));
      }
   }
};


document.getElementById('invertEdges').onclick = function() {
   removeAllAlgorithmicInfo();
   for (const edge of getAllEdges()) {
      const idFrom = edge.getAttribute('data-to'), idTo = edge.getAttribute('data-from');
      edge.setAttribute('data-from', idFrom);
      edge.setAttribute('data-to', idTo);
      moveEdge(edge, -1);
   }
   documentChanged(getText('action_edgesDirectionInversion'));
};

document.getElementById('mergeParallelEdges').onclick = function() {
   removeAllAlgorithmicInfo();
   const initialNbEdges = getAllEdges().length,
      foundEdges = new Array(nbVertices).fill(null).map(() => new Array(nbVertices).fill(false));
   for (const edge of getAllEdges()) {
      const vertFromNum = parseInt(svgGraph.getElementById(edge.getAttribute('data-from')).getAttribute('data-num')),
         vertToNum = parseInt(svgGraph.getElementById(edge.getAttribute('data-to')).getAttribute('data-num'));
      if (foundEdges[vertFromNum][vertToNum]) {
         edge.remove();
      } else {
         foundEdges[vertFromNum][vertToNum] = true;
         if (graphIsUndirected) {
            foundEdges[vertToNum][vertFromNum] = true;
         }
      }
   }
   if (getAllEdges().length != initialNbEdges) {
      documentChanged(getText('action_parallelEdgesMerge'));
   }
};


let sortEdgesResultTimeout = null;
document.querySelector('#sortEdges form').onsubmit = function(evt) {
   evt.preventDefault();
   const verticesNums = {};
   for (const vertex of getAllVertices()) {
      verticesNums[vertex.id] = parseInt(vertex.getAttribute('data-num'));
   }
   let edges = Array.from(getAllEdges());
   const comparisonFct = {
         ASD: (svNum1, svNum2, dvNum1, dvNum2) => svNum1 != svNum2 ? svNum1 - svNum2 : dvNum1 - dvNum2,
         DSD: (svNum1, svNum2, dvNum1, dvNum2) => svNum2 != svNum1 ? svNum2 - svNum1 : dvNum2 - dvNum1,
         ADS: (svNum1, svNum2, dvNum1, dvNum2) => dvNum1 != dvNum2 ? dvNum1 - dvNum2 : svNum1 - svNum2,
         DDS: (svNum1, svNum2, dvNum1, dvNum2) => dvNum2 != dvNum1 ? dvNum2 - dvNum1 : svNum2 - svNum1,
      }[this.elements.sortMethod.value];
   edges.sort((edge1, edge2) => {
         let svNum1 = verticesNums[edge1.getAttribute('data-from')], svNum2 = verticesNums[edge2.getAttribute('data-from')],
            dvNum1 = verticesNums[edge1.getAttribute('data-to')], dvNum2 = verticesNums[edge2.getAttribute('data-to')];
         if (graphIsUndirected) {
            if (svNum1 > dvNum1) [svNum1, dvNum1] = [dvNum1, svNum1];
            if (svNum2 > dvNum2) [svNum2, dvNum2] = [dvNum2, svNum2];
         }
         return comparisonFct(svNum1, svNum2, dvNum1, dvNum2);
      });
   for (const edge of edges) {
      svgGraph.edges.appendChild(edge);
   }
   documentChanged(getText('action_linksSorting'));
   sortEdgesResultTimeout = showTemporaryMsg(this.querySelector('.msgContainer'), getText('sortingDone'), 5000,
      sortEdgesResultTimeout);
};


function addOrUpdateEdge(vFrom, vTo, weight, edgesToKeep) {
   let edge = getEdgeByIds(vFrom.id, vTo.id, graphIsUndirected, edgesToKeep);
   if (edge != null) {
      edge.querySelector('.weight').textContent = floatToStr(weight);
   } else {
      edge = createEdge(vFrom, vTo, false, weight);
   }
   edgesToKeep.add(edge);
}

function removeEdgesNotIn(edgesToKeep) {
   for (const edge of Array.from(getAllEdges())) {
      if (!edgesToKeep.has(edge)) {
         edge.remove();
      }
   }
}

document.getElementById('calcEdgesList').onclick = function() {
   const edgesList = loadGraph().edges;
   let edgesListStr = "";
   for (const edge of edgesList) {
      edgesListStr += getVertexLabel(edge.src) + " " + getVertexLabel(edge.dest) + " " + floatToStr(edge.weight) + "\n";
   }
   document.querySelector('#graphRepresentations textarea').value = edgesListStr.trimEnd();
};

document.getElementById('applyEdgesList').onclick = function() {
   removeAllAlgorithmicInfo();
   const edgesListStrLines = document.querySelector('#graphRepresentations textarea').value.split("\n");
   const edgesToKeep = new Set();
   for (const edgeDesc of edgesListStrLines) {
      let [vFrom, vTo, weight] = edgeDesc.trim().split(/\s+/);
      vFrom = getVertex(vertexLabelToNum(vFrom));
      vTo = getVertex(vertexLabelToNum(vTo));
      weight = strToFloat(weight || "1") || 1;
      if (vFrom != null && vTo != null) {
         addOrUpdateEdge(vFrom, vTo, weight, edgesToKeep);
      }
   }
   removeEdgesNotIn(edgesToKeep);
   documentChanged(getText('action_edgesListApplication'));
};

document.getElementById('calcGenGraphFormat').onclick = function() {
   const edgesList = loadGraph().edges, edgeRepr = graphIsUndirected ? "-" : "->";
   let edgesListStr = "";
   let edgeCount = 0;
   for (const edge of edgesList) {
      edgeCount = (edgeCount + 1) % 12;
      edgesListStr += getVertexLabel(edge.src) + edgeRepr + getVertexLabel(edge.dest) + (edgeCount != 0 ? " " : "\n");
   }
   edgesListStr = edgesListStr.trim() + "\n";
   document.querySelector('#graphRepresentations textarea').value = edgesListStr.trimEnd();
};

document.getElementById('applyGenGraphFormat').onclick = function() {
   removeAllAlgorithmicInfo();
   svgGraph.edges.innerHTML = '';
   const pathsDescList = document.querySelector('#graphRepresentations textarea').value.split(/\s+/);
   for (const pathDesc of pathsDescList) {
      let vFrom, vTo = null;
      for (const vNum of pathDesc.split(/->?/)) {
         vFrom = vTo;
         vTo = getVertex(vertexLabelToNum(vNum));
         if (vFrom != null && vTo != null) {
            createEdge(vFrom, vTo);
         }
      }
   }
   documentChanged(getText('action_genGraphFormatApplication'));
};

function calcAdjLists(adjLists) {
   let adjListsStr = "", vertexNum = 0;
   for (const adjL of adjLists) {
      adjListsStr += getVertexLabel(vertexNum) + " :";
      for (const adj of adjL) {
         adjListsStr += " " + getVertexLabel(adj.vertex) + "|" + floatToStr(adj.weight);
      }
      adjListsStr += "\n";
      ++vertexNum;
   }
   document.querySelector('#graphRepresentations textarea').value = adjListsStr.trimEnd();
}

function applyAdjLists(inverted) {
   removeAllAlgorithmicInfo();
   const adjListsLines = document.querySelector('#graphRepresentations textarea').value.split("\n");
   const edgesToKeep = new Set();
   let vertexNum = 0;
   for (const adjListDesc of adjListsLines) {
      let vert1 = getVertex(vertexNum);
      for (const adjDesc of adjListDesc.matchAll(/(\S+)\|(\S+)/g)) {
         let vert2 = getVertex(vertexLabelToNum(adjDesc[1])), weight = strToFloat(adjDesc[2] || "1") || 1;
         if (vert1 != null && vert2 != null) {
            const [vFrom, vTo] = inverted ? [vert2, vert1] : [vert1, vert2];
            addOrUpdateEdge(vFrom, vTo, weight, edgesToKeep);
         }
      }
      ++vertexNum;
   }
   removeEdgesNotIn(edgesToKeep);
   documentChanged(getText('action_adjListsApplication'));
}

document.getElementById('calcSuccLists').onclick = () => calcAdjLists(loadGraph().succLists);
document.getElementById('applySuccLists').onclick = () => applyAdjLists();
document.getElementById('calcPredLists').onclick = () => calcAdjLists(loadGraph().predLists);
document.getElementById('applyPredLists').onclick = () => applyAdjLists(true);

document.getElementById('calcAdjMatrix').onclick = function() {
   function addSpaces(str, nbChars) {
      let spaces = "";
      for (let spaceI = str.length; spaceI < nbChars; ++spaceI) {
         spaces += " ";
      }
      return spaces + str;
   }

   const adjMatrix = loadGraph().adjMatrix, firstColLen = Math.max(2, getVertexLabel(nbVertices - 1).length),
      colLengths = new Array(nbVertices).fill(2);
   let matrixStr = addSpaces("", firstColLen);
   for (let colI = 0; colI < nbVertices; ++colI) {
      colLengths[colI] = Math.max(getVertexLabel(colI).length, colLengths[colI]);
      for (let rowI = 0; rowI < adjMatrix.length; ++rowI) {
         colLengths[colI] = Math.max(floatToStr(adjMatrix[rowI][colI]).length, colLengths[colI]);
      }
      matrixStr += " " + addSpaces(getVertexLabel(colI), colLengths[colI]);
   }
   for (let rowI = 0; rowI < adjMatrix.length; ++rowI) {
      matrixStr += "\n" + addSpaces(getVertexLabel(rowI), firstColLen);
      for (let colI = 0; colI < adjMatrix[rowI].length; ++colI) {
         matrixStr += " " + addSpaces(floatToStr(adjMatrix[rowI][colI]), colLengths[colI]);
      }
   }
   document.querySelector('#graphRepresentations textarea').value = matrixStr;
};

document.getElementById('applyAdjMatrix').onclick = function() {
   removeAllAlgorithmicInfo();
   const matrixStrLines = document.querySelector('#graphRepresentations textarea').value.split("\n"),
      lastRowNum = Math.min(matrixStrLines.length - 1, nbVertices);
   for (let rowI = 1; rowI <= lastRowNum; ++rowI) {
      const rowVals = matrixStrLines[rowI].trim().split(/\s+/), lastColNum = Math.min(rowVals.length - 1, nbVertices);
      for (let colI = 1; colI <= lastColNum; ++colI) {
         const vertFromNum = rowI - 1, vertToNum = colI - 1;
         let weight = strToFloat(rowVals[colI]);
         if (Number.isNaN(weight)) {
            weight = 0;
         }
         let edge = getEdge(vertFromNum, vertToNum, graphIsUndirected);
         if (weight != 0) {
            if (edge == null) {
               edge = createEdge(getVertex(vertFromNum), getVertex(vertToNum));
            }
            edge.querySelector('.weight').textContent = floatToStr(weight);
         } else if (edge != null) {
            edge.remove();
         }
      }
   }
   documentChanged(getText('action_adjMatrixApplication'));
};

document.getElementById('calcIncMatrix').onclick = function() {
   function addSpaces(str, nbChars) {
      let spaces = "";
      for (let spaceI = str.length; spaceI < nbChars; ++spaceI) {
         spaces += " ";
      }
      return spaces + str;
   }

   const edges = loadGraph().edges, firstColLen = Math.max(2, getVertexLabel(nbVertices - 1).length),
      colLengths = new Array(edges.length).fill(2);
   let matrixStr = addSpaces("", firstColLen);
   for (let colI = 0; colI < edges.length; ++colI) {
      colLengths[colI] = Math.max(("" + colI).length, colLengths[colI]);
      colLengths[colI] = Math.max(floatToStr(edges[colI].weight).length, colLengths[colI]);
      if (!graphIsUndirected) {
         colLengths[colI] = Math.max(floatToStr(-edges[colI].weight).length, colLengths[colI]);
      }
      matrixStr += " " + addSpaces("" + colI, colLengths[colI]);
   }
   for (let rowI = 0; rowI < nbVertices; ++rowI) {
      matrixStr += "\n" + addSpaces(getVertexLabel(rowI), firstColLen);
      for (let colI = 0; colI < edges.length; ++colI) {
         const edge = edges[colI],
            weight = edge.dest == rowI || (graphIsUndirected && edge.src == rowI) ? edge.weight :
               (edge.src == rowI ? -edge.weight : 0);
         matrixStr += " " + addSpaces(floatToStr(weight), colLengths[colI]);
      }
   }
   document.querySelector('#graphRepresentations textarea').value = matrixStr;
};
