function createEnum(values) {
   const enumObject = {};
   for (let valI = 0; valI < values.length; ++valI) {
      enumObject[values[valI]] = valI;
   }
   return Object.freeze(enumObject);
}


function getText(id, asText, args) {
   const textContainer = document.getElementById('text_' + id);
   if (textContainer == null) {
      return "!!!Texte introuvable/Text not found!!!";
   }
   let text = textContainer[asText ? 'textContent' : 'innerHTML'].trim().replace(/\s+/g, " ");
   if (args != undefined) {
      for (let argNum = 0; argNum < args.length; ++argNum) {
         text = text.replace('{$' + argNum + '}', args[argNum]);
      }
   }
   return text;
}


function updateElemsSize() {
   const svgGraph = document.getElementById('graph');
   const fontSize = 14 * sizeFactor, edgeThick = 4 * sizeFactor, circleThick = 2 * sizeFactor, invisibleThick = 30 * sizeFactor;
   let zoomStyle = svgGraph.getElementById('zoomStyle');
   if (zoomStyle == null) {
      zoomStyle = createSvgElement('style', { id: 'zoomStyle' });
      svgGraph.insertBefore(zoomStyle, document.getElementById('colorizations').nextSibling);
   }
   zoomStyle.textContent = ' ' +
      'svg { font-size: ' + fontSize + 'px; } ' +
      '.edgePath, #initialFinal > line { stroke-width: ' + edgeThick + 'px; } ' +
      'circle { stroke-width: ' + circleThick + 'px; } ' +
      '.thickInvisible { stroke-width: ' + invisibleThick + 'px; } ';
   for (const edge of svgGraph.querySelectorAll('#edges .edge')) {
      moveEdge(edge, 1);
   }
   const initialFinalContainer = svgGraph.getElementById('initialFinal');
   if (initialFinalContainer != null) {
      initialFinalContainer.innerHTML = '';
      for (const vertex of svgGraph.querySelectorAll('#vertices .vertex.initial')) {
         createInitialFinalArrow(initialFinalContainer, 'initial', vertex);
      }
      for (const vertex of svgGraph.querySelectorAll('#vertices .vertex.final')) {
         createInitialFinalArrow(initialFinalContainer, 'final', vertex);
      }
   }
}


window.addEventListener('load', function() {
   {
      function toggleGroupClickFct() {
         function toggle(button) {
            if (button) {
               button.classList.toggle('selected');
               if (button.classList.contains('tab')) {
                  if (button.classList.contains('collapsed')) {
                     button.classList.remove('collapsed');
                  } else {
                     document.getElementById(button.getAttribute('data-target')).classList.toggle('show');
                  }
               }
            }
         }
         if (this.classList.contains('selected')) {
            if (!this.parentElement.classList.contains('toggleGroupNeedOne')) {
               toggle(this);
            } else if (this.classList.contains('collapsible')) {
               this.classList.toggle('collapsed');
               document.getElementById(this.getAttribute('data-target')).classList.toggle('show');
            }
         } else {
            toggle(this.parentElement.querySelector('.selected'));
            toggle(this);
         }
      }
      for (const button of document.querySelectorAll('.toggleGroup button')) {
         button.addEventListener('click', toggleGroupClickFct);
      }

      window.Tools = createEnum(['GENERATE', 'CREATE', 'MOVE', 'SELECT', 'ZOOM']);
      let buttonI = 0;
      for (const button of document.querySelectorAll('#modesSection .toggleGroup button')) {
         const myValue = buttonI;
         button.addEventListener('click', function() {
            window.currentTool = myValue;
         });
         ++buttonI;
      }
      window.currentTool = Tools.CREATE;
   }

   const graphContainer = document.getElementById('graphContainer');

   {
      window.layoutIsVertical = false;
      function doChangeLayout() {
         const setProp = layoutIsVertical ? 'width' : 'height', unsetProp = layoutIsVertical ? 'height' : 'width',
            prevVal = graphContainer.getAttribute('data-' + setProp);
         document.body.classList[layoutIsVertical ? 'add' : 'remove']('verticalMode');
         graphContainer.style[setProp] = prevVal != null ? prevVal : '50%';
         if (graphContainer.style[unsetProp]) {
            graphContainer.setAttribute('data-' + unsetProp, graphContainer.style[unsetProp]);
            graphContainer.style.removeProperty(unsetProp);
         }
      }
      doChangeLayout();

      let hashParams = {};

      document.getElementById('toggleHorizVertSep').onclick = function() {
         if (layoutIsVertical) {
            delete hashParams['layout'];
         } else {
            hashParams['layout'] = 'vertical';
         }
         layoutIsVertical = !layoutIsVertical;
         doChangeLayout();
         writeHashParams();
      };

      const sizeRange = document.getElementById('elemsSizeFactorRange'),
         sizeText = document.getElementById('elemsSizeFactorText');
      function changeElemsSize(value) {
         window.sizeFactor = value;
         updateElemsSize();
         if (value == 1) {
            delete hashParams['sizeFactor'];
         } else {
            hashParams['sizeFactor'] = value.toString();
         }
         writeHashParams();
      }
      sizeRange.onmousemove = function() {
         sizeText.value = this.value;
      };
      sizeRange.onchange = function() {
         sizeText.value = this.value;
         changeElemsSize(parseFloat(this.value));
      };
      sizeText.onchange = function() {
         sizeRange.value = this.value;
         changeElemsSize(parseFloat(this.value));
      };
      window.sizeFactor = 1;

      window.onhashchange = function() {
         const hashParamsSplitted = location.hash.substr(1).split('|');
         for (let hashParam of hashParamsSplitted) {
            let hashNameVal = hashParam.split('=');
            if (hashNameVal.length >= 2) {
               hashParams[hashNameVal[0]] = hashNameVal[1];
            }
         }
         parseHashParams();
      }
      function writeHashParams() {
         let hashParamsList = [];
         for (let hashPar in hashParams) {
            hashParamsList.push(hashPar + '=' + hashParams[hashPar]);
         }
         location.replace('#' + hashParamsList.join('|'));
      }
      function parseHashParams() {
         if (hashParams['subject'] == undefined) {
            hashParams['subject'] = 'weighted';
            writeHashParams();
            location.reload();
            throw '';
         } else if (window.currentSubject != undefined && currentSubject != hashParams['subject']) {
            location.reload();
            throw '';
         }
         window.currentSubject = hashParams['subject'];
         document.body.classList.add(currentSubject);
         defaultWeight = currentSubject == 'automata' ? 'ε' : 1;
         if ((hashParams['layout'] == 'vertical') != layoutIsVertical) {
            layoutIsVertical = !layoutIsVertical;
            doChangeLayout();
         }
         let sizeFactor = parseFloat(hashParams['sizeFactor']);
         if (!isNaN(sizeFactor) && sizeFactor != window.sizeFactor) {
            sizeRange.value = sizeText.value = sizeFactor;
            changeElemsSize(sizeFactor);
         }
      };

      window.onhashchange();
   }

   {
      const svgServerList = document.getElementById('svgServerList');
      fetch('samples/' + currentSubject + '.json').then(response => response.json()).then(json => {
            for (const svgName of json) {
               const option = document.createElement('option');
               option.setAttribute('value', svgName + '.svg');
               option.textContent = svgName;
               svgServerList.appendChild(option);
            }
         });
      document.getElementById('svgLoadFromServer').onclick = function() {
         if (svgServerList.selectedIndex >= 0) {
            const fileName = svgServerList.value;
            fetch('samples/' + encodeURIComponent(fileName)).then(response => response.text()).then(data => {
                  loadSvg(data);
               });
         }
      };
   }

   {
      const separator = document.getElementById('separator');
      let movingSeparator = false;
      separator.onmousedown = function(evt) {
         if (evt.button == 0) {
            movingSeparator = true;
            separator.classList.add('grabbed');
         }
      };
      window.addEventListener('mousemove', function(evt) {
         if (!movingSeparator) {
         } else if ((evt.buttons & 1) != 1) {
            window.dispatchEvent(new MouseEvent('mouseup', { button: 0 }));
         } else if (document.body.classList.contains('verticalMode')) {
            graphContainer.style.width = (graphContainer.offsetWidth + evt.movementX) + 'px';
         } else {
            graphContainer.style.height = (graphContainer.offsetHeight + evt.movementY) + 'px';
         }
      });
      window.addEventListener('mouseup', function(evt) {
         if (evt.button == 0) {
            movingSeparator = false;
            separator.classList.remove('grabbed');
         }
      });
   }
});


function showTemporaryMsg(container, text, duration, prevTimeout, asHtml) {
   if (prevTimeout != null) {
      clearTimeout(prevTimeout);
   }
   container[asHtml ? 'innerHTML' : 'textContent'] = text;
   return setTimeout(function() {
         container.innerHTML = '';
      }, duration);
}
