window.addEventListener('load', function() {
   function addScript(path) {
      const scriptTag = document.createElement('script');
      scriptTag.src = path;
      document.head.appendChild(scriptTag);
   }
   addScript('js/' + currentSubject + '.js');
   addScript('apis/' + currentSubject + '/api.js');

   const algoNameSelect = document.getElementById('algorithmName'),
      requiredClassName = 'for' + currentSubject[0].toUpperCase() + currentSubject.substring(1);
   for (const algoOption of algoNameSelect.options) {
      if (algoOption.classList.contains(requiredClassName)) {
         const scriptTag = document.createElement('script');
         scriptTag.src = 'algorithms/' + currentSubject + '/' + algoOption.value + '.js';
         document.head.appendChild(scriptTag);
      }
   }
   if (!algoNameSelect.selectedOptions[0].classList.contains(requiredClassName)) {
      algoNameSelect.value = '';
   }
   algoNameSelect.onchange = function() {
      removeAllAlgorithmicInfo();
      const algoCodeName = algoNameSelect.value, link = document.getElementById('showAlgoSourceFile');
      if (algoCodeName != '' && algoCodeName != '_') {
         link.href = 'algorithms/' + currentSubject + '/' + algoCodeName + '.js';
         link.classList.remove('hide');

         function showNeededFields() {
            let neededFields = algoNameSelect.selectedOptions[0].getAttribute('data-input');
            neededFields = neededFields == null ? [] : neededFields.split(' ');
            for (const fieldContainer of document.querySelectorAll('.algoInput')) {
               const fieldName = fieldContainer.querySelector('input').id.split('_')[1];
               fieldContainer.classList[neededFields.indexOf(fieldName) >= 0 ? 'remove' : 'add']('hide');
            }
         }
         showNeededFields();

         function showAboutAlgoBlocks() {
            for (const block of document.querySelectorAll('#algoInterface .aboutTheAlgorithm .show')) {
               block.classList.remove('show');
            }
            const aboutAlgoBlocks =
               document.querySelectorAll('#algoInterface .aboutTheAlgorithm [data-algos~="' + algoCodeName + '"]');
            for (const block of aboutAlgoBlocks) {
               block.classList.add('show');
            }
            document.querySelector('#algoInterface .aboutTheAlgorithm')
               .classList[aboutAlgoBlocks.length > 0 ? 'add' : 'remove']('show');
         }
         showAboutAlgoBlocks();
      } else {
         for (const fieldContainer of document.querySelectorAll('.algoInput')) {
            fieldContainer.classList[algoCodeName == '_' ? 'remove' : 'add']('hide');
         }
         link.classList.add('hide');
         document.querySelector('#algoInterface .aboutTheAlgorithm').classList.remove('show');
      }
   };
   algoNameSelect.onchange();

   document.getElementById('tab_algo').onclick = function() {
      const tab = this;
      setTimeout(function() {
         if (tab.classList.contains('selected')) {
            const selectModeButton = document.getElementById('mode_select');
            if (!selectModeButton.classList.contains('selected')) {
               selectModeButton.dispatchEvent(new Event('click'));
               selectModeButton.dispatchEvent(new Event('click'));
            }
         }
      });
   };

   document.getElementById('runAlgorithm').onsubmit = function(evt) {
      evt.preventDefault();
      const fctName = algoNameSelect.value, fct = window[fctName];
      if (fct) {
         runAlgorithm(fct);
      } else if (fctName == '_') {
         document.getElementById('tab_externAlgo').dispatchEvent(new Event('click'));
      } else {
         removeAllAlgorithmicInfo();
      }
      updateSelectionControls();
   };

   const algoStatesButtons = document.getElementById('algoStatesButtons').children;
   function checkAlgoStatesButtons() {
      if (algoCurStNum <= 0) {
         algoStatesButtons[1].setAttribute('disabled', '');
      } else {
         algoStatesButtons[1].removeAttribute('disabled');
      }
      if (algoCurStNum >= algoStates.length - 1) {
         algoStatesButtons[3].setAttribute('disabled', '');
      } else {
         algoStatesButtons[3].removeAttribute('disabled');
      }
      algoStatesButtons[2].textContent = (algoCurStNum + 1) + " / " + algoStates.length;
   }
   function changeAlgoState(stateNum) {
      algoCurStNum = stateNum;
      checkAlgoStatesButtons();
      showAlgoState();
   }
   algoStatesButtons[0].onclick = function() {
      changeAlgoState(0);
   };
   algoStatesButtons[1].onclick = function() {
      changeAlgoState(algoCurStNum - 1);
   };
   algoStatesButtons[2].onclick = function() {
      const stateNum = parseInt(prompt(getText('typeStateNum', true), algoCurStNum + 1));
      if (!Number.isNaN(stateNum)) {
         changeAlgoState(Math.max(1, Math.min(stateNum, algoStates.length)) - 1);
      }
   };
   algoStatesButtons[3].onclick = function() {
      changeAlgoState(algoCurStNum + 1);
   };
   algoStatesButtons[4].onclick = function() {
      changeAlgoState(algoStates.length - 1);
   };

   let externAlgoMsgTimeout = null;
   document.getElementById('externAlgoInput').onclick = function() {
      document.querySelector('#externAlgo textarea').value = getGraphAsText();
   };
   document.getElementById('externAlgoInputWithPos').onclick = function() {
      document.querySelector('#externAlgo textarea').value = getGraphAsText(1);
   };
   document.getElementById('externAlgoOutput').onclick = function() {
      removeAllAlgorithmicInfo();
      try {
         processAlgorithmOutput(document.querySelector('#externAlgo textarea').value);
         initAlgorithmResult();
      } catch {
         externAlgoMsgTimeout = showTemporaryMsg(document.getElementById('externAlgoMsg'),
            getText('invalidJsonInTextarea'), 5000, externAlgoMsgTimeout);
      }
   };
   document.getElementById('externAlgoSaveToFile').onclick = function() {
      const link = this.parentElement.querySelector('a[download]'), text = document.querySelector('#externAlgo textarea').value;
      link.setAttribute('download', getSvgTitle() + ".txt");
      link.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(text);
      link.click();
   };
   document.getElementById('externAlgoLoadFromFile').onclick = function() {
      document.getElementById('externAlgoFileInput').showPicker();
   };
   document.getElementById('externAlgoFileInput').onchange = function() {
      const file = this.files[0];
      file.text().then(data => loadAlgorithmText(data, file.name));
   };
   document.getElementById('externAlgoExec').onclick = function() {
      const msgContainer = document.getElementById('externAlgoMsg');
      try {
         eval(document.querySelector('#externAlgo textarea').value);
         if (algoStates == null || algoOperations == null) {
            externAlgoMsgTimeout = showTemporaryMsg(msgContainer, getText('algorithmGaveNoData'), 30000,
               externAlgoMsgTimeout, true);
         } else {
            document.getElementById('tab_algoInterface').dispatchEvent(new Event('click'));
            algoNameSelect.value = '_';
            msgContainer.innerHTML = '';
         }
      } catch (exc) {
         externAlgoMsgTimeout = showTemporaryMsg(msgContainer, exc + " — " + getText('errorOccurredSeeConsole'), 30000,
            externAlgoMsgTimeout, true);
         throw exc;
      }
   };
});


function processAlgorithmOutput(data) {
   const algoResult = JSON.parse(document.querySelector('#externAlgo textarea').value);
   algoStates = algoResult.states;
   algoOperations = algoResult.operations;
   document.getElementById('tab_algoInterface').dispatchEvent(new Event('click'));
   document.getElementById('algorithmName').value = '_';
}

function loadAlgorithmText(data, fileName) {
   const algoTab = document.getElementById('tab_algo');
   if (!algoTab.classList.contains('selected')) {
      algoTab.dispatchEvent(new Event('click'));
   }
   document.querySelector('#externAlgo textarea').value = data;
   try {
      if ((fileName && fileName.endsWith(".js")) || data.startsWith('function ')) {
         eval(data);
         if (algoStates == null || algoOperations == null) {
            throw null;
         } else {
            document.getElementById('tab_algoInterface').dispatchEvent(new Event('click'));
            document.getElementById('algorithmName').value = '_';
            document.getElementById('externAlgoMsg').innerHTML = '';
         }
      } else {
         processAlgorithmOutput(data);
         initAlgorithmResult();
      }
   } catch {
      document.getElementById('tab_externAlgo').dispatchEvent(new Event('click'));
   }
}


function removeAlgorithmResult() {
   if (algoStates != null) {
      svgGraph.classList.remove('algorithm_hideWeights');
      svgGraph.getElementById('algorithmResult').innerHTML = '';
      for (const svgGroup of svgGraph.querySelectorAll('g, #initialFinal > line')) {
         for (let classNameI = 0; classNameI < svgGroup.classList.length;) {
            const className = svgGroup.classList[classNameI];
            if (className.startsWith('colorization_')) {
               svgGroup.classList.remove(className);
            } else {
               ++classNameI;
            }
         }
      }
   }
}

function removeAllAlgorithmicInfo() {
   if (algoStates != null) {
      removeAlgorithmResult();
      algoStates = algoOperations = null;
      document.getElementById('resultTables').innerHTML = '';
      document.getElementById('complexity').closest('table').classList.add('hide');
      document.getElementById('algoStatesButtons').classList.add('hide');
   }
}


function getElemsFromDesc(elemsData) {
   const elemsList = [];
   try {
      for (const data of elemsData) {
         try {
            if (typeof data == 'number') {
               elemsList.push(getVertex(data));
            } else if ('num' in data) {
               elemsList.push(svgGraph.edges.children[data.num]);
            } else if ('state' in data) {
               const stateId = svgGraph.vertices.querySelector('.vertex[data-num="' + data.state + '"]').id;
               elemsList.push(svgGraph.querySelector('#initialFinal .' + data.arrow + 'Arrow[data-vertex="' + stateId + '"]'));
            } else {
               elemsList.push(getEdge(data.src, data.dest, graphIsUndirected));
            }
         } catch {
            elemsList.push(null);
         }
      }
   } catch {
      return null;
   }
   return elemsList;
}


let algoStates = null, algoOperations = null, algoCurStNum = 0;

function showAlgoState() {
   if (algoStates == null) {
      return;
   }
   const state = algoStates[algoCurStNum];
   removeAlgorithmResult();
   let html = '';
   if (state) {
      function formatData(data) {
         return typeof data == 'object' ? getText(data.textCode) : typeof data == 'number' ? floatToStr(data) : data;
      }
      let svgCode = '';
      let dataNum = 0;
      for (const data of state) {
         ++dataNum;
         try {
            switch (data.type) {
               case 'text':
                  if (data.asHtml) {
                     html += data.value;
                  } else {
                     const par = document.createElement('p');
                     par.textContent = data.value;
                     html += par.outerHTML;
                  }
                  break;
               case 'table': {
                  let tableHtml = '<table' + (data.style ? ' style="' + data.style + '"' : '') + '><tbody>';
                  const nbHeadRows = data.nbHeadRows == undefined ? 0 : data.nbHeadRows;
                  let rowNum = 0;
                  for (const tableRowData of data.value) {
                     tableHtml += '<tr>';
                     let cellType = 'th';
                     for (const tableCellData of tableRowData) {
                        const cell = document.createElement(cellType);
                        cell[data.asHtml ? 'innerHTML' : 'textContent'] = formatData(tableCellData);
                        tableHtml += cell.outerHTML;
                        cellType = rowNum < nbHeadRows ? 'th' : 'td';
                     }
                     tableHtml += '</tr>';
                     ++rowNum;
                  }
                  tableHtml += '</tbody></table>';
                  html += tableHtml;
               }  break;
               case 'colorization': {
                  const colorName = addColorization(data.value);
                  if (colorName == null) {
                     throw null;
                  }
                  let nbErrors = 0;
                  for (const elem of data.target) {
                     try {
                        elem.classList.add('colorization_' + colorName);
                     } catch {
                        ++nbErrors;
                     }
                  }
                  if (nbErrors > 0) {
                     html += '<p class="error">' + getText('algorithmInvalidColorizationData', false, [dataNum]) +
                        ' <code>' + JSON.stringify(data) + '</code></p>';
                  }
               }  break;
               case 'vertexValues': {
                  const position = data.position || [0, -1];
                  let classAttr = '';
                  if (data.color) {
                     addColorization(data.color);
                     classAttr = ' class="colorization_' + data.color + '"';
                  }
                  for (let vertexNum = 0; vertexNum < data.value.length; ++vertexNum) {
                     if (data.value[vertexNum] !== undefined) {
                        let curValue = data.value[vertexNum];
                        curValue = curValue == null ? "" : formatData(curValue);
                        const vertex = getVertex(vertexNum),
                           bounds = vertex.getBoundingClientRect(), circle = vertex.querySelector('circle'),
                           centerX = parseFloat(circle.getAttribute('cx')), centerY = parseFloat(circle.getAttribute('cy'));
                        svgCode += '<text' + classAttr + ' ' +
                              'x="' + (centerX + position[0] * bounds.width * .9) + '" ' +
                              'y="' + (centerY + position[1] * bounds.height * .9) + '">' + curValue +
                           '</text>';
                     }
                  }
               }  break;
               case 'edgeValues': {
                  svgGraph.classList.add('algorithm_hideWeights');
                  let classValues = [];
                  if (data.color) {
                     addColorization(data.color);
                     classValues.push('colorization_' + data.color);
                  }
                  const allEdges = getAllEdges();
                  for (let edgeNum = 0; edgeNum < data.value.length; ++edgeNum) {
                     if (data.value[edgeNum] != null) {
                        const curValue = formatData(data.value[edgeNum]),
                           weightText = allEdges[edgeNum].querySelector('.weight');
                        let classValuesThere = classValues.slice();
                        for (const classToTake of ['leftAligned', 'rightAligned']) {
                           if (weightText.classList.contains(classToTake)) {
                              classValuesThere.push(classToTake);
                           }
                        }
                        const classAttr = classValuesThere.length > 0 ? ' class="' + classValuesThere.join(' ') + '"' : '';
                        svgCode += '<text' + classAttr + ' ' +
                              'x="' + weightText.getAttribute('x') + '" y="' + weightText.getAttribute('y') + '" ' +
                              'transform="' + weightText.getAttribute('transform') + '">' + curValue +
                           '</text>';
                     }
                  }
               }  break;
               default:
                  throw null;
            }
         } catch {
            html += '<p class="error">' + getText('algorithmInvalidData', false, [dataNum]) +
               ' <code>' + JSON.stringify(data) + '</code></p>';
         }
      }
      svgGraph.getElementById('algorithmResult').innerHTML = svgCode;
   }
   document.getElementById('resultTables').innerHTML = html;
}

function showOperations() {
   const nbEdges = getAllEdges().length, nbVertices = getAllVertices().length;
   let tableRows = '';
   for (let op in algoOperations) {
      const nbOp = algoOperations[op];
      let details = [];
      if (nbOp == nbVertices) details.push("n");
      else if (nbOp == nbVertices - 1) details.push("n - 1");
      else if (nbOp == Math.ceil(Math.log2(nbVertices))) details.push("log n");
      else if (nbOp == nbVertices * nbVertices) details.push("n²");
      else if (nbOp == Math.ceil(Math.log2(nbVertices) * nbVertices)) details.push("n log n");
      else if (nbOp == nbVertices * nbVertices * nbVertices) details.push("n³");
      else if (nbOp == nbVertices * (nbVertices - 1) / 2) details.push("n(n - 1) / 2");
      else if (nbOp % nbVertices == 0 && nbOp % (nbVertices * nbEdges) != 0) details.push((nbOp / nbVertices) + " n");
      if (nbOp == nbEdges) details.push("m");
      else if (nbOp == Math.ceil(Math.log2(nbEdges))) details.push("log m");
      else if (nbOp == nbEdges * nbEdges) details.push("m²");
      else if (nbOp == Math.ceil(nbEdges * Math.log2(nbEdges))) details.push("m log m");
      else if (nbOp == nbEdges * nbEdges * nbEdges) details.push("m³");
      else if (nbOp % nbEdges == 0 && nbOp % (nbVertices * nbEdges) != 0) details.push((nbOp / nbEdges) + " m");
      if (nbVertices < nbOp && nbOp < nbEdges) details.push("]n ; m[");
      else if (nbEdges < nbOp && nbOp < nbVertices - 1) details.push("]m ; n[");
      else if (nbOp == nbVertices * nbEdges) details.push("n m");
      else if (nbOp % (nbVertices * nbEdges) == 0) details.push((nbOp / (nbVertices * nbEdges)) + " n m");
      else if (nbOp == nbVertices * nbVertices * nbEdges) details.push("n² m");
      if (details.length == 0) {
         if (nbOp < Math.ceil(Math.log2(nbVertices))) details.push("< log n");
         else if (nbOp < nbVertices) details.push("< n");
         else if (nbOp < Math.ceil(Math.log2(nbVertices) * nbVertices)) details.push("< n log n");
         if (nbOp < Math.ceil(Math.log2(nbEdges))) details.push("< log m");
         else if (nbOp < nbEdges) details.push("< m");
         else if (nbOp < Math.ceil(nbEdges * Math.log2(nbEdges))) details.push("< m log m");
      }
      if (details.length == 0) {
         for ([nb, varName] of [[nbVertices, 'n'], [nbEdges, 'm']]) {
            if (nb > 1) {
               let varPow = 2;
               for (let val = nb * nb; val < nbOp; val *= nb) ++varPow;
               details.push("⩽ " + varName + '<sup>' + varPow + '</sup>');
            }
         }
      }
      tableRows += '<tr><td>' + op + '</td><td>' + nbOp + '</td><td>' + details.join(" ; ") + '</td></tr>';
   }
   const complexityTBody = document.getElementById('complexity');
   if (tableRows == '') {
      complexityTBody.closest('table').classList.add('hide');
   } else {
      complexityTBody.closest('table').classList.remove('hide');
   }
   complexityTBody.closest('table').querySelector('.cardinals').textContent = "(n = " + nbVertices + " ; m = " + nbEdges + ")";
   complexityTBody.innerHTML = tableRows;
}

function initAlgorithmResult() {
   if (algoStates == undefined || algoStates.length <= 0) {
      removeAllAlgorithmicInfo();
   } else {
      deselectAll();
      for (const state of algoStates) {
         for (const data of state) {
            if (data.type == 'colorization') {
               data.target = getElemsFromDesc(data.target);
            }
         }
      }
      const maxNbFigures = ("" + algoStates.length).length;
      const algoStatesButtons = document.getElementById('algoStatesButtons');
      algoStatesButtons.children[2].style.minWidth = (maxNbFigures * 2 + 3) + 'ex';
      algoStatesButtons.children[0].click();
      algoStatesButtons.classList[algoStates.length <= 1 ? 'add' : 'remove']('hide');
      showOperations();
   }
}
