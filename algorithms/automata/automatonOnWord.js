function automatonOnWord(automaton) {
   let dataToShow = [];
   function addDataToShow(current) {
      const tableContent = [];
      for (const cur of cursors) {
         const wordHtml = '<span style="color: blue;"><u>' + automaton.word.substring(0, cur.pos) + '</u></span>' +
               automaton.word.substring(cur.pos);
         tableContent.push([getStateLabel(cur.state), wordHtml]);
      }
      for (const state of acceptingStates) {
         if (typeof state == 'number') {
            const wordHtml = '<span style="color: green;"><u>' + automaton.word + '</u></span>';
            tableContent.push([getStateLabel(state), wordHtml]);
         }
      }
      for (const cur of failedCursors) {
         const wordHtml = '<u>' + automaton.word.substring(0, cur.pos) + '</u>' +
            '<span style="color: red">' + automaton.word.substring(cur.pos) + '</span>';
         tableContent.push([getStateLabel(cur.state), wordHtml]);
      }
      dataToShow.push([
            { type: 'table', asHtml: true, value: tableContent },
            { type: 'colorization', target: wrongStates.slice(), value: 'red' },
            { type: 'colorization', target: acceptingStates.slice(), value: 'green' },
            { type: 'colorization', target: current, value: 'darkcyan' },
         ]);
   }
   if (automaton.initial.length == 0) {
      return [[{ type: 'text', value: "L’automate ne contient aucun sommet initial." }]];
   }

   let cursors = automaton.initial.map(stateNum => ({ state: stateNum, pos: 0 })), acceptingStates = [];
   const wrongStates = [], failedCursors = [];
   addDataToShow(automaton.initial.concat(automaton.initial.map(stateNum => ({ state: stateNum, arrow: 'initial' }))));
   while (cursors.length > 0) {
      const nextCursors = [], usedTransi = [];
      for (const cur of cursors) {
         let transiFound = false;
         if (cur.pos >= automaton.word.length && automaton.final.indexOf(cur.state) >= 0) {
            transiFound = true;
            acceptingStates.push(cur.state, { state: cur.state, arrow: 'final' });
         }
         for (const transi of automaton.states[cur.state]) {
            if (transi.letter == '' && (cur.state == transi.state ||
                  (cur.emptySeq != undefined && cur.emptySeq.indexOf(transi.state) >= 0))) {
               usedTransi.push(transi);
            } else if (automaton.word.substr(cur.pos, transi.letter.length) == transi.letter) {
               transiFound = true;
               usedTransi.push(transi);
               const nextCur = { state: transi.state, pos: cur.pos + transi.letter.length };
               if (transi.letter == '') {
                  nextCur.emptySeq = (cur.emptySeq != undefined ? cur.emptySeq : []).concat([cur.state]);
               }
               nextCursors.push(nextCur);
            }
         }
         if (!transiFound) {
            wrongStates.push(cur.state);
            failedCursors.push(cur);
         }
      }
      cursors = nextCursors;
      addDataToShow(usedTransi.concat(cursors.map(cur => cur.state)));
   }

   return dataToShow;
}
