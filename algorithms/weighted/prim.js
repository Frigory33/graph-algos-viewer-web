function prim(graph) {
   const dataToShow = [];
   function addDataToShow(curVert, curEdge) {
      const queueTable = formatBinaryTree("File à priorité",
            queue.data.map(vertex =>
               getVertexLabel(vertex) + " <strong>" + graph.edges[edgesForVertices[vertex]].weight + "</strong>")),
         joinedElems = getVerticesFromBools(verticesAreJoined).concat(parentsToEdges(parents)),
         currentElems = [];
      if (curVert != undefined) {
         currentElems.push(curVert);
      }
      if (curEdge != undefined) {
         currentElems.push(curEdge);
      }
      dataToShow.push([
            { type: 'text', value: "Sommet de départ : " + getVertexLabel(graph.selectedVertices[0]) },
            { type: 'table', value: formatArraysForVertices(["Père"], [parents.map(getVertexLabel)]) },
            { type: 'table', asHtml: true, value: queueTable },
            { type: 'text', value: "Poids de l’arbre : " + floatToStr(treeWeight) },
            { type: 'colorization', target: joinedElems, value: 'green' },
            { type: 'colorization', target: getEdgesFromBools(edgesAreSkipped), value: 'gray' },
            { type: 'colorization', target: getEdgesFromBools(edgesInQueue), value: 'darkorange' },
            { type: 'colorization', target: currentElems, value: 'red' },
         ]);
   }

   if (graph.selectedVertices.length == 0) {
      return [[{ type: 'text', value: "Veuillez sélectionner un sommet pour que l’algorithme puisse fonctionner." }]];
   }

   const edgesAddedInQueue = new Array(graph.nbEdges).fill(false), edgesForVertices = new Array(graph.nbVertices).fill(-1),
      verticesAreJoined = new Array(graph.nbVertices).fill(false), parents = new Array(graph.nbVertices).fill(null);
   const edgesInQueue = new Array(graph.nbEdges).fill(false), edgesAreSkipped = new Array(graph.nbEdges).fill(false);
   const queue = PriorityQueue((v1, v2) => graph.edges[edgesForVertices[v1]].weight < graph.edges[edgesForVertices[v2]].weight,
         true, ["Rangement pour enfilement", "Rangement pour défilement"]);
   let treeWeight = 0;

   function addVertex(vNum, parentVNum) {
      countOperation("Connexion de sommet");
      verticesAreJoined[vNum] = true;
      parents[vNum] = parentVNum;
      for (const edge of graph.succLists[vNum]) {
         countOperation("Consultation d’arc incident");
         if (!verticesAreJoined[edge.vertex] && !edgesAddedInQueue[edge.num]) {
            if (edgesForVertices[edge.vertex] < 0 || edge.weight < graph.edges[edgesForVertices[edge.vertex]].weight) {
               countOperation("Enfilement ou mise à jour");
               const oldEdgeNum = edgesForVertices[edge.vertex];
               edgesForVertices[edge.vertex] = edge.num;
               if (oldEdgeNum >= 0) {
                  queue.upgrade(queue.dataIndexes[edge.vertex], edge.vertex);
                  edgesInQueue[oldEdgeNum] = false;
                  edgesAreSkipped[oldEdgeNum] = true;
               } else {
                  queue.push(edge.vertex);
               }
               edgesAddedInQueue[edge.num] = edgesInQueue[edge.num] = true;
            } else {
               edgesAreSkipped[edge.num] = true;
            }
         }
      }
      addDataToShow(vNum);
   }

   addDataToShow(graph.selectedVertices[0]);
   addVertex(graph.selectedVertices[0], null);
   while (queue.length() > 0) {
      countOperation("Défilement, visite de sommet");
      const vertex = queue.pop(), edge = graph.edges[edgesForVertices[vertex]];
      edgesInQueue[edge.num] = false;
      addDataToShow(undefined, edge);
      treeWeight += edge.weight;
      addVertex(vertex, edge.src == vertex ? edge.dest : edge.src);
   }
   addDataToShow();

   return dataToShow;
}
