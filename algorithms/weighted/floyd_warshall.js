function floyd_warshall(graph) {
   const dataToShow = [];
   const vertexLabelsList = new Array(graph.nbVertices).fill(null).map((value, index) => getVertexLabel(index));
   function addDataToShow(vNums, absorbentEdges) {
      const distancesTable = [["Distances"].concat(vertexLabelsList)], parentsTable = [["Pères"].concat(vertexLabelsList)];
      for (let vFromNum = 0; vFromNum < graph.nbVertices; ++vFromNum) {
         const distancesRow = [vertexLabelsList[vFromNum]], parentsRow = [vertexLabelsList[vFromNum]];
         for (let vToNum = 0; vToNum < graph.nbVertices; ++vToNum) {
            const dist = distances[vFromNum][vToNum], par = parents[vFromNum][vToNum];
            distancesRow.push(dist == null ? "—" : dist);
            parentsRow.push(par == null ? "—" : vertexLabelsList[par]);
         }
         distancesTable.push(distancesRow);
         parentsTable.push(parentsRow);
      }
      const tablesCss = 'display: inline-block; margin: 0 .5em .5em 0;';
      const data = [
            { type: 'table', style: tablesCss, nbHeadRows: 1, value: distancesTable },
            { type: 'table', style: tablesCss, nbHeadRows: 1, value: parentsTable },
         ];
      if (vNums != undefined) {
         const distancesOnGraph = new Array(graph.nbVertices).fill(''), parentsOnGraph = new Array(graph.nbVertices).fill(null);
         distancesOnGraph[vNums[1]] = "•";
         distancesOnGraph[vNums[0]] = distances[vNums[1]][vNums[0]] + " + " +
            distances[vNums[0]][vNums[2]];
         if (distances[vNums[1]][vNums[2]] != null) {
            distancesOnGraph[vNums[2]] = distances[vNums[1]][vNums[2]];
            let curVertex = vNums[2];
            if (distances[vNums[1]][vNums[2]] == distances[vNums[1]][vNums[0]] + distances[vNums[0]][vNums[2]]) {
               while (curVertex != vNums[0] && parentsOnGraph[curVertex] == null) {
                  const parent = parents[vNums[0]][curVertex];
                  parentsOnGraph[curVertex] = parent;
                  curVertex = parent;
               }
            }
            while (curVertex != vNums[1] && parentsOnGraph[curVertex] == null) {
               const parent = parents[vNums[1]][curVertex];
               parentsOnGraph[curVertex] = parent;
               curVertex = parent;
            }
         }
         const vLabels = vNums.map(vNum => vertexLabelsList[vNum]);
         data.push(
            { type: 'colorization', target: parentsToEdges(parentsOnGraph), value: 'green' },
            { type: 'vertexValues', color: 'red', value: distancesOnGraph },
            { type: 'colorization', target: vNums, value: 'red' },
            { type: 'text', value: "Itération : par " + vLabels[0] + ", de " + vLabels[1] + " à " + vLabels[2] });
      }
      if (absorbentEdges != undefined && absorbentEdges.length > 0) {
         data.push(
            { type: 'colorization', target: absorbentEdges, value: 'darkorange' },
            { type: 'text', value: absorbentEdges.length + " arcs sont compris dans au moins une chaine absorbante." });
      }
      dataToShow.push(data);
   }

   const distances = new Array(graph.nbVertices), parents = new Array(graph.nbVertices);
   for (let vertexNum = 0; vertexNum < graph.nbVertices; ++vertexNum) {
      countOperation("Initialisation d’une case de la matrice", graph.nbVertices);
      distances[vertexNum] = new Array(graph.nbVertices).fill(null);
      parents[vertexNum] = new Array(graph.nbVertices).fill(null);
      distances[vertexNum][vertexNum] = 0;
      parents[vertexNum][vertexNum] = vertexNum;
   }
   addDataToShow();
   for (const edge of graph.edges) {
      distances[edge.src][edge.dest] = edge.weight;
      parents[edge.src][edge.dest] = edge.src;
      if (graph.isUndirected) {
         distances[edge.dest][edge.src] = edge.weight;
         parents[edge.dest][edge.src] = edge.dest;
      }
   }
   addDataToShow();

   for (let vInterNum = 0; vInterNum < graph.nbVertices; ++vInterNum) {
      for (let vFromNum = 0; vFromNum < graph.nbVertices; ++vFromNum) {
         for (let vToNum = 0; vToNum < graph.nbVertices; ++vToNum) {
            countOperation("Test de raccourci");
            const distInter1 = distances[vFromNum][vInterNum], distInter2 = distances[vInterNum][vToNum],
               prevDist = distances[vFromNum][vToNum];
            if (distInter1 != null && distInter2 != null && (prevDist == null || distInter1 + distInter2 < prevDist)) {
               countOperation("Affectation de raccourci");
               addDataToShow([vInterNum, vFromNum, vToNum]);
               distances[vFromNum][vToNum] = distInter1 + distInter2;
               parents[vFromNum][vToNum] = parents[vInterNum][vToNum];
               addDataToShow([vInterNum, vFromNum, vToNum]);
            }
         }
      }
   }
   let absorbentEdges = [];
   for (const edge of graph.edges) {
      if ((distances[edge.dest][edge.src] != null && distances[edge.dest][edge.src] + edge.weight < 0) ||
            (graph.isUndirected && distances[edge.src][edge.dest] != null && distances[edge.src][edge.dest] + edge.weight < 0)) {
         absorbentEdges.push(edge);
      }
   }
   addDataToShow(undefined, absorbentEdges);

   return dataToShow;
}
