function bellman(graph) {
   const dataToShow = [];
   function addDataToShow(curVerts, curEdge) {
      const verticesTable = formatArraysForVertices(
            ["Nombre de prédécesseurs", "Distance", "Père", "Rang"], [nbPred, distances, parents.map(getVertexLabel), ranks]),
         currentElems = curVerts == undefined ? [] : curVerts;
      if (curEdge != undefined) {
         currentElems.push(curEdge);
      }
      const data = [
            { type: 'table', value: verticesTable },
            { type: 'table', value: [["File"].concat(queue.map(getVertexLabel))] },
            { type: 'vertexValues', color: 'red', value: distances.map(dist => dist == null ? '' : dist) },
            { type: 'vertexValues', position: [1, 0], color: 'brown', value: ranks.map(rank => rank == null ? '' : rank) },
            { type: 'vertexValues', position: [0, 1], color: 'green', value: nbPred.slice() },
            { type: 'colorization', target: parentsToEdges(parents), value: 'green' },
            { type: 'colorization', target: currentElems, value: 'red' },
         ];
      if (currentElems.length <= 0 && nbPred.some(nb => nb > 0)) {
         data.push({ type: 'text', value: "L’algorithme est bloqué par l’existence d’un ou plusieurs cycles." });
      }
      dataToShow.push(data);
   }

   const nbPred = new Array(graph.nbVertices), distances = new Array(graph.nbVertices).fill(null),
      parents = new Array(graph.nbVertices).fill(null), ranks = new Array(graph.nbVertices).fill(null);
   const queue = [];
   for (let vNum = 0; vNum < graph.nbVertices; ++vNum) {
      countOperation("Initialisation de sommet");
      nbPred[vNum] = graph.predLists[vNum].length;
      if (nbPred[vNum] <= 0) {
         countOperation("Enfilement");
         distances[vNum] = 0;
         ranks[vNum] = 0;
         queue.push(vNum);
      }
      addDataToShow([vNum]);
   }
   while (queue.length > 0) {
      countOperation("Défilement, visite de sommet");
      const vertex = queue.shift();
      addDataToShow([vertex]);
      for (const succ of graph.succLists[vertex]) {
         countOperation("Parcours d’arc");
         addDataToShow([vertex], succ);
         if (distances[succ.vertex] == null || distances[succ.vertex] > distances[vertex] + succ.weight) {
            countOperation("Mise à jour de distance");
            distances[succ.vertex] = distances[vertex] + succ.weight;
            parents[succ.vertex] = vertex;
            addDataToShow([vertex]);
         }
         --nbPred[succ.vertex];
         if (nbPred[succ.vertex] == 0) {
            countOperation("Enfilement");
            ranks[succ.vertex] = ranks[vertex] + 1;
            queue.push(succ.vertex);
         }
         addDataToShow([vertex, succ.vertex]);
      }
   }
   addDataToShow();

   return dataToShow;
}
