#include "api.h"


static Graph const * graph;
static DynArr(Any) dataToShow = {};

static char const * firstVerticesTxt;

static bool * visited;
static double * distances;
static int * parents;
static PriorityQueue queue;


void addDataToShow(int curVert, int edgeNum)
{
   Slice(Any) queueContents = slice_new(Any, queue.data.len);
   dynarr_grow(&queueContents);
   for_range (int, elemNum, 0, < queue.data.len, ++) {
      int vNum = queue.data.data[elemNum].i;
      queueContents.data[elemNum] = Any(formatStr("%s <strong>%s</strong>", getVertexLabel(vNum), floatToStr(distances[vNum])));
   }
   Any queueTable = formatBinaryTree("File à priorité", queueContents);
   free(queueContents.data);
   DynArr(Any) currentElems = {};
   if (curVert >= 0) {
      dynarr_push(&currentElems, Any(curVert));
   }
   if (edgeNum >= 0) {
      dynarr_push(&currentElems, Any(dict_build(Any, { "num", Any(edgeNum) })));
   }
   Slice(Any) distancesData = slice_new(Any, graph->nbVertices), parentsData = slice_new(Any, graph->nbVertices);
   for (int vNum = 0; vNum < graph->nbVertices; ++vNum) {
      distancesData.data[vNum] = distances[vNum] < 0. ? Any("—") : Any(distances[vNum]);
      parentsData.data[vNum] = Any(getVertexLabel(parents[vNum]));
   }
   Slice(Any) verticesTableRows = slice_on_values(Any, Any(distancesData), Any(parentsData));
   Any verticesTable = formatArraysForVertices((char const *[]){ "Distance", "Père" }, verticesTableRows);
   free(parentsData.data);
   for (int vNum = 0; vNum < graph->nbVertices; ++vNum) {
      if (distances[vNum] < 0.) {
         distancesData.data[vNum] = Any("");
      }
   }
   dynarr_push(&dataToShow, Any(slice_build(Any,
         Any(dict_build(Any, { "type", Any("text") }, { "value", Any(firstVerticesTxt) })),
         Any(dict_build(Any, { "type", Any("table") }, { "value", verticesTable })),
         Any(dict_build(Any, { "type", Any("table") }, { "asHtml", Any(true) }, { "value", queueTable })),
         Any(dict_build(Any, { "type", Any("colorization") }, { "target", getVerticesFromBools(visited) },
            { "value", Any("green") })),
         Any(dict_build(Any, { "type", Any("colorization") }, { "target", parentsToEdges(parents) }, { "value", Any("green") })),
         Any(dict_build(Any, { "type", Any("vertexValues") }, { "color", Any("red") }, { "value", Any(distancesData) })),
         Any(dict_build(Any, { "type", Any("colorization") }, { "target", Any(currentElems) }, { "value", Any("red") })),
      )));
}

bool firstIsClosest(Any vNum1, Any vNum2)
{
   return distances[vNum1.i] < distances[vNum2.i];
}

Slice(Any) dijkstra(Graph const * graphArg)
{
   graph = graphArg;

   if (graph->selectedVertices.len <= 1) {
      firstVerticesTxt = graph->selectedVertices.len <= 0 ? "Attention : aucun sommet n’a été sélectionné." :
            formatStr("Sommet de départ : %s", getVertexLabel(graph->selectedVertices.data[0]));
   } else {
      char const * textParts[graph->selectedVertices.len * 2 + 1];
      for (int selNum = 0; selNum < graph->selectedVertices.len; ++selNum) {
         textParts[selNum * 2] = ", ";
         textParts[selNum * 2 + 1] = getVertexLabel(graph->selectedVertices.data[selNum]);
      }
      textParts[0] = "Sommets de départ : ";
      textParts[graph->selectedVertices.len * 2] = NULL;
      firstVerticesTxt = concatStrings(textParts);
   }

   bool visitedBuf[graph->nbVertices];
   double distancesBuf[graph->nbVertices];
   int parentsBuf[graph->nbVertices];
   visited = visitedBuf;
   distances = distancesBuf;
   parents = parentsBuf;
   for (int vNum = 0; vNum < graph->nbVertices; ++vNum) {
      countOperation("Initialisation");
      visited[vNum] = false;
      distances[vNum] = -1.;
      parents[vNum] = -1;
   }
   queue = PQ_create(&firstIsClosest, true, (char const *[]){ "Rangement pour enfilement", "Rangement pour défilement" });
   for (int selNum = 0; selNum < graph->selectedVertices.len; ++selNum) {
      countOperation("Enfilement ou mise à jour");
      int vNum = graph->selectedVertices.data[selNum];
      distances[vNum] = 0;
      PQ_push(&queue, Any(vNum));
   }
   if (graph->selectedVertices.len <= 0) {
      addDataToShow(-1, -1);
   }
   while (queue.data.len > 0) {
      countOperation("Défilement, visite de sommet");
      int curVert = PQ_pop(&queue).i;
      addDataToShow(curVert, -1);
      for (int succNum = 0; succNum < graph->succLists[curVert].len; ++succNum) {
         EdgeEnd neighb = graph->succLists[curVert].data[succNum];
         double newDist = distances[curVert] + neighb.weight;
         countOperation("Parcours d’arc");
         addDataToShow(curVert, neighb.num);
         if (distances[neighb.vertex] < 0. || (distances[neighb.vertex] > newDist && !visited[neighb.vertex])) {
            countOperation("Enfilement ou mise à jour");
            bool notInQueue = distances[neighb.vertex] < 0.;
            distances[neighb.vertex] = newDist;
            parents[neighb.vertex] = curVert;
            if (notInQueue) {
               PQ_push(&queue, Any(neighb.vertex));
            } else {
               PQ_upgrade(&queue, queue.dataIndexes.data[neighb.vertex], Any(neighb.vertex));
            }
            addDataToShow(curVert, -1);
         }
      }
      visited[curVert] = true;
      addDataToShow(-1, -1);
   }
   PQ_destroy(&queue);

   return slice_on(Any, dataToShow);
}


int main()
{
   runAlgorithm(dijkstra);
}
