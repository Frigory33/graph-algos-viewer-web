function bfs(graph) {
   const dataToShow = [];
   const firstVerticesTxt = graph.selectedVertices.length <= 0 ? "Attention : aucun sommet n’a été sélectionné." :
         (graph.selectedVertices.length == 1 ? "Sommet de départ : " + getVertexLabel(graph.selectedVertices[0]) :
            "Sommets de départ : " + graph.selectedVertices.map(getVertexLabel).join(", "));
   function addDataToShow(curVert, edgeEnd) {
      const queueTable = [["File"].concat(queue.map(vNum => getVertexLabel(vNum) + " <strong>" + distances[vNum] + "</strong>"))],
         currentElems = [];
      if (curVert != undefined) {
         currentElems.push(curVert);
      }
      if (edgeEnd != undefined) {
         currentElems.push(edgeEnd);
      }
      dataToShow.push([
            { type: 'text', value: firstVerticesTxt },
            { type: 'table', value: formatArraysForVertices(["Distance", "Père"], [distances, parents.map(getVertexLabel)]) },
            { type: 'table', asHtml: true, value: queueTable },
            { type: 'colorization', target: getVerticesFromBools(accessed).concat(parentsToEdges(parents)), value: 'green' },
            { type: 'vertexValues', color: 'red', value: distances.map(dist => dist == null ? '' : dist) },
            { type: 'colorization', target: currentElems, value: 'red' },
         ]);
   }

   const accessed = new Array(graph.nbVertices).fill(false), distances = new Array(graph.nbVertices).fill(null),
      parents = new Array(graph.nbVertices).fill(null);
   const queue = [];
   for (const vNum of graph.selectedVertices) {
      countOperation("Enfilement");
      accessed[vNum] = true;
      distances[vNum] = 0;
      queue.push(vNum);
   }
   while (queue.length > 0) {
      countOperation("Défilement, visite de sommet");
      const curVert = queue.shift();
      addDataToShow(curVert);
      for (const neighb of graph.succLists[curVert]) {
         countOperation("Parcours d’arc");
         addDataToShow(curVert, neighb);
         if (!accessed[neighb.vertex]) {
            countOperation("Enfilement");
            accessed[neighb.vertex] = true;
            distances[neighb.vertex] = distances[curVert] + 1;
            parents[neighb.vertex] = curVert;
            queue.push(neighb.vertex);
            addDataToShow(curVert);
         }
      }
   }
   addDataToShow();

   return dataToShow;
}
