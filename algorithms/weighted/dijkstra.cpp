#include "api.hpp"


DataToShow dijkstra(Graph const & graph)
{
   DataToShow dataToShow;
   std::string firstVerticesTxt = graph.selectedVertices.empty() ? "Attention : aucun sommet n’a été sélectionné." :
         (graph.selectedVertices.size() < 2 ? "Sommet de départ : " : "Sommets de départ : ")
            + getVertexLabel(graph.selectedVertices[0]);
   for (int selNum = 1; selNum < (int)graph.selectedVertices.size(); ++selNum) {
      firstVerticesTxt.append(", ").append(getVertexLabel(graph.selectedVertices[selNum]));
   }

   std::vector<bool> visited(graph.nbVertices);
   std::vector<double> distances(graph.nbVertices, -1.);
   std::vector<int> parents(graph.nbVertices, -1);
   std::string queueOpNames[] = { "Rangement pour enfilement", "Rangement pour défilement" };
   PriorityQueue queue([& distances](int v1, int v2) { return distances[v1] < distances[v2]; }, queueOpNames);

   auto addDataToShow = [&](int curVert = -1, int edgeNum = -1) {
      AnyVector queueContents;
      std::transform(whole(queue.data), std::back_inserter(queueContents),
         [& distances](int vNum) {
            std::string str;
            str.append(getVertexLabel(vNum)).append(" <strong>").append(floatToStr(distances[vNum])).append("</strong>");
            return str;
         });
      Table queueTable = formatBinaryTree("File à priorité", queueContents);
      AnyVector distancesRow, parentsRow;
      std::replace_copy_if(whole(distances), std::back_inserter(distancesRow), [](double dist) { return dist < 0.; }, "—");
      std::transform(whole(parents), std::back_inserter(parentsRow), &getVertexLabel);
      AnyVector distancesData;
      std::replace_copy_if(whole(distances), std::back_inserter(distancesData), [](double dist) { return dist < 0.; }, "");
      AnyVector currentElems;
      if (curVert >= 0) {
         currentElems.push_back(curVert);
      }
      if (edgeNum >= 0) {
         currentElems.push_back(Dict{ { "num", edgeNum } });
      }
      dataToShow.push_back({
            { { "type", "text" }, { "value", firstVerticesTxt } },
            { { "type", "table" },
               { "value", formatArraysForVertices({ "Distance", "Père" }, Table{ distancesRow, parentsRow }) } },
            { { "type", "table" }, { "asHtml", true }, { "value", queueTable } },
            { { "type", "colorization" }, { "target", getVerticesFromBools(visited) }, { "value", "green" } },
            { { "type", "colorization" }, { "target", parentsToEdges(parents) }, { "value", "green" } },
            { { "type", "vertexValues" }, { "color", "red" }, { "value", distancesData } },
            { { "type", "colorization" }, { "target", currentElems }, { "value", "red" } },
         });
   };

   for (int vNum: graph.selectedVertices) {
      countOperation("Enfilement ou mise à jour");
      distances[vNum] = 0;
      queue.push(vNum);
   }
   if (graph.selectedVertices.empty()) {
      addDataToShow();
   }
   while (queue.length() > 0) {
      countOperation("Défilement, visite de sommet");
      int curVert = queue.pop();
      addDataToShow(curVert);
      for (auto const & neighb: graph.succLists[curVert]) {
         double newDist = distances[curVert] + neighb.weight;
         countOperation("Parcours d’arc");
         addDataToShow(curVert, neighb.num);
         if (distances[neighb.vertex] < 0. || (distances[neighb.vertex] > newDist && !visited[neighb.vertex])) {
            countOperation("Enfilement ou mise à jour");
            bool notInQueue = distances[neighb.vertex] < 0.;
            distances[neighb.vertex] = newDist;
            parents[neighb.vertex] = curVert;
            if (notInQueue) {
               queue.push(neighb.vertex);
            } else {
               queue.upgrade(queue.dataIndexes[neighb.vertex], neighb.vertex);
            }
            addDataToShow(curVert);
         }
      }
      visited[curVert] = true;
      addDataToShow();
   }

   return dataToShow;
}


int main()
{
   runAlgorithm(dijkstra);
}
