function tarjan(graph) {
   const dataToShow = [];
   function addDataToShow(curVert) {
      const verticesPropsTable = formatArraysForVertices(
            ["Début de visite", "Père", "Date de découverte de la CFC"],
            [visitStart, parents.map(getVertexLabel), backVertexStart]);
      dataToShow.push([
            { type: 'colorization', target: getVerticesFromBools(accessed).concat(parentsToEdges(parents)), value: 'green' },
            { type: 'colorization', target: closingEdges.slice(), value: 'darkorange' },
            { type: 'colorization', target: curVert == undefined ? [] : [curVert], value: 'red' },
            { type: 'table', value: verticesPropsTable },
            { type: 'table', value: [["Sommets en attente"].concat(waitingVerticesList.map(getVertexLabel))] },
            { type: 'table', value: [["CFC"].concat(cfcList.map(cfc => cfc.map(getVertexLabel).join(", ")))] },
         ]);
   }

   const accessed = new Array(graph.nbVertices).fill(false), parents = new Array(graph.nbVertices).fill(null),
      visitStart = new Array(graph.nbVertices).fill(null), backVertexStart = new Array(graph.nbVertices).fill(null);
   let time = 0;
   const waitingVerticesList = [], cfcList = [], waitingVertices = new Array(graph.nbVertices).fill(false), closingEdges = [];

   function visit(vNum) {
      countOperation("Visite de sommet");
      accessed[vNum] = true;
      visitStart[vNum] = ++time;
      backVertexStart[vNum] = time;
      const depth = waitingVerticesList.length;
      waitingVerticesList.push(vNum);
      waitingVertices[vNum] = true;
      for (const neighb of graph.succLists[vNum]) {
         countOperation("Parcours d’arc");
         if (!accessed[neighb.vertex]) {
            parents[neighb.vertex] = vNum;
            visit(neighb.vertex);
            countOperation("Mise à jour de la date de la CFC");
            backVertexStart[vNum] = Math.min(backVertexStart[vNum], backVertexStart[neighb.vertex]);
         } else if (waitingVertices[neighb.vertex]) {
            countOperation("Mise à jour de la date de la CFC");
            backVertexStart[vNum] = Math.min(backVertexStart[vNum], visitStart[neighb.vertex]);
            closingEdges.push(neighb);
         }
      }
      if (backVertexStart[vNum] == visitStart[vNum]) {
         countOperation("Découverte de CFC");
         addDataToShow(vNum);
         parents[vNum] = null;
         cfcList.push(waitingVerticesList.slice(depth));
         for (let waitingI = depth; waitingI < waitingVerticesList.length; ++waitingI) {
            countOperation("Ajout de sommet à CFC");
            waitingVertices[waitingVerticesList[waitingI]] = false;
         }
         waitingVerticesList.splice(depth);
         addDataToShow(vNum);
      }
   }

   for (let vNum = 0; vNum < graph.nbVertices; ++vNum) {
      countOperation("Consultation de sommet");
      if (!accessed[vNum]) {
         visit(vNum);
      }
   }
   addDataToShow();

   return dataToShow;
}
