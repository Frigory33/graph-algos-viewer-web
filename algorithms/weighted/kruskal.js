function kruskal(graph) {
   const dataToShow = [];
   function addDataToShow(edge, group1, group2) {
      const data = [
            { type: 'colorization', target: getEdgesFromBools(edgesAreTaken), value: 'green' },
            { type: 'colorization', target: getEdgesFromBools(edgesAreSkipped), value: 'gray' },
            { type: 'text', value: "Poids de l’arbre : " + floatToStr(treeWeight) },
            { type: 'table', value: formatArraysForVertices(["Groupe"], [verticesGroups.map(getVertexLabel)]) },
         ];
      if (edge != undefined) {
         data.push(
            { type: 'text', value: "Groupes : " + getVertexLabel(group1) + " et " + getVertexLabel(group2) },
            { type: 'colorization', target: [edge], value: 'red' });
      } else {
         data.push({ type: 'text', value: " " });
      }
      dataToShow.push(data);
   }

   const edgesAreTaken = new Array(graph.nbEdges).fill(false), edgesAreSkipped = new Array(graph.nbEdges).fill(false),
      sortedEdgesNums = new Array(graph.nbEdges).fill(null).map((elem, index) => index)
         .sort((edgeNum1, edgeNum2) => graph.edges[edgeNum1].weight - graph.edges[edgeNum2].weight),
      verticesGroups = new Array(graph.succLists.length).fill(null).map((elem, index) => index);
   let treeWeight = 0;

   function getVertexTopGroup(vNum) {
      if (verticesGroups[vNum] != verticesGroups[verticesGroups[vNum]]) {
         countOperation("Mise à jour de groupe");
         verticesGroups[vNum] = getVertexTopGroup(verticesGroups[vNum]);
      }
      return verticesGroups[vNum];
   }

   for (const edgeNum of sortedEdgesNums) {
      countOperation("Consultation d’arc");
      const edge = graph.edges[edgeNum], topGroup1 = getVertexTopGroup(edge.src), topGroup2 = getVertexTopGroup(edge.dest);
      addDataToShow(edge, topGroup1, topGroup2);
      if (topGroup1 != topGroup2) {
         edgesAreTaken[edgeNum] = true;
         treeWeight += edge.weight;
         countOperation("Mise à jour de groupe");
         verticesGroups[topGroup2] = topGroup1;
      } else {
         edgesAreSkipped[edgeNum] = true;
      }
      addDataToShow();
   }

   return dataToShow;
}
