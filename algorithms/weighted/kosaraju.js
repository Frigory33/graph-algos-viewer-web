function kosaraju(graph) {
   const accessed = new Array(graph.nbVertices).fill(false), parents = new Array(graph.nbVertices).fill(null),
      treeNums = new Array(graph.nbVertices).fill(null), visitEnd = new Array(graph.nbVertices).fill('');
   let treeEdges = [];
   const sortedByEnd = [];

   function minimalDFS(verticesOrder, succLists) {
      function visit(vNum) {
         countOperation("Visite de sommet");
         accessed[vNum] = true;
         treeNums[vNum] = treeNum;
         for (const neighb of succLists[vNum]) {
            countOperation("Parcours d’arc");
            if (!accessed[neighb.vertex]) {
               parents[neighb.vertex] = vNum;
               treeEdges.push({ src: vNum, dest: neighb.vertex, num: neighb.num });
               visit(neighb.vertex);
            } else if (treeNums[neighb.vertex] == treeNums[vNum]) {
               treeEdges.push({ src: vNum, dest: neighb.vertex, num: neighb.num });
            }
         }
         visitEnd[vNum] = ++time;
         sortedByEnd.push(vNum);
      }

      let time = 0, treeNum = 0;
      for (const vNum of verticesOrder) {
         countOperation("Consultation de sommet");
         if (!accessed[vNum]) {
            ++treeNum;
            visit(vNum);
         }
      }
   }

   const listOfVertices = new Array(graph.nbVertices), predLists = new Array(graph.nbVertices);
   for (let vNum = 0; vNum < graph.nbVertices; ++vNum) {
      listOfVertices[vNum] = vNum;
      predLists[vNum] = [];
   }
   minimalDFS(listOfVertices, graph.succLists);

   for (let sortedI = sortedByEnd.length - 1; sortedI >= 0; --sortedI) {
      const vNum = sortedByEnd[sortedI];
      for (const neighb of graph.succLists[vNum]) {
         countOperation("Ajout de prédécesseur");
         predLists[neighb.vertex].push({ vertex: vNum, num: neighb.num });
      }
   }
   const newOrder = sortedByEnd.slice().reverse();

   const tableToShow = [
         ["Sommets réordonnés"].concat(newOrder.map(getVertexLabel)),
         ["Date de fin de visite"].concat(newOrder.map(vNum => visitEnd[vNum])),
         ["Prédécesseurs réordonnés"].concat(
            newOrder.map(vNum => predLists[vNum].map(pred => getVertexLabel(pred.vertex)).join(", "))),
      ], dataToShow = [
         { type: 'table', value: tableToShow },
         { type: 'vertexValues', position: [1, 0], color: 'purple', value: visitEnd.slice() },
      ];

   treeEdges = [];
   accessed.fill(false);
   parents.fill(null);
   treeNums.fill(null);
   visitEnd.fill('');
   minimalDFS(newOrder, predLists);
   const cfcEdges = new Array(treeEdges.length);
   for (let edgeNum = 0; edgeNum < treeEdges.length; ++edgeNum) {
      cfcEdges[edgeNum] = { src: treeEdges[edgeNum].dest, dest: treeEdges[edgeNum].src, num: treeEdges[edgeNum].num };
   }

   dataToShow.push({ type: 'colorization', target: getVerticesFromBools(accessed).concat(cfcEdges), value: 'green' });
   return [dataToShow];
}
