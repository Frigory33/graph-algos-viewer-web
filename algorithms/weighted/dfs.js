function dfs(graph) {
   const dataToShow = [];
   function addDataToShow(curVert, edgeEnd) {
      const tableToShow = formatArraysForVertices(
            ["Début de visite", "Fin de visite", "Père"], [visitStart, visitEnd, parents.map(getVertexLabel)]),
         currentElems = [];
      if (curVert != undefined) {
         currentElems.push(curVert);
      }
      if (edgeEnd != undefined) {
         currentElems.push(edgeEnd);
      }
      dataToShow.push([
            { type: 'table', value: tableToShow },
            { type: 'table', value: [["Pile"].concat(stack.map(getVertexLabel))] },
            { type: 'colorization', target: getVerticesFromBools(accessed).concat(parentsToEdges(parents)), value: 'green' },
            { type: 'colorization', target: backEdges.slice(), value: 'darkorange' },
            { type: 'colorization', target: forwardEdges.slice(), value: 'turquoise' },
            { type: 'colorization', target: crossEdges.slice(), value: 'magenta' },
            { type: 'vertexValues', position: [-1, 0], color: 'purple', value: visitStart.slice() },
            { type: 'vertexValues', position: [1, 0], color: 'purple', value: visitEnd.slice() },
            { type: 'colorization', target: currentElems, value: 'red' },
         ]);
   }

   const accessed = new Array(graph.nbVertices).fill(false),
      visitStart = new Array(graph.nbVertices).fill(''), visitEnd = new Array(graph.nbVertices).fill(''),
      parents = new Array(graph.nbVertices).fill(null);
   const backEdges = [], forwardEdges = [], crossEdges = [];
   const stack = [];
   let time = 0;

   function visit(vNum) {
      countOperation("Visite de sommet");
      stack.push(vNum);
      accessed[vNum] = true;
      visitStart[vNum] = ++time;
      addDataToShow(vNum);
      for (const neighb of graph.succLists[vNum]) {
         countOperation("Parcours d’arc");
         addDataToShow(vNum, neighb);
         if (!accessed[neighb.vertex]) {
            parents[neighb.vertex] = vNum;
            visit(neighb.vertex);
         } else if (visitEnd[neighb.vertex] === '') {
            if (!graph.isUndirected || parents[vNum] != neighb.vertex) {
               backEdges.push(neighb);
            }
         } else if (visitStart[vNum] < visitStart[neighb.vertex]) {
            if (!graph.isUndirected) {
               forwardEdges.push(neighb);
            }
         } else {
            crossEdges.push(neighb);
         }
      }
      visitEnd[vNum] = ++time;
      addDataToShow(vNum);
      stack.pop(vNum);
   }

   for (let vNum = 0; vNum < graph.nbVertices; ++vNum) {
      countOperation("Consultation de sommet");
      addDataToShow(vNum);
      if (!accessed[vNum]) {
         visit(vNum);
      }
   }
   addDataToShow();

   return dataToShow;
}
