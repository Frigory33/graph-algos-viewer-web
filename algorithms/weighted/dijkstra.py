#! /bin/env python3


from api import *


def dijkstra(graph):
   dataToShow = []
   if len(graph.selectedVertices) <= 0:
      firstVerticesTxt = "Attention : aucun sommet n’a été sélectionné."
   elif len(graph.selectedVertices) == 1:
      firstVerticesTxt = "Sommet de départ : " + getVertexLabel(graph.selectedVertices[0])
   else:
      firstVerticesTxt = "Sommets de départ : " + ", ".join(map(getVertexLabel, graph.selectedVertices))
   def addDataToShow(curVert = None, edgeEnd = None):
      queueTable = formatBinaryTree("File à priorité",
         list(map(lambda vNum: "{} <strong>{}</strong>".format(getVertexLabel(vNum), floatToStr(distances[vNum])), queue.data)))
      currentElems = []
      if curVert != None:
         currentElems.append(curVert)
      if edgeEnd != None:
         currentElems.append(edgeEnd)
      dataToShow.append([
            { "type": "text", "value": firstVerticesTxt },
            { "type": "table", "value": formatArraysForVertices(["Distance", "Père"], [distances, map(getVertexLabel, parents)]) },
            { "type": "table", "asHtml": True, "value": queueTable },
            { "type": "colorization", "target": getVerticesFromBools(visited) + parentsToEdges(parents), "value": "green" },
            { "type": "vertexValues", "color": "red", "value": list(map(lambda dist: "" if dist == None else dist, distances)) },
            { "type": "colorization", "target": currentElems, "value": "red" },
         ])

   visited = [False] * graph.nbVertices
   distances = [None] * graph.nbVertices
   parents = [None] * graph.nbVertices
   queue = PriorityQueue(lambda v1, v2: distances[v1] < distances[v2], True,
      ("Rangement pour enfilement", "Rangement pour défilement"))
   for vNum in graph.selectedVertices:
      countOperation("Enfilement ou mise à jour")
      distances[vNum] = 0
      queue.push(vNum)
   if len(graph.selectedVertices) == 0:
      addDataToShow()
   while queue.length() > 0:
      countOperation("Défilement, visite de sommet")
      curVert = queue.pop()
      addDataToShow(curVert)
      for neighb in graph.succLists[curVert]:
         newDist = distances[curVert] + neighb.weight
         countOperation("Parcours d’arc")
         addDataToShow(curVert, neighb)
         if distances[neighb.vertex] == None or (distances[neighb.vertex] > newDist and not visited[neighb.vertex]):
            countOperation("Enfilement ou mise à jour")
            notInQueue = distances[neighb.vertex] == None
            distances[neighb.vertex] = newDist
            parents[neighb.vertex] = curVert
            if notInQueue:
               queue.push(neighb.vertex)
            else:
               queue.upgrade(queue.dataIndexes[neighb.vertex], neighb.vertex)
            addDataToShow(curVert)
      visited[curVert] = True
      addDataToShow()

   return dataToShow


runAlgorithm(dijkstra)
