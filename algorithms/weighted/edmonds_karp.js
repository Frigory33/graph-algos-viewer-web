function edmonds_karp(graph) {
   const dataToShow = [];
   function addDataToShow(flowIncr) {
      const pathEdges = [], inPathEdges = new Array(graph.nbEdges).fill(false), otherTakenEdges = [];
      for (let curEdge = prevEdges[dstVNum]; curEdge != null; curEdge = prevEdges[curEdge.prevVert]) {
         pathEdges.push(curEdge);
         inPathEdges[curEdge.num] = true;
      }
      for (const edge of prevEdges) {
         if (edge != null && !inPathEdges[edge.num]) {
            otherTakenEdges.push(edge);
         }
      }
      const edgesLabels = edgesFlow.map((val, index) => val + "/" + graph.edges[index].weight);
      dataToShow.push([
            { type: 'text', value: "Source : " + getVertexLabel(srcVNum) + " → Puits : " + getVertexLabel(dstVNum) },
            { type: 'text', value: "Taille du flot : " + flowSize + (flowIncr != undefined ? " + " + flowIncr : "") },
            { type: 'colorization', target: [srcVNum, dstVNum], value: 'darkorange' },
            { type: 'colorization', target: pathEdges, value: 'red' },
            { type: 'colorization', target: otherTakenEdges, value: 'green' },
            { type: 'edgeValues', color: 'red', value: edgesLabels.map((val, index) => inPathEdges[index] ? val : '') },
            { type: 'edgeValues', color: 'green', value: edgesLabels.map((val, index) => !inPathEdges[index] ? val : '') },
         ]);
   }

   if (graph.selectedVertices.length < 2) {
      const msgPrg1 = "Sélectionnez deux sommets pour faire fonctionner cet algorithme. " +
            "Celui ayant l’étiquette la plus petite sera utilisé comme sommet source. " +
            "Celui ayant la deuxième étiquette la plus petite sera utilisé comme sommet puits.",
         msgPrg2 = "Remarque : quand vous cliquez sur l’onglet Algorithmes, le mode Sélectionner est activé automatiquement " +
            "pour vous permettre de sélectionner les deux sommets en appuyant sur Maj.";
      return [[{ type: 'text', value: msgPrg1 }, { type: 'text', value: msgPrg2 }]];
   }

   const srcVNum = graph.selectedVertices[0], dstVNum = graph.selectedVertices[1];
   const prevEdges = new Array(graph.nbVertices), edgesFlow = new Array(graph.nbEdges).fill(0);
   let flowSize = 0;

   function searchForPath() {
      countOperation("Recherche de chemin améliorant");
      const queue = [srcVNum];
      prevEdges.fill(null);
      prevEdges[srcVNum] = {};
      while (queue.length > 0) {
         countOperation("Visite de sommet");
         const curVert = queue.shift(), neighborsLists = [
               [graph.succLists[curVert], false, (capacity, flow) => capacity > flow],
               [graph.predLists[curVert], true, (capacity, flow) => flow > 0]
            ];
         for (const [neighbList, inverted, canTakeFct] of neighborsLists) {
            for (const neighb of neighbList) {
               countOperation("Parcours d’arc");
               if (prevEdges[neighb.vertex] == null && canTakeFct(neighb.weight, edgesFlow[neighb.num])) {
                  prevEdges[neighb.vertex] = { num: neighb.num, inverted: inverted, prevVert: curVert };
                  if (neighb.vertex == dstVNum) {
                     prevEdges[srcVNum] = null;
                     return true;
                  }
                  queue.push(neighb.vertex);
               }
            }
         }
      }
      return false;
   }
   function improveFlow() {
      countOperation("Exploitation de chemin améliorant");
      let amount = Infinity;
      for (let curEdge = prevEdges[dstVNum]; curEdge != null; curEdge = prevEdges[curEdge.prevVert]) {
         const flow = edgesFlow[curEdge.num];
         amount = Math.min(amount, curEdge.inverted ? flow : graph.edges[curEdge.num].weight - flow);
      }
      addDataToShow(amount);
      for (let curEdge = prevEdges[dstVNum]; curEdge != null; curEdge = prevEdges[curEdge.prevVert]) {
         countOperation("Mise à jour du flot sur un arc");
         edgesFlow[curEdge.num] += curEdge.inverted ? -amount : amount;
      }
      flowSize += amount;
      addDataToShow();
   }

   while (searchForPath()) {
      improveFlow();
   }
   prevEdges.fill(null);
   addDataToShow();

   return dataToShow;
}
