import java.util.*;


public class Dijkstra extends Api {
   private Graph graph;
   private final List<List<Map<String, Object>>> dataToShow = new ArrayList<>();

   private String firstVerticesTxt;

   private boolean[] visited;
   private Double[] distances;
   private int[] parents;
   private PriorityQueue queue;

   private void addDataToShow(Integer curVert, Graph.EdgeEnd edgeEnd) {
      String[] tableRowNames = { "Distance", "Père" };
      var distancesRow = Arrays.stream(distances).<Object>map(dist -> dist == null ? "—" : dist).toList();
      var parentsRow = Arrays.stream(parents).mapToObj(vNum -> vNum < 0 ? "—" : getVertexLabel(vNum)).toList();
      var tableRows = List.of(distancesRow, parentsRow);
      var queueContents = queue.data.stream()
            .map(vNum -> getVertexLabel(vNum) + " <strong>" + floatToStr(distances[vNum]) + "</strong>").toList();
      var queueTable = formatBinaryTree("File à priorité", queueContents);
      var markedElems = getVerticesFromBools(visited);
      markedElems.addAll(parentsToEdges(parents));
      var distancesData = Arrays.stream(distances).map(dist -> dist == null ? "" : dist).toList();
      List<Object> currentElems = new ArrayList<>();
      if (curVert != null) {
         currentElems.add(curVert);
      }
      if (edgeEnd != null) {
         currentElems.add(edgeEnd);
      }
      dataToShow.add(List.of(
            Map.ofEntries(Map.entry("type", "text"), Map.entry("value", firstVerticesTxt)),
            Map.ofEntries(Map.entry("type", "table"), Map.entry("value", formatArraysForVertices(tableRowNames, tableRows))),
            Map.ofEntries(Map.entry("type", "table"), Map.entry("asHtml", true), Map.entry("value", queueTable)),
            Map.ofEntries(Map.entry("type", "colorization"), Map.entry("target", markedElems), Map.entry("value", "green")),
            Map.ofEntries(Map.entry("type", "vertexValues"), Map.entry("color", "red"), Map.entry("value", distancesData)),
            Map.ofEntries(Map.entry("type", "colorization"), Map.entry("target", currentElems), Map.entry("value", "red"))
         ));
   }

   private List<List<Map<String, Object>>> dijkstra(Graph graphArg) {
      graph = graphArg;

      firstVerticesTxt = graph.selectedVertices.length <= 0 ? "Attention : aucun sommet n’a été sélectionné." :
         (graph.selectedVertices.length == 1 ? "Sommet de départ : " + getVertexLabel(graph.selectedVertices[0]) :
            "Sommets de départ : " + String.join(", ",
               Arrays.stream(graph.selectedVertices).mapToObj(Api::getVertexLabel).toList()));

      visited = new boolean[graph.nbVertices];
      distances = new Double[graph.nbVertices];
      parents = new int[graph.nbVertices];
      Arrays.fill(parents, -1);
      queue = new PriorityQueue((v1, v2) -> distances[v1] < distances[v2],
         new String[] { "Rangement pour enfilement", "Rangement pour défilement" });
      for (int vNum: graph.selectedVertices) {
         countOperation("Enfilement ou mise à jour");
         distances[vNum] = 0.;
         queue.push(vNum);
      }
      if (graph.selectedVertices.length == 0) {
         addDataToShow(null, null);
      }
      while (queue.length() > 0) {
         countOperation("Défilement, visite de sommet");
         int curVert = queue.pop();
         addDataToShow(curVert, null);
         for (var neighb: graph.succLists[curVert]) {
            double newDist = distances[curVert] + neighb.weight;
            countOperation("Parcours d’arc");
            addDataToShow(curVert, neighb);
            if (distances[neighb.vertex] == null || (distances[neighb.vertex] > newDist && !visited[neighb.vertex])) {
               countOperation("Enfilement ou mise à jour");
               boolean notInQueue = distances[neighb.vertex] == null;
               distances[neighb.vertex] = newDist;
               parents[neighb.vertex] = curVert;
               if (notInQueue) {
                  queue.push(neighb.vertex);
               } else {
                  queue.upgrade(queue.dataIndexes.get(neighb.vertex), neighb.vertex);
               }
               addDataToShow(curVert, null);
            }
         }
         visited[curVert] = true;
         addDataToShow(null, null);
      }

      return dataToShow;
   }


   public static void main(String[] args) {
      Dijkstra templ = new Dijkstra();
      runAlgorithm(templ::dijkstra);
   }
}
