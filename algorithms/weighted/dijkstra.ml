open Api


let dijkstra graph =
   let dataToShow = ref [] in
   let nbSelectedVertices = List.length graph.selectedVertices in
   let firstVerticesTxt =
      if nbSelectedVertices <= 0 then "Attention : aucun sommet n’a été sélectionné."
      else if nbSelectedVertices == 1 then "Sommet de départ : " ^ getVertexLabel (List.hd graph.selectedVertices)
      else "Sommets de départ : " ^ String.concat ", " (List.map getVertexLabel graph.selectedVertices)
   in

   let visited = Array.make graph.nbVertices false
   and distances = Array.make graph.nbVertices ~-.1.
   and parents = Array.make graph.nbVertices ~-1 in
   let queue = new priority_queue
      (fun v1 v2 -> distances.(v1) < distances.(v2)) ~assignValue:assignValueWithIndexes
      ~opNames:[| "Rangement pour enfilement"; "Rangement pour défilement" |] ~-1 in

   let addDataToShow curVert edgeNum =
      let formatQueueElem vNum =
         JsonStr (Printf.sprintf "%s <strong>%s</strong>" (getVertexLabel vNum) (floatToStr distances.(vNum)))
      in
      let queueTable = formatBinaryTree "File à priorité" (Array.map formatQueueElem queue#data) in
      let currentElems = ref [] in
      if curVert >= 0 then
         currentElems := (JsonInt curVert) :: !currentElems;
      if edgeNum >= 0 then
         currentElems := (JsonObj [("num", JsonInt edgeNum)]) :: !currentElems;
      let formatDistance noneStr dist = if dist < 0. then JsonStr noneStr else JsonFloat dist
      and formatVertexNum noneStr vNum = if vNum < 0 then JsonStr noneStr else JsonStr (getVertexLabel vNum) in
      let distancesRow = Array.map (formatDistance "—") distances
      and parentsRow = Array.map (formatVertexNum "—") parents in
      let tableData = formatArraysForVertices [| "Distance"; "Père" |] [| distancesRow; parentsRow |] in
      let markedElems = (getVerticesFromBools visited) @ (parentsToEdges parents) in
      let distancesData = Array.map (formatDistance "") distances in
      dataToShow := [
            [("type", JsonStr "text"); ("value", JsonStr firstVerticesTxt)];
            [("type", JsonStr "table"); ("value", JsonArray tableData)];
            [("type", JsonStr "table"); ("asHtml", JsonBool true); ("value", JsonArray queueTable)];
            [("type", JsonStr "colorization"); ("target", JsonList markedElems); ("value", JsonStr "green")];
            [("type", JsonStr "vertexValues"); ("color", JsonStr "red"); ("value", JsonArray distancesData)];
            [("type", JsonStr "colorization"); ("target", JsonList !currentElems); ("value", JsonStr "red")];
         ] :: !dataToShow;
   in

   List.iter (fun vNum ->
         countOperation "Enfilement ou mise à jour";
         distances.(vNum) <- 0.;
         queue#push(vNum);
      ) graph.selectedVertices;
   if nbSelectedVertices == 0 then
      addDataToShow ~-1 ~-1;
   while queue#length > 0 do
      countOperation "Défilement, visite de sommet";
      let curVert = queue#pop in
      addDataToShow curVert ~-1;
      List.iter (fun neighb ->
            let newDist = distances.(curVert) +. neighb.weight in
            countOperation "Parcours d’arc";
            addDataToShow curVert neighb.num;
            if distances.(neighb.vertex) < 0. || (distances.(neighb.vertex) > newDist && not visited.(neighb.vertex)) then (
               countOperation "Enfilement ou mise à jour";
               let notInQueue = distances.(neighb.vertex) < 0. in
               distances.(neighb.vertex) <- newDist;
               parents.(neighb.vertex) <- curVert;
               if notInQueue then (
                  queue#push neighb.vertex;
               ) else (
                  queue#upgrade queue#dataIndexes.(neighb.vertex) neighb.vertex;
               );
               addDataToShow curVert ~-1;
            );
         ) graph.succLists.(curVert);
      visited.(curVert) <- true;
      addDataToShow ~-1 ~-1;
   done;

   List.rev !dataToShow


let _ =
   runAlgorithm dijkstra;
