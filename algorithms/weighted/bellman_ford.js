function bellman_ford(graph) {
   const dataToShow = [];
   const firstVerticesTxt = graph.selectedVertices.length <= 0 ? "Attention : aucun sommet n’a été sélectionné." :
         (graph.selectedVertices.length == 1 ? "Sommet de départ : " + getVertexLabel(graph.selectedVertices[0]) :
            "Sommets de départ : " + graph.selectedVertices.map(getVertexLabel).join(", "));
   function addDataToShow(iterNum, edge, loosenedNb) {
      const currentElems = edge != undefined ? [edge] : [];
      const data = [
            { type: 'text', value: firstVerticesTxt },
            { type: 'table', value: formatArraysForVertices(["Distance", "Père"], [distances, parents.map(getVertexLabel)]) },
            { type: 'colorization', target: parentsToEdges(parents), value: 'green' },
            { type: 'vertexValues', color: 'red', value: distances.map(dist => dist == null ? '' : dist) },
            { type: 'colorization', target: currentElems, value: 'red' },
         ];
      if (iterNum != undefined) {
         data.push({ type: 'text', value: "Itération n° " + iterNum + " / " + graph.nbVertices });
      }
      if (loosenedNb != undefined && loosenedNb > 0) {
         data.push({ type: 'text', value: loosenedNb + " relaxations effectuées durant la dernière itération : " +
               "au moins un circuit absorbant est accessible depuis au moins un des nœuds sources." });
      }
      dataToShow.push(data);
   }

   const distances = new Array(graph.nbVertices).fill(null), parents = new Array(graph.nbVertices).fill(null);
   for (const vNum of graph.selectedVertices) {
      distances[vNum] = 0;
   }

   function checkEdge(iterNum, edge) {
      countOperation("Consultation d’arc");
      if (distances[edge.src] != null &&
            (distances[edge.dest] == null || distances[edge.dest] > distances[edge.src] + edge.weight)) {
         countOperation("Relaxation effective");
         addDataToShow(iterNum, edge);
         distances[edge.dest] = distances[edge.src] + edge.weight;
         parents[edge.dest] = edge.src;
         ++loosenedNb;
         addDataToShow(iterNum);
      }
   }

   let loosenedNb = 0;
   for (let iterNum = graph.selectedVertices.length; iterNum <= graph.nbVertices; ++iterNum) {
      countOperation("Itération");
      addDataToShow(iterNum);
      loosenedNb = 0;
      for (const edge of graph.edges) {
         checkEdge(iterNum, edge);
         if (graph.isUndirected) {
            checkEdge(iterNum, { src: edge.dest, dest: edge.src, weight: edge.weight, num: edge.num });
         }
      }
   }
   addDataToShow(undefined, undefined, loosenedNb);

   return dataToShow;
}
