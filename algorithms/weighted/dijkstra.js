function dijkstra(graph) {
   const dataToShow = [];
   const firstVerticesTxt = graph.selectedVertices.length <= 0 ? "Attention : aucun sommet n’a été sélectionné." :
         (graph.selectedVertices.length == 1 ? "Sommet de départ : " + getVertexLabel(graph.selectedVertices[0]) :
            "Sommets de départ : " + graph.selectedVertices.map(getVertexLabel).join(", "));
   function addDataToShow(curVert, edgeEnd) {
      const queueTable = formatBinaryTree("File à priorité", queue.data.map(vNum =>
               getVertexLabel(vNum) + " <strong>" + floatToStr(distances[vNum]) + "</strong>")),
         currentElems = [];
      if (curVert != undefined) {
         currentElems.push(curVert);
      }
      if (edgeEnd != undefined) {
         currentElems.push(edgeEnd);
      }
      dataToShow.push([
            { type: 'text', value: firstVerticesTxt },
            { type: 'table', value: formatArraysForVertices(["Distance", "Père"], [distances, parents.map(getVertexLabel)]) },
            { type: 'table', asHtml: true, value: queueTable },
            { type: 'colorization', target: getVerticesFromBools(visited).concat(parentsToEdges(parents)), value: 'green' },
            { type: 'vertexValues', color: 'red', value: distances.map(dist => dist == null ? '' : dist) },
            { type: 'colorization', target: currentElems, value: 'red' },
         ]);
   }

   const visited = new Array(graph.nbVertices).fill(false), distances = new Array(graph.nbVertices).fill(null),
      parents = new Array(graph.nbVertices).fill(null);
   const queue = PriorityQueue((v1, v2) => distances[v1] < distances[v2], true,
         ["Rangement pour enfilement", "Rangement pour défilement"]);
   for (const vNum of graph.selectedVertices) {
      countOperation("Enfilement ou mise à jour");
      distances[vNum] = 0;
      queue.push(vNum);
   }
   if (graph.selectedVertices.length == 0) {
      addDataToShow();
   }
   while (queue.length() > 0) {
      countOperation("Défilement, visite de sommet");
      const curVert = queue.pop();
      addDataToShow(curVert);
      for (const neighb of graph.succLists[curVert]) {
         const newDist = distances[curVert] + neighb.weight;
         countOperation("Parcours d’arc");
         addDataToShow(curVert, neighb);
         if (distances[neighb.vertex] == null || (distances[neighb.vertex] > newDist && !visited[neighb.vertex])) {
            countOperation("Enfilement ou mise à jour");
            const notInQueue = distances[neighb.vertex] == null;
            distances[neighb.vertex] = newDist;
            parents[neighb.vertex] = curVert;
            if (notInQueue) {
               queue.push(neighb.vertex);
            } else {
               queue.upgrade(queue.dataIndexes[neighb.vertex], neighb.vertex);
            }
            addDataToShow(curVert);
         }
      }
      visited[curVert] = true;
      addDataToShow();
   }

   return dataToShow;
}
